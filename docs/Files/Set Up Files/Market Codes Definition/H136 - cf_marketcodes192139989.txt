PARENT_MARKET_CODE	SELL_SEQUENCE	MARKET_CODE	DESCRIPTION	DISPLAY_COLOR	INACTIVE_YN
CAT	54	CCO	Cat - Corporate	BLACK	
CAT	55	CSO	Cat - Social	BLACK	
CAT	56	CIM	Cat - Intermediary	BLACK	
CAT	57	CSA	Cat - State Association	BLACK	
CAT	58	CLG	Cat - Government	BLACK	
CAT	59	CTT	Cat - Tour & Travel	BLACK	
CAT	60	CSP	Cat - Sports	BLACK	
CAT	61	CFR	Cat - Fraternal	BLACK	
CAT	62	CRE	Cat - Religious	BLACK	
CAT	63	CED	Cat - Educational Institution	BLACK	
CAT	64	CWH	Cat - Wholesale	BLACK	
CAT	65	CWE	Cat - Wedding	BLACK	
CAT	66	CMC	Cat - Multi-Cultural	BLACK	
CMP	23	CMP	Complimentary	YELLOW	
CON	21	CON	Contract	GREEN	
CON	22	RCO	Rebate Contract	GREEN	
GRP	24	GCO	Grp - Corporate	CYAN	
GRP	25	GSO	Grp - Social	CYAN	
GRP	26	GIM	Grp - Intermediary	CYAN	
GRP	27	GSA	Grp - Association	CYAN	
GRP	28	GLG	Grp - Goverment	CYAN	
GRP	29	GTT	Grp - Tour & Travel	CYAN	
GRP	30	GSP	Grp - Sports	CYAN	
GRP	31	GFR	Grp - Fraternal	CYAN	
GRP	32	GRE	Grp - Religious	CYAN	
GRP	33	GED	Grp - Educational Inst.	CYAN	
GRP	34	GWH	Grp - Wholesale	CYAN	
GRP	35	GMC	Grp - Multi-Cultural	CYAN	
GRP	36	DEL	Grp - Delayed Flights	CYAN	
GRP	37	RGR	Grp - Rebate	CYAN	
GRP	38	GWE	Grp - Wedding	CYAN	
INT	0	CRS	Default for Marsha	RED	
INT	40	HAC	House / PM Accounts	RED	
INT	41	NOM	No Market Specified	RED	
INT	42	A/R	Accounts Receivable	RED	
OTH	17	PDA	Part Day	WHITE	
OTH	18	GNS	Gtd. No Show	WHITE	
OTH	19	OTR	Other Rev. / Serv. Chg	WHITE	
OTH	20	RGN	Rebate Gtd. No Show	WHITE	
TRA	1	REG	Premium Retail	BLUE	
TRA	2	BEN	Standard Retail	BLUE	
TRA	3	SPE	Special Corporate	BLUE	
TRA	4	PKG	Packages	BLUE	
TRA	5	SFB	Weekend Standard Retail/SFB	BLUE	
TRA	6	OTD	Other Discounts	BLUE	
TRA	7	WHO	Wholesaler	BLUE	
TRA	8	RTR	Rebate Transient	BLUE	
TRA	9	ECM	eChannel Retail	BLUE	
TRA	10	MAR	Associate Leisure	BLUE	
TRA	11	MIL	Govt. / Military	BLUE	
TRA	12	AAA	AAA	BLUE	
TRA	13	SDC	Senior Discount	BLUE	
TRA	14	ADP	Advance Purchase	BLUE	
TRA	15	RMR	Rebate MRWD	BLUE	
TRA	16	BMR	Rewards Redemptions	BLUE	
CF_LOGO

