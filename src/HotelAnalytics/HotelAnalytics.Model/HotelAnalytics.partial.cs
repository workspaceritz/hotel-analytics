﻿using Serilog;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace HotelAnalytics.Model
{
	public partial class HotelAnalytics
	{
		const string METHOD = "Method";

		private readonly ILogger _classLogger = Log.ForContext<HotelAnalytics>();

		private int _commandTimeout = 3600;
		public int CommandTimeout
		{
			get { return _commandTimeout; }
		}

		partial void InitializePartial()
		{
			var log = _classLogger.ForContext(METHOD, nameof(InitializePartial));
			SetCommandTimeOut(_commandTimeout);
			bool.TryParse(ConfigurationManager.AppSettings["DebugIncludeDBStatements"], out var debugIncludeDBStatements);
			if (debugIncludeDBStatements)
			{
				Database.Log = s => log.Debug(s);
			}
		}

		public void SetCommandTimeOut(int timeout)
		{
			var objectContext = (this as IObjectContextAdapter).ObjectContext;
			objectContext.CommandTimeout = timeout;
		}


		public override int SaveChanges()
		{
			try
			{
				return base.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				// Retrieve the error messages as a list of strings.
				var errorMessages = ex.EntityValidationErrors
						.SelectMany(x => x.ValidationErrors)
						.Select(x => x.ErrorMessage);

				// Join the list to a single string.
				var fullErrorMessage = string.Join("; ", errorMessages);

				// Combine the original exception message with the new one.
				var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

				// Throw a new DbEntityValidationException with the improved exception message.
				throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
			}
		}

		public override Task<int> SaveChangesAsync()
		{
			try
			{
				return base.SaveChangesAsync();
			}
			catch (DbEntityValidationException ex)
			{
				// Retrieve the error messages as a list of strings.
				var errorMessages = ex.EntityValidationErrors
						.SelectMany(x => x.ValidationErrors)
						.Select(x => x.ErrorMessage);

				// Join the list to a single string.
				var fullErrorMessage = string.Join("; ", errorMessages);

				// Combine the original exception message with the new one.
				var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

				// Throw a new DbEntityValidationException with the improved exception message.
				throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
			}
		}
	}
}
