// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // MarketCodeDefinitions
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class Staging_MarketCodeDefinitionConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<Staging_MarketCodeDefinition>
    {
        public Staging_MarketCodeDefinitionConfiguration()
            : this("Staging")
        {
        }

        public Staging_MarketCodeDefinitionConfiguration(string schema)
        {
            ToTable("MarketCodeDefinitions", schema);
            Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.BatchId).HasColumnName(@"BatchId").HasColumnType("bigint");
            Property(x => x.Created).HasColumnName(@"Created").HasColumnType("datetime2");
            Property(x => x.MarketGroup).HasColumnName(@"MarketGroup").HasColumnType("nvarchar").IsOptional();
            Property(x => x.MarketSegment).HasColumnName(@"MarketSegment").HasColumnType("nvarchar").IsOptional();
            Property(x => x.Account).HasColumnName(@"Account").HasColumnType("nvarchar").IsOptional();
            Property(x => x.AccountComments).HasColumnName(@"AccountComments").HasColumnType("nvarchar").IsOptional();
            Property(x => x.Type).HasColumnName(@"Type").HasColumnType("nvarchar").IsOptional();
            Property(x => x.WDWE).HasColumnName(@"WDWE").HasColumnType("nvarchar").IsOptional();
            Property(x => x.DptFrom).HasColumnName(@"DptFrom").HasColumnType("nvarchar").IsOptional();
            Property(x => x.DptTo).HasColumnName(@"DptTo").HasColumnType("nvarchar").IsOptional();

            // Foreign keys
            HasRequired(a => a.Batch).WithMany(b => b.Staging_MarketCodeDefinitions).HasForeignKey(c => c.BatchId).WillCascadeOnDelete(false); // FK_MarketCodeDefinitions_Batch
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
