// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // FactTransaction
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_FactTransactionConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<DW_FactTransaction>
    {
        public DW_FactTransactionConfiguration()
            : this("DW")
        {
        }

        public DW_FactTransactionConfiguration(string schema)
        {
            ToTable("FactTransaction", schema);
            Property(x => x.FactTransactionId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.DataPeriod).HasColumnName(@"DataPeriod").HasColumnType("date");
            Property(x => x.DimHotelId).HasColumnName(@"DimHotelId").HasColumnType("int");
            Property(x => x.DimScenarioId).HasColumnName(@"DimScenarioId").HasColumnType("int");
            Property(x => x.DimMarketId).HasColumnName(@"DimMarketId").HasColumnType("int");
            Property(x => x.DimTransactionTypeId).HasColumnName(@"DimTransactionTypeId").HasColumnType("int");
            Property(x => x.DimRoomClassId).HasColumnName(@"DimRoomClassId").HasColumnType("int");
            Property(x => x.ReceiptNo).HasColumnName(@"ReceiptNo").HasColumnType("nvarchar");
            Property(x => x.GuestFullName).HasColumnName(@"GuestFullName").HasColumnType("nvarchar");
            Property(x => x.TrxDesc).HasColumnName(@"TrxDesc").HasColumnType("nvarchar");
            Property(x => x.Reference).HasColumnName(@"Reference").HasColumnType("nvarchar");
            Property(x => x.TrxNo).HasColumnName(@"TrxNo").HasColumnType("nvarchar");
            Property(x => x.Room).HasColumnName(@"Room").HasColumnType("nvarchar");
            Property(x => x.CreditCardSupplement).HasColumnName(@"CreditCardSupplement").HasColumnType("nvarchar");
            Property(x => x.Remark).HasColumnName(@"Remark").HasColumnType("nvarchar");
            Property(x => x.ChequeNumber).HasColumnName(@"ChequeNumber").HasColumnType("nvarchar");
            Property(x => x.DebitValue).HasColumnName(@"DebitValue").HasColumnType("decimal").HasPrecision(18,6);
            Property(x => x.CreditValue).HasColumnName(@"CreditValue").HasColumnType("decimal").HasPrecision(18,6);
            Property(x => x.BatchId).HasColumnName(@"BatchId").HasColumnType("bigint");
            Property(x => x.Created).HasColumnName(@"Created").HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").HasColumnType("nvarchar");
            Property(x => x.Modified).HasColumnName(@"Modified").HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName(@"ModifiedBy").HasColumnType("nvarchar");

            // Foreign keys
            HasRequired(a => a.Batch).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.BatchId).WillCascadeOnDelete(false); // FK_FactTransaction_Batch
            HasRequired(a => a.DW_DimDate).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.DataPeriod).WillCascadeOnDelete(false); // FK_FactTransaction_DimDate
            HasRequired(a => a.DW_DimMarket).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.DimMarketId).WillCascadeOnDelete(false); // FK_FactTransaction_DimMarket
            HasRequired(a => a.DW_DimRoomClass).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.DimRoomClassId).WillCascadeOnDelete(false); // FK_FactTransaction_DimRoomClass
            HasRequired(a => a.DW_DimScenario).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.DimScenarioId).WillCascadeOnDelete(false); // FK_FactTransaction_DimScenario
            HasRequired(a => a.DW_DimTransactionType).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.DimTransactionTypeId).WillCascadeOnDelete(false); // FK_FactTransaction_DimTransactionType
            HasRequired(a => a.Hotel).WithMany(b => b.DW_FactTransactions).HasForeignKey(c => c.DimHotelId).WillCascadeOnDelete(false); // FK_FactTransaction_DimHotel
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
