﻿namespace HotelAnalytics.Model
{
	public class EFDataAccessFactory : IDataAccessFactory
	{
		IHotelAnalytics IDataAccessFactory.GetHotelAnalytics()
		{
			return new HotelAnalytics();
		}

		IHotelAnalytics IDataAccessFactory.GetHotelAnalytics(string connectionString)
		{
			return new HotelAnalytics(connectionString);
		}
	}
}
