// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // vDSVDimClubMember
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_vDSVDimClubMember
    {
        [Column(@"DimClubMemberId", Order = 1, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim club member ID")]
        public int DimClubMemberId { get; set; } // DimClubMemberId (Primary key)

        [Column(@"IsClubMember", Order = 2, TypeName = "varchar")]
        [Required]
        [MaxLength(3)]
        [StringLength(3)]
        [Key]
        [Display(Name = "Is club member")]
        public string IsClubMember { get; set; } // IsClubMember (Primary key) (length: 3)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Code")]
        public string Code { get; set; } // Code (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Number")]
        public string Number { get; set; } // Number (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Card nr")]
        public string CardNr { get; set; } // CardNr (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Last name")]
        public string LastName { get; set; } // LastName (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "First name")]
        public string FirstName { get; set; } // FirstName (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Civility")]
        public string Civility { get; set; } // Civility (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Gender")]
        public string Gender { get; set; } // Gender (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "City")]
        public string City { get; set; } // City (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Country")]
        public string Country { get; set; } // Country (length: 255)

        public DW_vDSVDimClubMember()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
