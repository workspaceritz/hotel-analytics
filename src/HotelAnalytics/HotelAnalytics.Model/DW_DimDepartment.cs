// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // DimDepartment
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_DimDepartment
    {
        [Column(@"DimDepartmentId", Order = 1, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim department ID")]
        public int DimDepartmentId { get; set; } // DimDepartmentId (Primary key)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Department code")]
        public string DepartmentCode { get; set; } // DepartmentCode (length: 256)

        [Required]
        [Display(Name = "Created")]
        public System.DateTime Created { get; set; } // Created

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Created by")]
        public string CreatedBy { get; set; } // CreatedBy (length: 128)

        [Required]
        [Display(Name = "Modified")]
        public System.DateTime Modified { get; set; } // Modified

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Modified by")]
        public string ModifiedBy { get; set; } // ModifiedBy (length: 128)

        // Reverse navigation

        /// <summary>
        /// Child DW_FactPlannings where [FactPlanning].[DimDepartmentId] point to this entity (FK_FactPlanning_DimDepartment)
        /// </summary>
        public virtual System.Collections.Generic.ICollection<DW_FactPlanning> DW_FactPlannings { get; set; } // FactPlanning.FK_FactPlanning_DimDepartment

        public DW_DimDepartment()
        {
            Created = System.DateTime.Now;
            CreatedBy = "suser_sname()";
            Modified = System.DateTime.Now;
            ModifiedBy = "suser_sname()";
            DW_FactPlannings = new System.Collections.Generic.List<DW_FactPlanning>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
