// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // DimDate
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_DimDateConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<DW_DimDate>
    {
        public DW_DimDateConfiguration()
            : this("DW")
        {
        }

        public DW_DimDateConfiguration(string schema)
        {
            ToTable("DimDate", schema);
            Property(x => x.the_date).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.year).HasColumnName(@"year").HasColumnType("smallint");
            Property(x => x.quarter_of_year).HasColumnName(@"quarter_of_year").HasColumnType("tinyint");
            Property(x => x.month_of_year).HasColumnName(@"month_of_year").HasColumnType("tinyint");
            Property(x => x.day_of_year).HasColumnName(@"day_of_year").HasColumnType("int");
            Property(x => x.day_of_month).HasColumnName(@"day_of_month").HasColumnType("tinyint");
            Property(x => x.quarter).HasColumnName(@"quarter").HasColumnType("int");
            Property(x => x.month).HasColumnName(@"month").HasColumnType("int");
            Property(x => x.day).HasColumnName(@"day").HasColumnType("int");
            Property(x => x.day_of_week).HasColumnName(@"day_of_week").HasColumnType("tinyint");
            Property(x => x.is_working_day).HasColumnName(@"is_working_day").HasColumnType("int");
            Property(x => x.is_weekend).HasColumnName(@"is_weekend").HasColumnType("int");
            Property(x => x.quarter_name).HasColumnName(@"quarter_name").HasColumnType("nvarchar");
            Property(x => x.month_name).HasColumnName(@"month_name").HasColumnType("nvarchar");
            Property(x => x.day_name).HasColumnName(@"day_name").HasColumnType("nvarchar");
            Property(x => x.day_of_week_name).HasColumnName(@"day_of_week_name").HasColumnType("nvarchar");
            Property(x => x.quarter_of_year_name).HasColumnName(@"quarter_of_year_name").HasColumnType("nvarchar");
            Property(x => x.month_of_year_name).HasColumnName(@"month_of_year_name").HasColumnType("nvarchar");
            Property(x => x.is_working_day_name).HasColumnName(@"is_working_day_name").HasColumnType("nvarchar");
            Property(x => x.week).HasColumnName(@"week").HasColumnType("int");
            Property(x => x.week_of_year).HasColumnName(@"week_of_year").HasColumnType("int");
            Property(x => x.week_name).HasColumnName(@"week_name").HasColumnType("nvarchar");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
