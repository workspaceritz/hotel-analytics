// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // DimReservationStatus
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_DimReservationStatuConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<DW_DimReservationStatu>
    {
        public DW_DimReservationStatuConfiguration()
            : this("DW")
        {
        }

        public DW_DimReservationStatuConfiguration(string schema)
        {
            ToTable("DimReservationStatus", schema);
            Property(x => x.DimReservationStatusId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.ReservationStatusCode).HasColumnName(@"ReservationStatusCode").HasColumnType("nvarchar");
            Property(x => x.Created).HasColumnName(@"Created").HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").HasColumnType("nvarchar");
            Property(x => x.Modified).HasColumnName(@"Modified").HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName(@"ModifiedBy").HasColumnType("nvarchar");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
