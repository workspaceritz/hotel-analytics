// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // vDSVDimLegalEntity
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_vDSVDimLegalEntityConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<DW_vDSVDimLegalEntity>
    {
        public DW_vDSVDimLegalEntityConfiguration()
            : this("DW")
        {
        }

        public DW_vDSVDimLegalEntityConfiguration(string schema)
        {
            ToTable("vDSVDimLegalEntity", schema);
            Property(x => x.DimLegalEntityId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.LegalEntityCode).IsUnicode(false).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
