// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // DimRoomType
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_DimRoomType
    {
        [Column(@"DimRoomTypeId", Order = 1, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim room type ID")]
        public int DimRoomTypeId { get; set; } // DimRoomTypeId (Primary key)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Label")]
        public string Label { get; set; } // Label (length: 256)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Class")]
        public string Class { get; set; } // Class (length: 256)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Description")]
        public string Description { get; set; } // Description (length: 256)

        [Required]
        [Display(Name = "Suite")]
        public bool Suite { get; set; } // Suite

        [Required]
        [Display(Name = "Is room")]
        public bool IsRoom { get; set; } // IsRoom

        [Required]
        [Display(Name = "Created")]
        public System.DateTime Created { get; set; } // Created

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Created by")]
        public string CreatedBy { get; set; } // CreatedBy (length: 128)

        [Required]
        [Display(Name = "Modified")]
        public System.DateTime Modified { get; set; } // Modified

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Modified by")]
        public string ModifiedBy { get; set; } // ModifiedBy (length: 128)

        public DW_DimRoomType()
        {
            Created = System.DateTime.Now;
            CreatedBy = "suser_sname()";
            Modified = System.DateTime.Now;
            ModifiedBy = "suser_sname()";
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
