// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // H196
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class Staging_H196
    {
        [Column(@"Id", Order = 1, TypeName = "bigint")]
        [Required]
        [Key]
        [Display(Name = "Id")]
        public long Id { get; set; } // Id (Primary key)

        [Required]
        [Display(Name = "Batch ID")]
        public long BatchId { get; set; } // BatchId

        [Required]
        [Display(Name = "Created")]
        public System.DateTime Created { get; set; } // Created

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Resort")]
        public string RESORT { get; set; } // RESORT (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Tax inclusive yn")]
        public string TAX_INCLUSIVE_YN { get; set; } // TAX_INCLUSIVE_YN (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Frequent flyer yn")]
        public string FREQUENT_FLYER_YN { get; set; } // FREQUENT_FLYER_YN (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Trx code")]
        public string TRX_CODE { get; set; } // TRX_CODE (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Tc group")]
        public string TC_GROUP { get; set; } // TC_GROUP (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Tc subgroup")]
        public string TC_SUBGROUP { get; set; } // TC_SUBGROUP (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Description")]
        public string DESCRIPTION { get; set; } // DESCRIPTION (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Is manual post allowed")]
        public string IS_MANUAL_POST_ALLOWED { get; set; } // IS_MANUAL_POST_ALLOWED (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Adj trx code")]
        public string ADJ_TRX_CODE { get; set; } // ADJ_TRX_CODE (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Ind revenue gp")]
        public string IND_REVENUE_GP { get; set; } // IND_REVENUE_GP (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Currency 1")]
        public string CURRENCY1 { get; set; } // CURRENCY1 (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Ind ar")]
        public string IND_AR { get; set; } // IND_AR (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Ind billing")]
        public string IND_BILLING { get; set; } // IND_BILLING (length: 255)

        [Display(Name = "Ind cash")]
        public string IND_CASH { get; set; } // IND_CASH

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Ind deposit yn")]
        public string IND_DEPOSIT_YN { get; set; } // IND_DEPOSIT_YN (length: 255)

        // Foreign keys

        /// <summary>
        /// Parent Batch pointed by [H196].([BatchId]) (FK_H196_Batch)
        /// </summary>
        public virtual Batch Batch { get; set; } // FK_H196_Batch

        public Staging_H196()
        {
            Created = System.DateTime.Now;
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
