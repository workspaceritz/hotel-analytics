// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // DimOperaTransaction
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_DimOperaTransaction
    {
        [Column(@"DimOperaTransactionId", Order = 1, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim opera transaction ID")]
        public int DimOperaTransactionId { get; set; } // DimOperaTransactionId (Primary key)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Transaction code")]
        public string TransactionCode { get; set; } // TransactionCode (length: 256)

        [Required]
        [Display(Name = "Description")]
        public string Description { get; set; } // Description

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Account")]
        public string Account { get; set; } // Account (length: 256)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Dept")]
        public string Dept { get; set; } // Dept (length: 256)

        [Required]
        [Display(Name = "Exclude")]
        public bool Exclude { get; set; } // Exclude

        [Required]
        [Display(Name = "Input vatf lag")]
        public bool InputVATFlag { get; set; } // InputVATFlag

        [Required]
        [Display(Name = "Output vatf lag")]
        public bool OutputVATFlag { get; set; } // OutputVATFlag

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Vata pplicable code")]
        public string VATApplicableCode { get; set; } // VATApplicableCode (length: 256)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Vatt ax percentage")]
        public string VATTaxPercentage { get; set; } // VATTaxPercentage (length: 256)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Offset account")]
        public string OffsetAccount { get; set; } // OffsetAccount (length: 256)

        [Required]
        [MaxLength(256)]
        [StringLength(256)]
        [Display(Name = "Offset dept")]
        public string OffsetDept { get; set; } // OffsetDept (length: 256)

        [Required]
        [Display(Name = "Created")]
        public System.DateTime Created { get; set; } // Created

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Created by")]
        public string CreatedBy { get; set; } // CreatedBy (length: 128)

        [Required]
        [Display(Name = "Modified")]
        public System.DateTime Modified { get; set; } // Modified

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Modified by")]
        public string ModifiedBy { get; set; } // ModifiedBy (length: 128)

        public DW_DimOperaTransaction()
        {
            Created = System.DateTime.Now;
            CreatedBy = "suser_sname()";
            Modified = System.DateTime.Now;
            ModifiedBy = "suser_sname()";
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
