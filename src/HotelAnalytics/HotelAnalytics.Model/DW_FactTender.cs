// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // FactTender
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_FactTender
    {
        [Column(@"FactTenderId", Order = 1, TypeName = "bigint")]
        [Required]
        [Key]
        [Display(Name = "Fact tender ID")]
        public long FactTenderId { get; set; } // FactTenderId (Primary key)

        [Required]
        [Display(Name = "Business dates")]
        public System.DateTime BusinessDates { get; set; } // BusinessDates

        [Required]
        [Display(Name = "Dim hotel ID")]
        public int DimHotelId { get; set; } // DimHotelId

        [Required]
        [Display(Name = "Dim revenue center ID")]
        public int DimRevenueCenterId { get; set; } // DimRevenueCenterId

        [Required]
        [Display(Name = "Dim order type ID")]
        public int DimOrderTypeId { get; set; } // DimOrderTypeId

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Check number")]
        public string CheckNumber { get; set; } // CheckNumber (length: 255)

        [Required]
        [Display(Name = "Business date")]
        public System.DateTime BusinessDate { get; set; } // BusinessDate

        [Required]
        [Display(Name = "Transaction date time")]
        public System.DateTime TransactionDateTime { get; set; } // TransactionDateTime

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Seat number")]
        public string SeatNumber { get; set; } // SeatNumber (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Price level")]
        public string PriceLevel { get; set; } // PriceLevel (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Item name")]
        public string ItemName { get; set; } // ItemName (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Item number")]
        public string ItemNumber { get; set; } // ItemNumber (length: 255)

        [Display(Name = "Line count")]
        public int? LineCount { get; set; } // LineCount

        [Display(Name = "Line total")]
        public decimal? LineTotal { get; set; } // LineTotal

        [Display(Name = "Line total 1")]
        public decimal? LineTotal1 { get; set; } // LineTotal1

        [Display(Name = "Report line count")]
        public int? ReportLineCount { get; set; } // ReportLineCount

        [Display(Name = "Report line total")]
        public decimal? ReportLineTotal { get; set; } // ReportLineTotal

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Voids")]
        public string Voids { get; set; } // Voids (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Manager voids")]
        public string ManagerVoids { get; set; } // ManagerVoids (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Returns")]
        public string Returns { get; set; } // Returns (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Check employee")]
        public string CheckEmployee { get; set; } // CheckEmployee (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Transaction employee")]
        public string TransactionEmployee { get; set; } // TransactionEmployee (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Authorization employee")]
        public string AuthorizationEmployee { get; set; } // AuthorizationEmployee (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Meal employee")]
        public string MealEmployee { get; set; } // MealEmployee (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Reason code")]
        public string ReasonCode { get; set; } // ReasonCode (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Reference info")]
        public string ReferenceInfo { get; set; } // ReferenceInfo (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Swipe type")]
        public string SwipeType { get; set; } // SwipeType (length: 255)

        [Required]
        [Display(Name = "Batch ID")]
        public long BatchId { get; set; } // BatchId

        [Required]
        [Display(Name = "Created")]
        public System.DateTime Created { get; set; } // Created

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Created by")]
        public string CreatedBy { get; set; } // CreatedBy (length: 128)

        [Required]
        [Display(Name = "Modified")]
        public System.DateTime Modified { get; set; } // Modified

        [Required]
        [MaxLength(128)]
        [StringLength(128)]
        [Display(Name = "Modified by")]
        public string ModifiedBy { get; set; } // ModifiedBy (length: 128)

        // Foreign keys

        /// <summary>
        /// Parent Batch pointed by [FactTender].([BatchId]) (FK_FactTender_Batch)
        /// </summary>
        public virtual Batch Batch { get; set; } // FK_FactTender_Batch

        /// <summary>
        /// Parent DW_DimDate pointed by [FactTender].([BusinessDate]) (FK_FactTender_DimDate)
        /// </summary>
        public virtual DW_DimDate DW_DimDate { get; set; } // FK_FactTender_DimDate

        public DW_FactTender()
        {
            Created = System.DateTime.Now;
            CreatedBy = "suser_sname()";
            Modified = System.DateTime.Now;
            ModifiedBy = "suser_sname()";
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
