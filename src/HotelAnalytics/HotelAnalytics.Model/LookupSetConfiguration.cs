// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // LookupSet
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class LookupSetConfiguration : System.Data.Entity.ModelConfiguration.EntityTypeConfiguration<LookupSet>
    {
        public LookupSetConfiguration()
            : this("dbo")
        {
        }

        public LookupSetConfiguration(string schema)
        {
            ToTable("LookupSet", schema);
            Property(x => x.LookupSetId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.LookupSet_).HasColumnName(@"LookupSet").HasColumnType("nvarchar");
            Property(x => x.Created).HasColumnName(@"Created").HasColumnType("datetime2");
            Property(x => x.CreatedBy).HasColumnName(@"CreatedBy").HasColumnType("nvarchar");
            Property(x => x.Modified).HasColumnName(@"Modified").HasColumnType("datetime2");
            Property(x => x.ModifiedBy).HasColumnName(@"ModifiedBy").HasColumnType("nvarchar");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
