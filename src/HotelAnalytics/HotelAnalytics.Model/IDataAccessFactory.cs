﻿namespace HotelAnalytics.Model
{
	public interface IDataAccessFactory
	{
		IHotelAnalytics GetHotelAnalytics();

		IHotelAnalytics GetHotelAnalytics(string connectionString);
	}
}
