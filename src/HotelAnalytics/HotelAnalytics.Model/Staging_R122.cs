// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // R122
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class Staging_R122
    {
        [Column(@"Id", Order = 1, TypeName = "bigint")]
        [Required]
        [Key]
        [Display(Name = "Id")]
        public long Id { get; set; } // Id (Primary key)

        [Required]
        [Display(Name = "Batch ID")]
        public long BatchId { get; set; } // BatchId

        [Required]
        [Display(Name = "Created")]
        public System.DateTime Created { get; set; } // Created

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Reservation status")]
        public string RESERVATION_STATUS { get; set; } // RESERVATION_STATUS (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Report ID")]
        public string REPORT_ID { get; set; } // REPORT_ID (length: 255)

        [Display(Name = "Reservation date")]
        public System.DateTime? RESERVATION_DATE { get; set; } // RESERVATION_DATE

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Char reservation date")]
        public string CHAR_RESERVATION_DATE { get; set; } // CHAR_RESERVATION_DATE (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Market code")]
        public string MARKET_CODE { get; set; } // MARKET_CODE (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Cf market code seq")]
        public string CF_MARKET_CODE_SEQ { get; set; } // CF_MARKET_CODE_SEQ (length: 255)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Market code desc")]
        public string MARKET_CODE_DESC { get; set; } // MARKET_CODE_DESC (length: 255)

        [Display(Name = "S def rooms day")]
        public int? S_DEF_ROOMS_DAY { get; set; } // S_DEF_ROOMS_DAY

        [Display(Name = "S def rooms mkt")]
        public int? S_DEF_ROOMS_MKT { get; set; } // S_DEF_ROOMS_MKT

        [Display(Name = "S rev day")]
        public decimal? S_REV_DAY { get; set; } // S_REV_DAY

        [Display(Name = "S rev mkt")]
        public decimal? S_REV_MKT { get; set; } // S_REV_MKT

        [Display(Name = "Day rooms")]
        public int? DAY_ROOMS { get; set; } // DAY_ROOMS

        [Display(Name = "Tot arr day")]
        public decimal? TOT_ARR_DAY { get; set; } // TOT_ARR_DAY

        [Display(Name = "Tot arr mkt")]
        public decimal? TOT_ARR_MKT { get; set; } // TOT_ARR_MKT

        [Display(Name = "Arrival rms day")]
        public int? ARRIVAL_RMS_DAY { get; set; } // ARRIVAL_RMS_DAY

        [Display(Name = "Arr rms mkt")]
        public int? ARR_RMS_MKT { get; set; } // ARR_RMS_MKT

        [Display(Name = "Guest day")]
        public int? GUEST_DAY { get; set; } // GUEST_DAY

        [Display(Name = "Guest mkt")]
        public int? GUEST_MKT { get; set; } // GUEST_MKT

        [Display(Name = "Double occ day")]
        public int? DOUBLE_OCC_DAY { get; set; } // DOUBLE_OCC_DAY

        [Display(Name = "Double occ mkt")]
        public int? DOUBLE_OCC_MKT { get; set; } // DOUBLE_OCC_MKT

        [Display(Name = "Single occ day")]
        public int? SINGLE_OCC_DAY { get; set; } // SINGLE_OCC_DAY

        [Display(Name = "Single occ mkt")]
        public int? SINGLE_OCC_MKT { get; set; } // SINGLE_OCC_MKT

        [Display(Name = "Per double mkt")]
        public decimal? PER_DOUBLE_MKT { get; set; } // PER_DOUBLE_MKT

        [Display(Name = "Per double occ day")]
        public decimal? PER_DOUBLE_OCC_DAY { get; set; } // PER_DOUBLE_OCC_DAY

        [Display(Name = "Per occ day")]
        public decimal? PER_OCC_DAY { get; set; } // PER_OCC_DAY

        [Display(Name = "Per occ mkt")]
        public decimal? PER_OCC_MKT { get; set; } // PER_OCC_MKT

        [Display(Name = "Cs sum oo mkt")]
        public decimal? CS_SUM_OO_MKT { get; set; } // CS_SUM_OO_MKT

        [Display(Name = "Cs avg oo day")]
        public decimal? CS_AVG_OO_DAY { get; set; } // CS_AVG_OO_DAY

        [Display(Name = "No definite rooms")]
        public int? NO_DEFINITE_ROOMS { get; set; } // NO_DEFINITE_ROOMS

        [Display(Name = "Arrival rms")]
        public int? ARRIVAL_RMS { get; set; } // ARRIVAL_RMS

        [Display(Name = "No of guests")]
        public int? NO_OF_GUESTS { get; set; } // NO_OF_GUESTS

        [Display(Name = "Single occupancy")]
        public int? SINGLE_OCCUPANCY { get; set; } // SINGLE_OCCUPANCY

        [Display(Name = "Multi occupancy")]
        public int? MULTI_OCCUPANCY { get; set; } // MULTI_OCCUPANCY

        [Display(Name = "Total revenue")]
        public decimal? TOTAL_REVENUE { get; set; } // TOTAL_REVENUE

        [Display(Name = "Oo rooms")]
        public int? OO_ROOMS { get; set; } // OO_ROOMS

        [Display(Name = "Print no of rooms")]
        public int? PRINT_NO_OF_ROOMS { get; set; } // PRINT_NO_OF_ROOMS

        [Display(Name = "Get arr")]
        public int? GET_ARR { get; set; } // GET_ARR

        [Display(Name = "Per occ")]
        public int? PER_OCC { get; set; } // PER_OCC

        [Display(Name = "Multi occ per")]
        public int? MULTI_OCC_PER { get; set; } // MULTI_OCC_PER

        // Foreign keys

        /// <summary>
        /// Parent Batch pointed by [R122].([BatchId]) (FK_R122_Batch)
        /// </summary>
        public virtual Batch Batch { get; set; } // FK_R122_Batch

        public Staging_R122()
        {
            Created = System.DateTime.Now;
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
