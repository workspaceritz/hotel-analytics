// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable EmptyNamespace
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.7
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelAnalytics.Model
{

    // vDSVFactTransaction
    [System.CodeDom.Compiler.GeneratedCode("EF.Reverse.POCO.Generator", "2.33.0.0")]
    public partial class DW_vDSVFactTransaction
    {
        [Column(@"Period", Order = 1, TypeName = "date")]
        [Required]
        [Key]
        [Display(Name = "Period")]
        public System.DateTime Period { get; set; } // Period (Primary key)

        [Column(@"DimHotelId", Order = 2, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim hotel ID")]
        public int DimHotelId { get; set; } // DimHotelId (Primary key)

        [Column(@"DimScenarioId", Order = 3, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim scenario ID")]
        public int DimScenarioId { get; set; } // DimScenarioId (Primary key)

        [Column(@"DimMarketId", Order = 4, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim market ID")]
        public int DimMarketId { get; set; } // DimMarketId (Primary key)

        [Column(@"DimTransactionTypeId", Order = 5, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim transaction type ID")]
        public int DimTransactionTypeId { get; set; } // DimTransactionTypeId (Primary key)

        [Column(@"DimRoomClassId", Order = 6, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim room class ID")]
        public int DimRoomClassId { get; set; } // DimRoomClassId (Primary key)

        [Column(@"DebitValue", Order = 7, TypeName = "decimal")]
        [Required]
        [Key]
        [Display(Name = "Debit value")]
        public decimal DebitValue { get; set; } // DebitValue (Primary key)

        [Column(@"CreditValue", Order = 8, TypeName = "decimal")]
        [Required]
        [Key]
        [Display(Name = "Credit value")]
        public decimal CreditValue { get; set; } // CreditValue (Primary key)

        [Column(@"RN", Order = 9, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Rn")]
        public int RN { get; set; } // RN (Primary key)

        [MaxLength(11)]
        [StringLength(11)]
        [Display(Name = "Micro link")]
        public string MicroLink { get; set; } // MicroLink (length: 11)

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Check number")]
        public string CheckNumber { get; set; } // CheckNumber (length: 255)

        [Column(@"DimClubMemberId", Order = 12, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim club member ID")]
        public int DimClubMemberId { get; set; } // DimClubMemberId (Primary key)

        [Column(@"DimOperaTransactionId", Order = 13, TypeName = "int")]
        [Required]
        [Key]
        [Display(Name = "Dim opera transaction ID")]
        public int DimOperaTransactionId { get; set; } // DimOperaTransactionId (Primary key)

        [Display(Name = "Dim room type ID")]
        public int? DimRoomTypeId { get; set; } // DimRoomTypeId

        public DW_vDSVFactTransaction()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
