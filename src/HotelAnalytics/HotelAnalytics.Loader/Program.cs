﻿using HotelAnalytics.Logic.Importer;
using Serilog;
using System;
using System.Configuration;

namespace HotelAnalytics.Loader
{
	class Program
	{
		static void Main()
		{
			Log.Logger = new LoggerConfiguration()
			.MinimumLevel.Debug()
			.WriteTo.Console()
			.WriteTo.File(
				"Logs\\HotelAnalytics.Loader.log",
				outputTemplate: "{Timestamp} [{Level:u3}] [{SourceContext}.{Method}] {Message}{NewLine}{Exception}",
				rollingInterval: RollingInterval.Day
			)
			.CreateLogger();

			var log = Log.ForContext<Program>().ForContext("Method", "Main");
			log.Information("Begin");

			try
			{
				var username = "importer";
				var connectionString = ConfigurationManager.ConnectionStrings["HotelAnalytics"].ConnectionString;
				var hotelCode = ConfigurationManager.AppSettings["HotelCode"];
				var inputPath = ConfigurationManager.AppSettings["InputPath"];

				var importDate = DateTime.Now.ToString("yyyyMMdd_HHmmss");

				// configure data sources
				var dataSourceList = new DataSourceList
				{
					//// 1. MasterData / Setup Files
					{ DataSourceCodeEnum.H136, new DataSourceConfig(@"Set Up Files\H136 - Opera Market Codes Description") },
					{ DataSourceCodeEnum.H222, new DataSourceConfig(@"Set Up Files\H222 - Opera Market Mapping") },
					{ DataSourceCodeEnum.MarketCodeDefinitions, new DataSourceConfig(@"Set Up Files\MarketCodeDefinitions") },
					{ DataSourceCodeEnum.H196, new DataSourceConfig(@"Set Up Files\H196 - Opera Transaction Codes") },
					{ DataSourceCodeEnum.Recipe, new DataSourceConfig(@"Set Up Files\MC Recipes (can be compiled manually)") },
					{ DataSourceCodeEnum.H176, new DataSourceConfig(@"Set Up Files\H176 - Rom Type") },
					{ DataSourceCodeEnum.H226, new DataSourceConfig(@"Set Up Files\H226 - Opera Transaction Code Mapping") },
					//// 2. Facts
					//// Operations - Planning
					{ DataSourceCodeEnum.R122, new DataSourceConfig(@"-01- Income\-10- Rooms\R122 - TRA,DEF, TE1,TE2,PRO - Daily") },
					//// Operations - Actual
					{ DataSourceCodeEnum.D140, new DataSourceConfig(@"-01- Income\Audit Trail Report\D140 - Journal by Transactions - Daily") },
					{ DataSourceCodeEnum.F100, new DataSourceConfig(@"-01- Income\-10- Rooms\F100 - Actual Rooms  - Daily") },
					//// // { DataSourceCodeEnum.AD180, new DataSourceConfig(@"-01- Income\-10- Rooms\AD180 - Daily") },
					//// Operations - Budget/Forecast
					{ DataSourceCodeEnum.OperaPlanningBudget, new DataSourceConfig(@"BST - Budget - Forecast\Budget") },
					{ DataSourceCodeEnum.OperaPlanningForecast, new DataSourceConfig(@"BST - Budget - Forecast\Forecast") },
					//// CRM
					{ DataSourceCodeEnum.MicrosSummaryTransactions, new DataSourceConfig(@"CRM data Files\Micros Summary Transactions - Daily") },
					{ DataSourceCodeEnum.MicrosTenderMedia, new DataSourceConfig(@"CRM data Files\Micros Tender Media - Daily") },
					{ DataSourceCodeEnum.MicrosMenuItems, new DataSourceConfig(@"CRM data Files\Micros Menu Items - Daily") },
					{ DataSourceCodeEnum.MicrosMenuItemFamilies, new DataSourceConfig(@"CRM data Files\MicrosMenuItemFamilies") },
					{ DataSourceCodeEnum.ClubMembers, new DataSourceConfig(@"CRM data Files\ClubMembers") },
				};
				log.Information("Running Loader for data sources {DataSources}", dataSourceList.Keys);
				new Loader(username, connectionString, hotelCode, inputPath, dataSourceList, importDate).Run();
			}
			catch (Exception ex)
			{
				log.Error("Execution error with {exception}", ex);
			}
			finally
			{
				log.Information("End");
			}
		}
	}
}
