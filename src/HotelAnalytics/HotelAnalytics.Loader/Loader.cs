﻿using HotelAnalytics.Logic.Importer;
using HotelAnalytics.Logic.Importer.Structures;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace HotelAnalytics.Loader
{
	public class Loader
	{
		const string METHOD = "Method";
		const string PROCESSED = "Processed";
		const string ERROR = "Error";

		private readonly ILogger _classLogger = Log.ForContext<Loader>();

		public string Username { get; }
		public string ConnectionString { get; }
		public string HotelCode { get; }
		public string BasePath { get; }
		public DataSourceList DataSourceList { get; }
		public string ImportDate { get; }

		public Loader(string username, string connectionString, string hotelCode, string basePath, DataSourceList dataSourceList, string importDate)
		{
			Username = username;
			ConnectionString = connectionString;
			HotelCode = hotelCode;
			BasePath = basePath;
			DataSourceList = dataSourceList;
			ImportDate = importDate;
		}

		public void Run()
		{
			var log = _classLogger.ForContext(METHOD, nameof(Run));
			using (var context = new Model.HotelAnalytics(ConnectionString))
			{
				// cycle all configured data sources
				foreach (var dataSource in DataSourceList)
				{
					// for each data source process input folder
					log.Information("Cycling directory {directory}", $@"{BasePath}\{dataSource.Value.InputPath}");
					ProcessDirectory(context, dataSource.Key, $@"{BasePath}\{dataSource.Value.InputPath}");
				}
			}
		}

		private void ProcessDirectory(Model.HotelAnalytics context, DataSourceCodeEnum dataSourceCode, string directory)
		{
			var log = _classLogger.ForContext(METHOD, nameof(ProcessDirectory));
			var processedDirectory = $@"{directory}\{PROCESSED}\{ImportDate}";
			var errorDirectory = $@"{directory}\{ERROR}\{ImportDate}";

			// cycle and handle all files in this directory
			var dir = new DirectoryInfo(directory);

			foreach (var fi in dir.GetFiles().OrderBy((f) => f.LastWriteTimeUtc))
			{
				if (fi.Name.EndsWith(".ignore"))
				{
					log.Information("Ignoring file {file}", fi.FullName);
				}
				else
				{
					log.Information("Importing file {file}", fi.FullName);
					(var errors, var batchId) = ImportFile(context, dataSourceCode, fi.FullName, fi.Name);

					if (errors.Count == 0)
					{
						// if no errors, then moved to "Processed" directory
						Directory.CreateDirectory(processedDirectory);
						File.Move(fi.FullName, $@"{processedDirectory}\{fi.Name}");
					}
					else
					{
						// otherwise move to "Error" directory
						var errorFileName = $@"{errorDirectory}\error_log.tsv";
						Directory.CreateDirectory(errorDirectory);
						File.Move(fi.FullName, $@"{errorDirectory}\{fi.Name}");

						// append log to the error log file
						if (!File.Exists(errorFileName))
						{
							File.AppendAllText(errorFileName, "FILE_NAME\tBATCH_ID\tERROR_TYPE\tERROR_MSG\n");
						}
						var logBuilder = new StringBuilder();
						errors.ForEach(e => logBuilder.AppendLine($"{fi.Name}\t{batchId}\t{e.ErrorType}\t{e.ErrorMessage}"));
						File.AppendAllText(errorFileName, logBuilder.ToString());
						log.Error("Error(s) occurred. Check output log file and/or the database log tables.");
					}
				}
			}

			// cycle child directories and recursively call ProcessDirectory
			foreach (var childDir in dir.GetDirectories().Where(d => d.Name != PROCESSED && d.Name != ERROR))
			{
				if (childDir.FullName.EndsWith(".ignore"))
				{
					log.Information("Ignoring directory {directory}", childDir.FullName);
				}
				else
				{
					log.Information("Cycling directory {directory}", childDir.FullName);
					ProcessDirectory(context, dataSourceCode, childDir.FullName);
				}
			}
		}

		public (List<ErrorItem> errors, int batchId) ImportFile(Model.HotelAnalytics context, DataSourceCodeEnum dataSourceCode, string filePath, string fileName)
		{
			BaseImporter importer;

			if (dataSourceCode == DataSourceCodeEnum.H136)
			{
				importer = new H136Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.H222)
			{
				importer = new H222Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.R122)
			{
				importer = new R122Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.D140)
			{
				importer = new D140Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.AD180)
			{
				importer = new AD180Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.F100)
			{
				importer = new F100Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.OperaPlanningBudget)
			{
				importer = new OperaPlanningImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.OperaPlanningForecast)
			{
				importer = new OperaPlanningImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.AD180)
			{
				importer = new AD180Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.H196)
			{
				importer = new H196Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.MicrosSummaryTransactions)
			{
				importer = new MicrosSummaryTransactionsImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.MicrosTenderMedia)
			{
				importer = new MicrosTenderMediaImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.MicrosMenuItems)
			{
				importer = new MicrosMenuItemsImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.MicrosMenuItemFamilies)
			{
				importer = new MicrosMenuItemFamiliesImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.Recipe)
			{
				importer = new RecipeImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.ClubMembers)
			{
				importer = new ClubMembersImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.MarketCodeDefinitions)
			{
				importer = new MarketCodeDefinitionsImporter(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.H176)
			{
				importer = new H176Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else if (dataSourceCode == DataSourceCodeEnum.H226)
			{
				importer = new H226Importer(filePath, dataSourceCode.ToString(), HotelCode, fileName, DateTime.Now, Username, context);
			}
			else
			{
				throw new Exception($"Unknown dataSourceCode: {dataSourceCode}");
			}

			var batchId = importer.GetBatchId();
			try
			{
				var lValidateErrors = importer.ValidateStructure();
				if (lValidateErrors.Count == 0)
				{
					var lImporterErrors = importer.ImportToStaging();
					if (lImporterErrors.Count == 0)
					{
						var lValidateStaging = importer.ValidateStaging();
						if (lValidateStaging.Count == 0)
						{
							importer.ImportToDW();
						}
						else
						{
							importer.WriteBatchMessages(lValidateStaging);
							return (lValidateStaging, batchId);
						}
					}
					else
					{
						importer.WriteBatchMessages(lImporterErrors);
						return (lImporterErrors, batchId);
					}
				}
				else
				{
					importer.WriteBatchMessages(lValidateErrors);
					return (lValidateErrors, batchId);
				}

				return (new List<ErrorItem>(), batchId);
			}
			catch (Exception ex)
			{
				var errors = new List<ErrorItem>
				{
					new ErrorItem
					{
						ErrorType = "Exception",
						ErrorMessage = ex.ToString().Length > 4000 ? ex.ToString().Substring(0, 4000) : ex.ToString()
					}
				};
				importer.WriteBatchMessages(errors);
				return (errors, batchId);
			}
		}
	}
}
