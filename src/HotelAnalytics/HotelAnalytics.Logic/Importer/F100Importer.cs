﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class F100Importer : BaseImporterTextFileDelimited
	{
		public F100Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "GRP_SORT_ORDER\tGRP1_CODE_DESC\tPARENT_MARKET_CODE\tMARKET_GROUP_DESCRIPTION\tPM_DAY_ROOM\tPM_DAY_PERSON\tPM_MONTH_ROOM\tPM_MONTH_PERSON\tPM_YEAR_ROOM\tPM_YEAR_PRS\tPM_DAY_CREV\tPM_MONTH_CREV\tPM_YEAR_CREV\tPM_YEAR_ARR\tPM_MONTH_ARR\tPM_DAY_ARR\tPM_DAY_PER_OCC\tPM_MTD_PER_OCC\tPM_YTD_PER_OCC\tGRP2_CODE\tSUB_GRP_CODE_DESC\tDESCRIPTION\tGRAPH_X_CODE\tROOMS_DAY\tROOM_REV_DAY\tROOMS_MTD\tGUEST_MTD\tROOM_REV_MTD\tROOMS_YTD\tGUEST_YTD\tROOM_REV_YTD\tGUEST_DAY\tADR_DAY\tADR_MTD\tADR_YTD\tPER_OCC_DAY\tPER_OCC_MTD\tPER_OCC_YTD";

		protected int startline = 0;
		protected string stopconditionText = "S_DAY_ROOMS";
		protected char split = '\t';

		private DateTime GetParsedDate()
		{
			var tmp = FileName.Substring(0, 8);
			return DateTime.ParseExact($"{tmp} 00:00 -00:00", "ddMMyyyy HH:mm zzz", CultureInfo.InvariantCulture);
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);

			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}
				try
				{
					data.Rows.Add(record.Split(split));
				}
				catch (System.ArgumentException aEx) when (aEx.Message == "Input array is longer than the number of columns in this table.")
				{
					errorList.Add(new ErrorItem { ErrorType = "STRUCTURE", ErrorMessage = "Found unexpected data after column AQ." });
					return errorList;
				}
			}

			// Start load staging
			var rowsF100 = new List<Staging_F100>();
			foreach (DataRow dr in data.Rows)
			{
				rowsF100.Add(new Staging_F100
				{
					BatchId = BatchId,
					BUSINESS_DATE = GetParsedDate(),
					GRP_SORT_ORDER = GetString(dr["GRP_SORT_ORDER"].ToString()),
					GRP1_CODE_DESC = GetString(dr["GRP1_CODE_DESC"].ToString()),
					PARENT_MARKET_CODE = GetString(dr["PARENT_MARKET_CODE"].ToString()),
					MARKET_GROUP_DESCRIPTION = GetString(dr["MARKET_GROUP_DESCRIPTION"].ToString()),
					PM_DAY_ROOM = GetDecimal(dr["PM_DAY_ROOM"].ToString()),
					PM_DAY_PERSON = GetDecimal(dr["PM_DAY_PERSON"].ToString()),
					PM_MONTH_ROOM = GetDecimal(dr["PM_MONTH_ROOM"].ToString()),
					PM_MONTH_PERSON = GetDecimal(dr["PM_MONTH_PERSON"].ToString()),
					PM_YEAR_ROOM = GetDecimal(dr["PM_YEAR_ROOM"].ToString()),
					PM_YEAR_PRS = GetDecimal(dr["PM_YEAR_PRS"].ToString()),
					PM_DAY_CREV = GetDecimal(dr["PM_DAY_CREV"].ToString()),
					PM_MONTH_CREV = GetDecimal(dr["PM_MONTH_CREV"].ToString()),
					PM_YEAR_CREV = GetDecimal(dr["PM_YEAR_CREV"].ToString()),
					PM_YEAR_ARR = GetDecimal(dr["PM_YEAR_ARR"].ToString()),
					PM_MONTH_ARR = GetDecimal(dr["PM_MONTH_ARR"].ToString()),
					PM_DAY_ARR = GetDecimal(dr["PM_DAY_ARR"].ToString()),
					PM_DAY_PER_OCC = GetDecimal(dr["PM_DAY_PER_OCC"].ToString()),
					PM_MTD_PER_OCC = GetDecimal(dr["PM_MTD_PER_OCC"].ToString()),
					PM_YTD_PER_OCC = GetDecimal(dr["PM_YTD_PER_OCC"].ToString()),
					GRP2_CODE = GetString(dr["GRP2_CODE"].ToString()),
					SUB_GRP_CODE_DESC = GetString(dr["SUB_GRP_CODE_DESC"].ToString()),
					DESCRIPTION = GetString(dr["DESCRIPTION"].ToString()),
					GRAPH_X_CODE = GetString(dr["GRAPH_X_CODE"].ToString()),
					ROOMS_DAY = GetDecimal(dr["ROOMS_DAY"].ToString()),
					ROOM_REV_DAY = GetDecimal(dr["ROOM_REV_DAY"].ToString()),
					ROOMS_MTD = GetDecimal(dr["ROOMS_MTD"].ToString()),
					GUEST_MTD = GetDecimal(dr["GUEST_MTD"].ToString()),
					ROOM_REV_MTD = GetDecimal(dr["ROOM_REV_MTD"].ToString()),
					ROOMS_YTD = GetDecimal(dr["ROOMS_YTD"].ToString()),
					GUEST_YTD = GetDecimal(dr["GUEST_YTD"].ToString()),
					ROOM_REV_YTD = GetDecimal(dr["ROOM_REV_YTD"].ToString()),
					GUEST_DAY = GetDecimal(dr["GUEST_DAY"].ToString()),
					ADR_DAY = GetDecimal(dr["ADR_DAY"].ToString()),
					ADR_MTD = GetDecimal(dr["ADR_MTD"].ToString()),
					ADR_YTD = GetDecimal(dr["ADR_YTD"].ToString()),
					PER_OCC_DAY = GetDecimal(dr["PER_OCC_DAY"].ToString()),
					PER_OCC_MTD = GetDecimal(dr["PER_OCC_MTD"].ToString()),
					PER_OCC_YTD = GetDecimal(dr["PER_OCC_YTD"].ToString()),

				});
			}

			if (rowsF100.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsF100);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
