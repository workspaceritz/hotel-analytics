﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class H226Importer : BaseImporterTextFileDelimited
	{
		public H226Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "TRX_CODE\tDESCRIPTION\tBOF_TRANS_CODE\tBOF_TRANS_TEXT\tBOF_VALUE";

		protected int startline = 0;
		protected string stopconditionText = "CF_LOGO";
		protected char split = '\t';


		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			if (errorList.Count > 0)
			{
				return errorList;
			}


			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);
			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}

				data.Rows.Add(record.Split(split));
			}


			//Start load staging
			var rowsH226 = new List<Staging_H226>();
			foreach (DataRow dr in data.Rows)
			{
				rowsH226.Add(new Staging_H226
				{
					BatchId = BatchId,
					TRX_CODE = GetString(dr["TRX_CODE"].ToString()),
					DESCRIPTION = GetString(dr["DESCRIPTION"].ToString()),
					BOF_TRANS_CODE = GetString(dr["BOF_TRANS_CODE"].ToString()),
					BOF_TRANS_TEXT = GetString(dr["BOF_TRANS_TEXT"].ToString()),
					BOF_VALUE = GetString(dr["BOF_VALUE"].ToString()),
				});
			}


			if (rowsH226.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsH226);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
