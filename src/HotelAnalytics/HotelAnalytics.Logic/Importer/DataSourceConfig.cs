﻿namespace HotelAnalytics.Logic.Importer
{
	public class DataSourceConfig
	{
		public string InputPath { get; private set; }

		public DataSourceConfig(string inputPath)
		{
			InputPath = inputPath;
		}
	}
}
