﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class R122Importer : BaseImporterTextFileDelimited
	{
		const string METHOD = "Method";
		const string PROCESSED = "Processed";
		const string ERROR = "Error";

		private readonly ILogger _classLogger = Log.ForContext<R122Importer>();

		public R122Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "REPORT_ID\tRESERVATION_DATE\tCHAR_RESERVATION_DATE\tMARKET_CODE\tCF_MARKET_CODE_SEQ\tMARKET_CODE_DESC\tS_DEF_ROOMS_DAY\tS_DEF_ROOMS_MKT\tS_REV_DAY\tS_REV_MKT\tDAY_ROOMS\tTOT_ARR_DAY\tTOT_ARR_MKT\tARRIVAL_RMS_DAY\tARR_RMS_MKT\tGUEST_DAY\tGUEST_MKT\tDOUBLE_OCC_DAY\tDOUBLE_OCC_MKT\tSINGLE_OCC_DAY\tSINGLE_OCC_MKT\tPER_DOUBLE_MKT\tPER_DOUBLE_OCC_DAY\tPER_OCC_DAY\tPER_OCC_MKT\tCS_SUM_OO_MKT\tCS_AVG_OO_DAY\tNO_DEFINITE_ROOMS\tARRIVAL_RMS\tNO_OF_GUESTS\tSINGLE_OCCUPANCY\tMULTI_OCCUPANCY\tTOTAL_REVENUE\tOO_ROOMS\tPRINT_NO_OF_ROOMS\tGET_ARR\tPER_OCC\tMULTI_OCC_PER";

		protected int startline = 0;
		protected string stopconditionText = "S_DEF_ROOM_REPORT";
		protected char split = '\t';

		private string GetParsedReservationStatus()
		{
			var tmp = FileName.Replace("R122- ", "").Trim();
			return HotelAnalyticsContext.vLookupValues.FirstOrDefault((lv) => lv.LookupSet == "ReservationStatus_Mapping" && tmp.ToLower().Contains(lv.LookupValue.ToLower()))?.LookupMappedValue;
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			if (errorList.Count > 0)
			{
				return errorList;
			}

			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);
			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}

				data.Rows.Add(record.Split(split));
			}

			var reservationStatus = GetParsedReservationStatus();
			//Start load staging
			var rowsR122 = new List<Staging_R122>();
			foreach (DataRow dr in data.Rows)
			{
				rowsR122.Add(new Staging_R122
				{
					BatchId = BatchId,
					RESERVATION_STATUS = reservationStatus,
					REPORT_ID = GetString(dr["REPORT_ID"].ToString()),
					RESERVATION_DATE = GetDate(dr["RESERVATION_DATE"].ToString()),
					CHAR_RESERVATION_DATE = GetString(dr["CHAR_RESERVATION_DATE"].ToString()),
					MARKET_CODE = GetString(dr["MARKET_CODE"].ToString()),
					CF_MARKET_CODE_SEQ = GetString(dr["CF_MARKET_CODE_SEQ"].ToString()),
					MARKET_CODE_DESC = GetString(dr["MARKET_CODE_DESC"].ToString()),
					S_DEF_ROOMS_DAY = GetInt(dr["S_DEF_ROOMS_DAY"].ToString()),
					S_DEF_ROOMS_MKT = GetInt(dr["S_DEF_ROOMS_MKT"].ToString()),
					S_REV_DAY = GetDecimal(dr["S_REV_DAY"].ToString()),
					S_REV_MKT = GetDecimal(dr["S_REV_MKT"].ToString()),
					DAY_ROOMS = GetInt(dr["DAY_ROOMS"].ToString()),
					TOT_ARR_DAY = GetDecimal(dr["TOT_ARR_DAY"].ToString()),
					TOT_ARR_MKT = GetDecimal(dr["TOT_ARR_MKT"].ToString()),
					ARRIVAL_RMS_DAY = GetInt(dr["ARRIVAL_RMS_DAY"].ToString()),
					ARR_RMS_MKT = GetInt(dr["ARR_RMS_MKT"].ToString()),
					GUEST_DAY = GetInt(dr["GUEST_DAY"].ToString()),
					GUEST_MKT = GetInt(dr["GUEST_MKT"].ToString()),
					DOUBLE_OCC_DAY = GetInt(dr["DOUBLE_OCC_DAY"].ToString()),
					DOUBLE_OCC_MKT = GetInt(dr["DOUBLE_OCC_MKT"].ToString()),
					SINGLE_OCC_DAY = GetInt(dr["SINGLE_OCC_DAY"].ToString()),
					SINGLE_OCC_MKT = GetInt(dr["SINGLE_OCC_MKT"].ToString()),
					PER_DOUBLE_MKT = GetDecimal(dr["PER_DOUBLE_MKT"].ToString()),
					PER_DOUBLE_OCC_DAY = GetDecimal(dr["PER_DOUBLE_OCC_DAY"].ToString()),
					PER_OCC_DAY = GetDecimal(dr["PER_OCC_DAY"].ToString()),
					PER_OCC_MKT = GetDecimal(dr["PER_OCC_MKT"].ToString()),
					CS_SUM_OO_MKT = GetDecimal(dr["CS_SUM_OO_MKT"].ToString()),
					CS_AVG_OO_DAY = GetDecimal(dr["CS_AVG_OO_DAY"].ToString()),
					NO_DEFINITE_ROOMS = GetInt(dr["NO_DEFINITE_ROOMS"].ToString()),
					ARRIVAL_RMS = GetInt(dr["ARRIVAL_RMS"].ToString()),
					NO_OF_GUESTS = GetInt(dr["NO_OF_GUESTS"].ToString()),
					SINGLE_OCCUPANCY = GetInt(dr["SINGLE_OCCUPANCY"].ToString()),
					MULTI_OCCUPANCY = GetInt(dr["MULTI_OCCUPANCY"].ToString()),
					TOTAL_REVENUE = GetDecimal(dr["TOTAL_REVENUE"].ToString()),
					OO_ROOMS = GetInt(dr["OO_ROOMS"].ToString()),
					PRINT_NO_OF_ROOMS = GetInt(dr["PRINT_NO_OF_ROOMS"].ToString()),
					GET_ARR = GetInt(dr["GET_ARR"].ToString()),
					PER_OCC = GetInt(dr["PER_OCC"].ToString()),
					MULTI_OCC_PER = GetInt(dr["MULTI_OCC_PER"].ToString()),
				});
			}


			if (rowsR122.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsR122);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var log = _classLogger.ForContext(METHOD, nameof(ValidateStructure));
			var errorList = new List<ErrorItem>();

			// check if parsed reservation status exists in our mappings
			var reservationStatus = GetParsedReservationStatus();
			if (reservationStatus == null)
			{
				errorList.Add(new ErrorItem("STRUCTURE", "Unable to extract Reservation Status from file name"));
			}
			else
			{
				var reader = ReadAsLines(FilePath);

				var readerStart = reader.Skip(startline);

				if (hasHeader)
				{
					if (headerText != readerStart.First())
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
					}
				}
			}

			return errorList;
		}
	}
}
