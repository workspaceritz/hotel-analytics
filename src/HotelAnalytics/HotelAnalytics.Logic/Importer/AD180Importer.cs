﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class AD180Importer : BaseImporterTextFileDelimited
	{
		public AD180Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "PERIOD_START\tPERIOD_END\tPERIOD_DESCRIPTION\tVIRTUAL\tMASTER_ORDER\tORDER_BY\tSTATUS\tTOTAL_AVGRATE\tTOTAL_ROOMS\tTOTAL_REVENUE\tCUR_DATE\tEVENT_CODE\tNC_CODE\tRP_NC_CODE\tDAY_WORD\tDAY_NUMBER\tROOMS";

		protected int startline = 0;
		protected string stopconditionText = "LOGO";
		protected char split = '\t';


		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			if (errorList.Count > 0)
			{
				return errorList;
			}


			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);
			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}

				data.Rows.Add(record.Split(split));
			}


			//Start load staging
			var rowsAD180 = new List<Staging_AD180>();
			foreach (DataRow dr in data.Rows)
			{
				rowsAD180.Add(new Staging_AD180
				{
					BatchId = BatchId,
					PERIOD_START = GetString(dr["PERIOD_START"].ToString()),
					PERIOD_END = GetString(dr["PERIOD_END"].ToString()),
					PERIOD_DESCRIPTION = GetString(dr["PERIOD_DESCRIPTION"].ToString()),
					VIRTUAL = GetString(dr["VIRTUAL"].ToString()),
					MASTER_ORDER = GetString(dr["MASTER_ORDER"].ToString()),
					ORDER_BY = GetString(dr["ORDER_BY"].ToString()),
					STATUS = GetString(dr["STATUS"].ToString()),
					TOTAL_AVGRATE = GetDecimal(dr["TOTAL_AVGRATE"].ToString()),
					TOTAL_ROOMS = GetDecimal(dr["TOTAL_ROOMS"].ToString()),
					TOTAL_REVENUE = GetDecimal(dr["TOTAL_REVENUE"].ToString()),
					CUR_DATE = GetDate(dr["CUR_DATE"].ToString()),
					EVENT_CODE = GetString(dr["EVENT_CODE"].ToString()),
					NC_CODE = GetString(dr["NC_CODE"].ToString()),
					RP_NC_CODE = GetString(dr["RP_NC_CODE"].ToString()),
					DAY_WORD = GetString(dr["DAY_WORD"].ToString()),
					DAY_NUMBER = GetString(dr["DAY_NUMBER"].ToString()),
					ROOMS = GetString(dr["ROOMS"].ToString())


				});
			}


			if (rowsAD180.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsAD180);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
