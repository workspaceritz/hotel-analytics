﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class H136Importer : BaseImporterTextFileDelimited
	{
		public H136Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "PARENT_MARKET_CODE\tSELL_SEQUENCE\tMARKET_CODE\tDESCRIPTION\tDISPLAY_COLOR\tINACTIVE_YN";

		protected int startline = 0;
		protected string stopconditionText = "CF_LOGO";
		protected char split = '\t';


		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			if (errorList.Count > 0)
			{
				return errorList;
			}


			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);
			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}

				data.Rows.Add(record.Split(split));
			}


			//Start load staging
			var rowsH136 = new List<Staging_H136>();
			foreach (DataRow dr in data.Rows)
			{
				rowsH136.Add(new Staging_H136
				{
					BatchId = BatchId,
					PARENT_MARKET_CODE = GetString(dr["PARENT_MARKET_CODE"].ToString()),
					SELL_SEQUENCE = GetInt(dr["SELL_SEQUENCE"].ToString()),
					MARKET_CODE = GetString(dr["MARKET_CODE"].ToString()),
					DESCRIPTION = GetString(dr["DESCRIPTION"].ToString()),
					DISPLAY_COLOR = GetString(dr["DISPLAY_COLOR"].ToString()),
					INACTIVE_YN = GetString(dr["INACTIVE_YN"].ToString()),
				});
			}


			if (rowsH136.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsH136);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
