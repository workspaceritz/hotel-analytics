﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class D140Importer : BaseImporterTextFileDelimited
	{
		public D140Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "IS_INTERNAL_YN\tINTERNAL_DEBIT\tINTERNAL_CREDIT\tFIRST\tFIRST_DEBIT\tFIRST_CREDIT\tSECOND\tSECOND_DEBIT\tSECOND_CREDIT\tTHIRD\tTHIRD_DEBIT\tTHIRD_CREDIT\tEXP_DATE\tRECEIPT_NO\tGUEST_FULL_NAME\tTARGET_RESORT\tTRX_DESC\tMARKET_CODE\tBUSINESS_FORMAT_DATE\tBUSINESS_TIME\tBUSINESS_DATE\tREFERENCE\tTRX_NO\tCASHIER_DEBIT\tCASHIER_CREDIT\tROOM\tCREDIT_CARD_SUPPLEMENT\tCURRENCY1\tTRX_CODE\tCASHIER_ID\tREMARK\tINSERT_USER\tINSERT_DATE\tCHEQUE_NUMBER\tROOM_CLASS\tCC_CODE\tCASHIER_NAME\tUSER_NAME\tDEP_NET_TAX_AMT\tDEPOSIT_DEBIT\tCASH_ID_USER_NAME\tPRINT_CASHIER_DEBIT\tPRINT_CASHIER_CREDIT";

		protected int startline = 0;
		protected string stopconditionText = "R_DEBIT";
		protected char split = '\t';

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);

			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}
				try
				{
					data.Rows.Add(record.Split(split));
				}
				catch (System.ArgumentException aEx) when (aEx.Message == "Input array is longer than the number of columns in this table.")
				{
					errorList.Add(new ErrorItem { ErrorType = "STRUCTURE", ErrorMessage = "Found unexpected data after column AQ." });
					return errorList;
				}
			}

			// Start load staging
			var rowsD140 = new List<Staging_D140>();
			foreach (DataRow dr in data.Rows)
			{
				rowsD140.Add(new Staging_D140
				{
					BatchId = BatchId,
					IS_INTERNAL_YN = GetString(dr["IS_INTERNAL_YN"].ToString()),
					INTERNAL_DEBIT = GetDecimal(dr["INTERNAL_DEBIT"].ToString()),
					INTERNAL_CREDIT = GetDecimal(dr["INTERNAL_CREDIT"].ToString()),
					FIRST = GetDecimal(dr["FIRST"].ToString()),
					FIRST_DEBIT = GetDecimal(dr["FIRST_DEBIT"].ToString()),
					FIRST_CREDIT = GetDecimal(dr["FIRST_CREDIT"].ToString()),
					SECOND = GetDecimal(dr["SECOND"].ToString()),
					SECOND_DEBIT = GetDecimal(dr["SECOND_DEBIT"].ToString()),
					SECOND_CREDIT = GetDecimal(dr["SECOND_CREDIT"].ToString()),

					THIRD = GetDecimal(dr["THIRD"].ToString()),
					THIRD_DEBIT = GetDecimal(dr["THIRD_DEBIT"].ToString()),
					THIRD_CREDIT = GetDecimal(dr["THIRD_CREDIT"].ToString()),

					EXP_DATE = GetString(dr["EXP_DATE"].ToString()),
					RECEIPT_NO = GetString(dr["RECEIPT_NO"].ToString()),
					GUEST_FULL_NAME = GetString(dr["GUEST_FULL_NAME"].ToString()),
					TARGET_RESORT = GetString(dr["TARGET_RESORT"].ToString()),
					TRX_DESC = GetString(dr["TRX_DESC"].ToString()),
					MARKET_CODE = GetString(dr["MARKET_CODE"].ToString()),
					BUSINESS_FORMAT_DATE = GetString(dr["BUSINESS_FORMAT_DATE"].ToString()),
					BUSINESS_TIME = GetString(dr["BUSINESS_TIME"].ToString()),
					BUSINESS_DATE = GetString(dr["BUSINESS_DATE"].ToString()),
					REFERENCE = GetString(dr["REFERENCE"].ToString()),
					TRX_NO = GetString(dr["TRX_NO"].ToString()),

					CASHIER_DEBIT = GetDecimal(dr["CASHIER_DEBIT"].ToString()),
					CASHIER_CREDIT = GetDecimal(dr["CASHIER_CREDIT"].ToString()),

					ROOM = GetString(dr["ROOM"].ToString()),
					CREDIT_CARD_SUPPLEMENT = GetString(dr["CREDIT_CARD_SUPPLEMENT"].ToString()),
					CURRENCY1 = GetString(dr["CURRENCY1"].ToString()),
					TRX_CODE = GetString(dr["TRX_CODE"].ToString()),
					CASHIER_ID = GetString(dr["CASHIER_ID"].ToString()),
					REMARK = GetString(dr["REMARK"].ToString()),
					INSERT_USER = GetString(dr["INSERT_USER"].ToString()),
					INSERT_DATE = GetString(dr["INSERT_DATE"].ToString()),
					CHEQUE_NUMBER = GetString(dr["CHEQUE_NUMBER"].ToString()),
					ROOM_CLASS = GetString(dr["ROOM_CLASS"].ToString()),
					CC_CODE = GetString(dr["CC_CODE"].ToString()),
					CASHIER_NAME = GetString(dr["CASHIER_NAME"].ToString()),
					USER_NAME = GetString(dr["USER_NAME"].ToString()),
					DEP_NET_TAX_AMT = GetString(dr["DEP_NET_TAX_AMT"].ToString()),
					DEPOSIT_DEBIT = GetString(dr["DEPOSIT_DEBIT"].ToString()),
					CASH_ID_USER_NAME = GetString(dr["CASH_ID_USER_NAME"].ToString()),
					PRINT_CASHIER_DEBIT = GetString(dr["PRINT_CASHIER_DEBIT"].ToString()),
					PRINT_CASHIER_CREDIT = GetString(dr["PRINT_CASHIER_CREDIT"].ToString()),
					//EXTRA_COL = GetString(dr["EXTRA_COL"].ToString()),
				});
			}

			if (rowsD140.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsD140);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
