﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace HotelAnalytics.Logic.Importer
{
	public class MicrosSummaryTransactionsImporter : BaseImporterExcelFile
	{
		public int Year { get; private set; }
		#region Constants

		// positioning
		private const int HeaderRow = 10;
		private const int FirstDataRow = 12;
		private const int FirstDataColumn = 1;

		#endregion

		public MicrosSummaryTransactionsImporter(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					// validate structure

					// find sheet by name
					var ws = ep.Workbook.Worksheets.FirstOrDefault(myWs => myWs.Name == "Sheet1");

					if (ws == null)
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Expecting 'Sheet1' sheet."));
					}

					//validate Business Date - is date
					DateTime businessDate = GetDate(ws.Cells[8, 2].Value.ToString());
					if(businessDate == null)
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Unable to extract business date."));
					}

					return errorList;
				}
			}
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = ValidateStructure();

			if (errorList.Count > 0)
			{
				return errorList;
			}

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					var ws = ep.Workbook.Worksheets.First(myWs => myWs.Name == "Sheet1");
					var rows = new List<Staging_MicrosSummaryTransaction>();
					var LastRow = this.LastRow(ws);
					var LastCol = this.LastCol(ws);
					var range = ws.Cells[FirstDataRow, FirstDataColumn, LastRow, LastCol];

					DateTime businessDate = GetDate(ws.Cells[8, 2].Value.ToString());

					for (var i = FirstDataRow; i <= (LastRow); i++)
					{
						rows.Add(new Staging_MicrosSummaryTransaction
						{
							BatchId = BatchId,
							Location = GetString(ws.Cells[i, 2].Value?.ToString()),
							BusinessDates = businessDate,
							RevenueCenter = GetString(ws.Cells[i, 3].Value?.ToString()),
							OrderType = GetString(ws.Cells[i, 4].Value?.ToString()),
							OpenDateTime = GetDate(ws.Cells[i, 5].Value?.ToString()),
							CloseDateTime = GetDate(ws.Cells[i, 6].Value?.ToString()),
							OpenBusinessDate = GetDate(ws.Cells[i, 7].Value?.ToString()),
							CloseBusinessDate = GetDate(ws.Cells[i, 8].Value?.ToString()),
							CustomerNumber = GetString(ws.Cells[i, 9].Value?.ToString()),
							CustomerName = GetString(ws.Cells[i, 10].Value?.ToString()),
							CheckNumber = GetString(ws.Cells[i, 11].Value?.ToString()),
							CheckReference = GetString(ws.Cells[i, 12].Value?.ToString()),
							TableReference = GetString(ws.Cells[i, 13].Value?.ToString()),
							TableGroupNumber = GetString(ws.Cells[i, 14].Value?.ToString()),
							Employee = GetString(ws.Cells[i, 15].Value?.ToString()),
							NumberofGuests = GetInt(ws.Cells[i, 16].Value?.ToString()),
							NumberofItems  = GetInt(ws.Cells[i, 17].Value?.ToString()),
							DiscountTotal =  GetDecimal(ws.Cells[i, 18].Value?.ToString()),
							TipTotal = GetDecimal(ws.Cells[i, 19].Value?.ToString()),
							TaxTotal = GetDecimal(ws.Cells[i, 20].Value?.ToString()),
							SubTotal = GetDecimal(ws.Cells[i, 21].Value?.ToString()),
							CheckTotal = GetDecimal(ws.Cells[i, 22].Value?.ToString()),
							TaxExemptTotal = GetDecimal(ws.Cells[i, 23].Value?.ToString()),
							TaxExemptReference = GetString(ws.Cells[i, 24].Value?.ToString()),
							VoidsTotal = GetDecimal(ws.Cells[i, 25].Value?.ToString()),
							ManagerVoidsTotal = GetDecimal(ws.Cells[i,26].Value?.ToString()),
							ErrorCorrectTotal = GetDecimal(ws.Cells[i, 27].Value?.ToString()),
							InfoLines = GetString(ws.Cells[i, 28].Value?.ToString()),
							CheckDuration = GetDecimal(ws.Cells[i, 29].Value?.ToString()),
							DiningTime = GetDecimal(ws.Cells[i, 30].Value?.ToString()),
							TransferToCheckNumber = GetString(ws.Cells[i, 31].Value?.ToString()),
							NumberofTimePrinted = GetInt(ws.Cells[i, 32].Value?.ToString()),
							KDSStation = GetString(ws.Cells[i, 33].Value?.ToString()),
							PreparationTime = GetDecimal(ws.Cells[i, 34].Value?.ToString()),
							IVA6 = GetDecimal(ws.Cells[i, 35].Value?.ToString()),
							IVA13 = GetDecimal(ws.Cells[i, 36].Value?.ToString()),
							IVA23 = GetDecimal(ws.Cells[i, 37].Value?.ToString()),
							TaxRate4 = GetDecimal(ws.Cells[i, 38].Value?.ToString()),
							TaxRate5 = GetDecimal(ws.Cells[i, 39].Value?.ToString()),
							TaxRate6 = GetDecimal(ws.Cells[i, 40].Value?.ToString()),
							TaxRate7 = GetDecimal(ws.Cells[i, 41].Value?.ToString()),
							TaxRate8 = GetDecimal(ws.Cells[i, 42].Value?.ToString()),
						

						}); 
					}

					if (rows.Count > 0)
					{
						((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rows);
					}
				}
			}
			return errorList;
		}
	}
}
