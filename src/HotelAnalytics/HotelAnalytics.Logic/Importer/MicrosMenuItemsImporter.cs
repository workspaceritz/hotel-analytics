﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class MicrosMenuItemsImporter : BaseImporterExcelFile
	{
		public int Year { get; private set; }
		#region Constants

		// positioning
		private const int HeaderRow = 10;
		private const int FirstDataRow = 12;
		private const int FirstDataColumn = 1;


		#endregion

		public MicrosMenuItemsImporter(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					// validate structure

					// find sheet by name
					var ws = ep.Workbook.Worksheets.FirstOrDefault(myWs => myWs.Name == "Sheet1");

					if (ws == null)
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Expecting 'Sheet1' sheet."));
					}

					//validate Business Date - is date
					if (ws.Cells[8, 2].Value != null)
					{
						DateTime businessDate = GetDate(ws.Cells[8, 2].Value.ToString());
						if (businessDate == null)
						{
							errorList.Add(new ErrorItem("STRUCTURE", "Unable to extract business date."));
						}
					}
					else
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Unable to extract business date."));
					}


					return errorList;
				}
			}
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = ValidateStructure();

			if (errorList.Count > 0)
			{
				return errorList;
			}

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					var ws = ep.Workbook.Worksheets.First(myWs => myWs.Name == "Sheet1");
					var rows = new List<Staging_MicrosMenuItem>();
					var LastRow = this.LastRow(ws);
					var LastCol = this.LastCol(ws);
					var range = ws.Cells[FirstDataRow, FirstDataColumn, LastRow, LastCol];

					DateTime businessDate = GetDate(ws.Cells[8, 2].Value.ToString());

					for (var i = FirstDataRow; i <= (LastRow); i++)
					{
						rows.Add(new Staging_MicrosMenuItem
						{
							BatchId = BatchId,
							BusinessDates = businessDate,
							Location = GetString(ws.Cells[i, 2].Value?.ToString()),
							RevenueCenter = GetString(ws.Cells[i, 3].Value?.ToString()),
							OrderType = GetString(ws.Cells[i, 4].Value?.ToString()),
							CheckNumber = GetString(ws.Cells[i, 5].Value?.ToString()),
							BusinessDate = GetDate(ws.Cells[i, 6].Value?.ToString()),
							TransactionDateTime = GetDate(ws.Cells[i, 7].Value?.ToString()),
							SeatNumber = GetString(ws.Cells[i, 8].Value?.ToString()),
							PriceLevel = GetString(ws.Cells[i, 9].Value?.ToString()),
							ItemName = GetString(ws.Cells[i, 10].Value?.ToString()),
							ItemNumber = GetString(ws.Cells[i, 11].Value?.ToString()),
							LineCount = GetInt(ws.Cells[i, 12].Value?.ToString()),
							LineTotal = GetDecimal(ws.Cells[i, 13].Value?.ToString()),
							LineTotal1 = GetDecimal(ws.Cells[i, 14].Value?.ToString()),
							ReportLineCount = GetInt(ws.Cells[i, 15].Value?.ToString()),
							ReportLineTotal = GetDecimal(ws.Cells[i, 16].Value?.ToString()),
							Voids = GetString(ws.Cells[i, 17].Value?.ToString()),
							ManagerVoids = GetString(ws.Cells[i, 18].Value?.ToString()),
							Returns = GetString(ws.Cells[i, 19].Value?.ToString()),
							CheckEmployee = GetString(ws.Cells[i, 20].Value?.ToString()),
							TransactionEmployee = GetString(ws.Cells[i, 21].Value?.ToString()),
							AuthorizationEmployee = GetString(ws.Cells[i, 22].Value?.ToString()),
							MealEmployee = GetString(ws.Cells[i, 23].Value?.ToString()),
							ReasonCode = GetString(ws.Cells[i, 24].Value?.ToString()),
							ReferenceInfo = GetString(ws.Cells[i, 25].Value?.ToString()),
							SwipeType = GetString(ws.Cells[i, 26].Value?.ToString()),
						});
					}

					if (rows.Count > 0)
					{
						((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rows);
					}
				}
			}
			return errorList;
		}
	}
}
