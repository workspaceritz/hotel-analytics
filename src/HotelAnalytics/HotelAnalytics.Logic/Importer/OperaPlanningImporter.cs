﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace HotelAnalytics.Logic.Importer
{
	public class OperaPlanningImporter : BaseImporterExcelFile
	{
		public int Year { get; private set; }
		#region Constants
		private const string PSBF = "PSBF";

		// positioning
		private const int HeaderRow = 1;
		private const int FirstDataRow = 2;
		private const int FirstDataColumn = 1;

		// column indexes
		private const int PSBF_LineCount = 1;
		private const int PSBF_PlanningBU = 2;
		private const int PSBF_PlanningModelID = 3;
		private const int PSBF_PlanningCenter = 4;
		private const int PSBF_Activity = 5;
		private const int PSBF_Scenario = 6;
		private const int PSBF_Version = 7;
		private const int PSBF_OperatingUnit = 8;
		private const int PSBF_Department = 9;
		private const int PSBF_LegalEntity = 10;
		private const int PSBF_Account = 11;
		private const int PSBF_CY_FirstMonth = 12;
		#endregion

		public OperaPlanningImporter(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					// validate structure

					// find sheet by name
					var ws = ep.Workbook.Worksheets.FirstOrDefault(myWs => myWs.Name == PSBF);

					if (ws == null)
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Expecting 'PSBF' sheet."));
					}

					// check headers: first columns are stable
					var HeaderControl = new Dictionary<int, string>
					{
						{ PSBF_LineCount, "Line Count" },
						{ PSBF_PlanningBU, "Planning BU" },
						{ PSBF_PlanningModelID, "Planning Model ID" },
						{ PSBF_PlanningCenter, "Planning Center" },
						{ PSBF_Activity, "Activity" },
						{ PSBF_Scenario, "Scenario" },
						{ PSBF_Version, "Version" },
						{ PSBF_OperatingUnit, "Operating Unit" },
						{ PSBF_Department, "Department" },
						{ PSBF_LegalEntity, "Legal Entity" },
						{ PSBF_Account, "Account" },
					};

					for (var j = 1; j < PSBF_CY_FirstMonth; j++)
					{
						if (!ws.Cells[HeaderRow, j].Value.ToString().EndsWith(HeaderControl[j]))
						{
							errorList.Add(new ErrorItem("STRUCTURE", "Header #" + j + " (" + ws.Cells[HeaderRow, j].Value.ToString() + ") on PSBF sheet is not according. Expected: (ending with) " + HeaderControl[j].ToString()));
						}
					}

					// check and extract year 
					if (int.TryParse(ws.Cells[HeaderRow, PSBF_CY_FirstMonth].Value.ToString().Substring(0, 4), out var myYear))
					{
						Year = myYear;

					}
					else
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Unable to extract year."));
					}

					return errorList;
				}
			}
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = ValidateStructure();

			if (errorList.Count > 0)
			{
				return errorList;
			}

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					var ws = ep.Workbook.Worksheets.First(myWs => myWs.Name == PSBF);
					var rows = new List<Staging_OperaPlanning>();
					var LastRow = this.LastRow(ws);
					var LastCol = this.LastCol(ws);
					var range = ws.Cells[FirstDataRow, FirstDataColumn, LastRow, LastCol];

					// map column indexes to the months
					var cyMap = new Dictionary<int, int?> {
						{ 1, null },
						{ 2, null },
						{ 3, null },
						{ 4, null },
						{ 5, null },
						{ 6, null },
						{ 7, null },
						{ 8, null },
						{ 9, null },
						{ 10, null },
						{ 11, null },
						{ 12, null },
					};
					var nyMap = new Dictionary<int, int?> {
						{ 1, null },
						{ 2, null },
						{ 3, null },
						{ 4, null },
						{ 5, null },
						{ 6, null },
					};
					int? PSBF_Comments = null;

					var cyRegEx = new Regex(
						"(?<Year>\\d{4}) - M (?<Month>\\d{1,2})",
						RegexOptions.CultureInvariant | RegexOptions.Compiled
					);

					var nyRegEx = new Regex(
						"NY - M (?<Month>\\d{1,2})",
						RegexOptions.CultureInvariant | RegexOptions.Compiled
					);

					for (var j = PSBF_CY_FirstMonth; j <= ws.Dimension.End.Column; j++)
					{
						var colName = ws.Cells[HeaderRow, j].Value.ToString();
						// is it a CY column ?
						if (cyRegEx.IsMatch(colName))
						{
							var y = Convert.ToInt32(cyRegEx.Match(colName).Groups["Month"].Value);
							if (cyMap.ContainsKey(y))
							{
								cyMap[y] = j;
							}
						}
						// is it a NY column ?
						else if (nyRegEx.IsMatch(colName))
						{
							var y = Convert.ToInt32(nyRegEx.Match(colName).Groups["Month"].Value);
							if (nyMap.ContainsKey(y))
							{
								nyMap[y] = j;
							}
						}
						else
						// is it the Comments column ?
						if (colName == "Comments")
						{
							PSBF_Comments = j;
						}
					}

					for (var i = FirstDataRow; i <= (LastRow); i++)
					{
						rows.Add(new Staging_OperaPlanning
						{
							BatchId = BatchId,
							LineCount = GetInt(ws.Cells[i, PSBF_LineCount].Value?.ToString()),
							PlanningBU = ws.Cells[i, PSBF_PlanningBU].Value?.ToString(),
							PlanningModelID = ws.Cells[i, PSBF_PlanningModelID].Value?.ToString(),
							PlanningCenter = ws.Cells[i, PSBF_PlanningCenter].Value?.ToString(),
							Activity = ws.Cells[i, PSBF_Activity].Value?.ToString(),
							Scenario = ws.Cells[i, PSBF_Scenario].Value?.ToString(),
							Version = ws.Cells[i, PSBF_Version].Value?.ToString(),
							OperatingUnit = ws.Cells[i, PSBF_OperatingUnit].Value?.ToString(),
							Department = ws.Cells[i, PSBF_Department].Value?.ToString(),
							LegalEntity = ws.Cells[i, PSBF_LegalEntity].Value?.ToString(),
							Account = ws.Cells[i, PSBF_Account].Value?.ToString(),
							Year = Year,
							CY_M01 = cyMap[1].HasValue ? GetDecimal(ws.Cells[i, cyMap[1].Value].Value?.ToString()) : null,
							CY_M02 = cyMap[2].HasValue ? GetDecimal(ws.Cells[i, cyMap[2].Value].Value?.ToString()) : null,
							CY_M03 = cyMap[3].HasValue ? GetDecimal(ws.Cells[i, cyMap[3].Value].Value?.ToString()) : null,
							CY_M04 = cyMap[4].HasValue ? GetDecimal(ws.Cells[i, cyMap[4].Value].Value?.ToString()) : null,
							CY_M05 = cyMap[5].HasValue ? GetDecimal(ws.Cells[i, cyMap[5].Value].Value?.ToString()) : null,
							CY_M06 = cyMap[6].HasValue ? GetDecimal(ws.Cells[i, cyMap[6].Value].Value?.ToString()) : null,
							CY_M07 = cyMap[7].HasValue ? GetDecimal(ws.Cells[i, cyMap[7].Value].Value?.ToString()) : null,
							CY_M08 = cyMap[8].HasValue ? GetDecimal(ws.Cells[i, cyMap[8].Value].Value?.ToString()) : null,
							CY_M09 = cyMap[9].HasValue ? GetDecimal(ws.Cells[i, cyMap[9].Value].Value?.ToString()) : null,
							CY_M10 = cyMap[10].HasValue ? GetDecimal(ws.Cells[i, cyMap[10].Value].Value?.ToString()) : null,
							CY_M11 = cyMap[11].HasValue ? GetDecimal(ws.Cells[i, cyMap[11].Value].Value?.ToString()) : null,
							CY_M12 = cyMap[12].HasValue ? GetDecimal(ws.Cells[i, cyMap[12].Value].Value?.ToString()) : null,
							NY_M01 = nyMap[1].HasValue ? GetDecimal(ws.Cells[i, nyMap[1].Value].Value?.ToString()) : null,
							NY_M02 = nyMap[2].HasValue ? GetDecimal(ws.Cells[i, nyMap[2].Value].Value?.ToString()) : null,
							NY_M03 = nyMap[3].HasValue ? GetDecimal(ws.Cells[i, nyMap[3].Value].Value?.ToString()) : null,
							NY_M04 = nyMap[4].HasValue ? GetDecimal(ws.Cells[i, nyMap[4].Value].Value?.ToString()) : null,
							NY_M05 = nyMap[5].HasValue ? GetDecimal(ws.Cells[i, nyMap[5].Value].Value?.ToString()) : null,
							NY_M06 = nyMap[6].HasValue ? GetDecimal(ws.Cells[i, nyMap[6].Value].Value?.ToString()) : null,
							Comments = PSBF_Comments.HasValue ? ws.Cells[i, PSBF_Comments.Value].Value?.ToString() : null,
						});
					}

					if (rows.Count > 0)
					{
						((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rows);
					}
				}
			}
			return errorList;
		}
	}
}
