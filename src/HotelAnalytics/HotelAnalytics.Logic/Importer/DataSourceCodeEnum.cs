﻿
namespace HotelAnalytics.Logic.Importer
{
	public class DataSourceCodeEnum
	{
		private readonly string _value;

		private DataSourceCodeEnum(string value)
		{
			_value = value;
		}

		public override string ToString()
		{
			return _value;
		}

		// These values MUST match the database values for DataSource.DataSourceCode
		public static DataSourceCodeEnum H136 = new DataSourceCodeEnum("H136");
		public static DataSourceCodeEnum H222 = new DataSourceCodeEnum("H222");
		public static DataSourceCodeEnum R122 = new DataSourceCodeEnum("R122");
		public static DataSourceCodeEnum D140 = new DataSourceCodeEnum("D140");
		public static DataSourceCodeEnum AD180 = new DataSourceCodeEnum("AD180");
		public static DataSourceCodeEnum F100 = new DataSourceCodeEnum("F100");
		public static DataSourceCodeEnum OperaPlanningBudget = new DataSourceCodeEnum("OperaPlanningBudget");
		public static DataSourceCodeEnum OperaPlanningForecast = new DataSourceCodeEnum("OperaPlanningForecast");
		public static DataSourceCodeEnum H196 = new DataSourceCodeEnum("H196");
		public static DataSourceCodeEnum MicrosSummaryTransactions = new DataSourceCodeEnum("MicrosSummaryTransactions");
		public static DataSourceCodeEnum MicrosMenuItems = new DataSourceCodeEnum("MicrosMenuItems");
		public static DataSourceCodeEnum MicrosTenderMedia = new DataSourceCodeEnum("MicrosTenderMedia");
		public static DataSourceCodeEnum MicrosMenuItemFamilies = new DataSourceCodeEnum("MicrosMenuItemFamilies");
		public static DataSourceCodeEnum Recipe = new DataSourceCodeEnum("Recipe");
		public static DataSourceCodeEnum ClubMembers = new DataSourceCodeEnum("ClubMembers");
		public static DataSourceCodeEnum MarketCodeDefinitions = new DataSourceCodeEnum("MarketCodeDefinitions");
		public static DataSourceCodeEnum H176 = new DataSourceCodeEnum("H176");
		public static DataSourceCodeEnum H226 = new DataSourceCodeEnum("H226");

	}
}
