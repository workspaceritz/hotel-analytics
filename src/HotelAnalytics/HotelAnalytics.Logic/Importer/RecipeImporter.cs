﻿using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class RecipeImporter : BaseImporterExcelLegacyFile
	{
		#region Constants

		private const string SheetName = "Data$";

		#endregion

		private Dictionary<int, string> Columns { get; } = new Dictionary<int, string>
		{
			{ 0, "A" },
			{ 1, "Recipe No" },
			{ 2, "Recipe" },
			{ 3, "T" },
			{ 4, "Article No" },
			{ 5, "Component" },
			{ 6, "POT QTY" },
			{ 7, "Unit" },
			{ 8, "Text" },
			{ 9, "Loss" },
			{ 10, "ACT QTY" },
			{ 11, "QTY/BU" },
			{ 12, "BU" },
			{ 13, "AVE" },
			{ 14, "COS" },
			{ 15, "Note" },
			{ 16, "Pos" },
			{ 17, "Planned Price" },
			{ 18, "Sum Planned Price" },
			{ 19, "Future Planned Price" },
			{ 20, "Sum Future Planned Price" },
		};

		public RecipeImporter(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		private DataTable FillDataTable(OleDbCommand cmd, OleDbDataAdapter oda)
		{
			var dt = new DataTable(SheetName);
			cmd.CommandText = $"SELECT * FROM [{SheetName}]";
			oda.SelectCommand = cmd;
			oda.Fill(dt);
			dt.TableName = SheetName;
			return dt;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			using (var con = new OleDbConnection(ConnectionString))
			{
				using (var cmd = new OleDbCommand())
				{
					using (var oda = new OleDbDataAdapter())
					{
						cmd.Connection = con;
						con.Open();
						var dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
						var z = dtExcelSchema.AsEnumerable().ToList();
						if (dtExcelSchema.AsEnumerable().Any((r) => r["TABLE_NAME"].ToString() == SheetName))
						{
							var dt = FillDataTable(cmd, oda);

							for (var i = 0; i < dt.Columns.Count; i++)
							{
								if (!dt.Columns[i].ColumnName.StartsWith(Columns[i]))
								{
									errorList.Add(new ErrorItem { ErrorType = "STRUCTURE", ErrorMessage = $"Unexpected column {dt.Columns[i].ColumnName} on index {i}" });
								}
							}
						}
						else
						{
							errorList.Add(new ErrorItem { ErrorType = "STRUCTURE", ErrorMessage = $"Could not find sheet {SheetName}" });
						}
					}
				}
			}
			return errorList;
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = ValidateStructure();

			if (errorList.Count > 0)
			{
				return errorList;
			}

			using (var con = new OleDbConnection(ConnectionString))
			{
				using (var cmd = new OleDbCommand())
				{
					using (var oda = new OleDbDataAdapter())
					{
						cmd.Connection = con;
						con.Open();
						var dt = FillDataTable(cmd, oda);

						var rows = new List<Staging_Recipe>();
						for (var i = 0; i < dt.Rows.Count; i++)
						{
							rows.Add(new Staging_Recipe
							{
								BatchId = BatchId,
								A = GetString(dt.Rows[i][0].ToString()),
								RecipeNo = GetString(dt.Rows[i][1].ToString()),
								Recipe = GetString(dt.Rows[i][2].ToString()),
								T = GetString(dt.Rows[i][3].ToString()),
								ArticleNo = GetString(dt.Rows[i][4].ToString()),
								Component = GetString(dt.Rows[i][5].ToString()),
								POTQTY = GetDecimal(dt.Rows[i][6].ToString()),
								Unit = GetString(dt.Rows[i][7].ToString()),
								Text = GetString(dt.Rows[i][8].ToString()),
								Loss = GetString(dt.Rows[i][9].ToString()),
								ACTQTY = GetDecimal(dt.Rows[i][10].ToString()),
								QTYBU = GetDecimal(dt.Rows[i][11].ToString()),
								BU = GetString(dt.Rows[i][12].ToString()),
								AVE = GetDecimal(dt.Rows[i][13].ToString()),
								COS = GetDecimal(dt.Rows[i][14].ToString()),
								Note = GetString(dt.Rows[i][15].ToString()),
								Pos = GetDecimal(dt.Rows[i][16].ToString()),
								PlannedPrice = GetDecimal(dt.Rows[i][17].ToString()),
								SumPlannedPrice = GetDecimal(dt.Rows[i][18].ToString()),
								FuturePlannedPrice = GetDecimal(dt.Rows[i][19].ToString()),
								SumFuturePlannedPrice = GetDecimal(dt.Rows[i][20].ToString()),
							});
						}

						if (rows.Count > 0)
						{
							((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rows);
						}
					}
				}
			}

			return errorList;
		}
	}
}
