﻿namespace HotelAnalytics.Logic.Importer.Structures
{
	public class ErrorItem
	{
		public ErrorItem()
		{
		}

		public ErrorItem(string errorType, string errorMessage)
		{
			ErrorType = errorType;
			ErrorMessage = errorMessage;

		}

		public string ErrorType { get; set; }
		public string ErrorMessage { get; set; }
	}
}
