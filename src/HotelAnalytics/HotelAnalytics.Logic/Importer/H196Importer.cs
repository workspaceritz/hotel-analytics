﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class H196Importer : BaseImporterTextFileDelimited
	{
		public H196Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "RESORT\tTAX_INCLUSIVE_YN\tFREQUENT_FLYER_YN\tTRX_CODE\tTC_GROUP\tTC_SUBGROUP\tDESCRIPTION\tIS_MANUAL_POST_ALLOWED\tADJ_TRX_CODE\tIND_REVENUE_GP\tCURRENCY1\tIND_AR\tIND_BILLING\tIND_CASH\tIND_DEPOSIT_YN";
	
		protected int startline = 0;
		protected string stopconditionText = "LOGO";
		protected char split = '\t';


		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			if (errorList.Count > 0)
			{
				return errorList;
			}


			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);
			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}

				data.Rows.Add(record.Split(split));
			}


			//Start load staging
			var rowsH196 = new List<Staging_H196>();
			foreach (DataRow dr in data.Rows)
			{
				rowsH196.Add(new Staging_H196
				{
					BatchId = BatchId,
					RESORT = GetString(dr["RESORT"].ToString()),
					TAX_INCLUSIVE_YN = GetString(dr["TAX_INCLUSIVE_YN"].ToString()),
					FREQUENT_FLYER_YN = GetString(dr["FREQUENT_FLYER_YN"].ToString()),
					TRX_CODE = GetString(dr["TRX_CODE"].ToString()),
					TC_GROUP = GetString(dr["TC_GROUP"].ToString()),
					TC_SUBGROUP = GetString(dr["TC_SUBGROUP"].ToString()),
					DESCRIPTION = GetString(dr["DESCRIPTION"].ToString()),
					IS_MANUAL_POST_ALLOWED = GetString(dr["IS_MANUAL_POST_ALLOWED"].ToString()),
					ADJ_TRX_CODE = GetString(dr["ADJ_TRX_CODE"].ToString()),
					IND_REVENUE_GP = GetString(dr["IND_REVENUE_GP"].ToString()),
					CURRENCY1 = GetString(dr["CURRENCY1"].ToString()),
					IND_AR = GetString(dr["IND_AR"].ToString()),
					IND_BILLING = GetString(dr["IND_BILLING"].ToString()),
					IND_CASH = GetString(dr["IND_CASH"].ToString()),
					IND_DEPOSIT_YN = GetString(dr["IND_DEPOSIT_YN"].ToString())
				});
			}


			if (rowsH196.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsH196);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
