﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace HotelAnalytics.Logic.Importer
{
	public class ClubMembersImporter : BaseImporterExcelFile
	{
		public int Year { get; private set; }
		#region Constants

		// positioning
		private const int HeaderRow = 1;
		private const int FirstDataRow = 2;
		private const int FirstDataColumn = 1;

		// column indexes
		#endregion

		public ClubMembersImporter(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					// validate structure

					// find sheet by name
					var ws = ep.Workbook.Worksheets.FirstOrDefault(myWs => myWs.Name == "SUP - Club Member");

					if (ws == null)
					{
						errorList.Add(new ErrorItem("STRUCTURE", "Expecting 'SUP - Club Member' sheet."));
					}

					return errorList;
				}
			}
		}

		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = ValidateStructure();

			if (errorList.Count > 0)
			{
				return errorList;
			}

			using (var stream = new MemoryStream())
			{
				stream.Write(FileContent, 0, FileContent.Length);

				using (var ep = new ExcelPackage(stream))
				{
					var ws = ep.Workbook.Worksheets.First(myWs => myWs.Name == "SUP - Club Member");
					var rows = new List<Staging_ClubMember>();
					var LastRow = this.LastRow(ws);
					var LastCol = this.LastCol(ws);
					var range = ws.Cells[FirstDataRow, FirstDataColumn, LastRow, LastCol];

					

					for (var i = FirstDataRow; i <= (LastRow); i++)
					{
						rows.Add(new Staging_ClubMember
						{
							BatchId = BatchId,
							Number = GetString(ws.Cells[i, 1].Value?.ToString()),
							CardNr = GetString(ws.Cells[i, 2].Value?.ToString()),
							LastName = GetString(ws.Cells[i, 3].Value?.ToString()),
							FirstName = GetString(ws.Cells[i, 4].Value?.ToString()),
							Civility = GetString(ws.Cells[i, 5].Value?.ToString()),
							Gender = GetString(ws.Cells[i, 6].Value?.ToString()),
							Address = GetString(ws.Cells[i, 7].Value?.ToString()),
							City = GetString(ws.Cells[i, 8].Value?.ToString()),
							ZipCode = GetString(ws.Cells[i, 9].Value?.ToString()),
							Email = GetString(ws.Cells[i, 11].Value?.ToString()),
							TelephoneClean = GetString(ws.Cells[i, 12].Value?.ToString()),
							DataNascimento = GetString(ws.Cells[i, 13].Value?.ToString()),
							Source = GetString(ws.Cells[i, 14].Value?.ToString()),
							Aceita = GetString(ws.Cells[i, 15].Value?.ToString()),
							Telephone = GetString(ws.Cells[i, 17].Value?.ToString()),
							Country = GetString(ws.Cells[i, 18].Value?.ToString()),

						}); 
					}

					if (rows.Count > 0)
					{
						((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rows);
					}
				}
			}
			return errorList;
		}
	}
}
