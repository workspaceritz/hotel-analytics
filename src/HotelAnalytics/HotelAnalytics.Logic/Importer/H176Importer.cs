﻿
using EntityFramework.BulkInsert.Extensions;
using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public class H176Importer : BaseImporterTextFileDelimited
	{
		public H176Importer(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(filePath, dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
		}

		protected bool hasHeader = true;
		protected string headerText = "FLOW_THROUGH	LABEL\tORDER_BY\tCOMPONENTS\tFEATURE_LIST\tGET_ROOM_LIST_ROOM_CATEGORY\tACTIVE_DATE\tDEF_OCCUPANCY\tMEETINGROOM_YN\tHOUSEKEEPING\tSEND_TO_INTERFACE_YN\tRATE_AMOUNT\tGENERIC_FLAG\tSHORT_DESCRIPTION\tROOM_CLASS\tNUMBER_ROOMS\tMAX_OCCUPANCY\tMAX_ROLLAWAYS\tRATE_CODE\tPSUEDO_ROOM_TYPE\tSUITE\tYIELDABLE_YN\tYIELD_CATEGORY\tROOM_CATEGORY";

		protected int startline = 0;
		protected string stopconditionText = "LOGO	CF_YIELD_CAT_DESC";
		protected char split = '\t';


		public override List<ErrorItem> ImportToStaging()
		{
			var errorList = new List<ErrorItem>();

			if (errorList.Count > 0)
			{
				return errorList;
			}


			var reader = ReadAsLines(FilePath);

			var data = new DataTable();

			var readerStart = reader.Skip(startline);

			var headerLineColumns = readerStart.First().Split(split);
			foreach (var header in headerLineColumns)
			{
				data.Columns.Add(header);
			}

			var records = readerStart.Skip(1);
			foreach (var record in records)
			{
				if (record.StartsWith(stopconditionText))
				{
					break;
				}

				data.Rows.Add(record.Split(split));
			}


			//Start load staging
			var rowsH136 = new List<Staging_H176>();
			foreach (DataRow dr in data.Rows)
			{
				rowsH136.Add(new Staging_H176
				{
					BatchId = BatchId,



					FLOW_THROUGH = GetString(dr["FLOW_THROUGH"].ToString()),
					LABEL = GetString(dr["LABEL"].ToString()),
					ORDER_BY = GetString(dr["ORDER_BY"].ToString()),
					COMPONENTS = GetString(dr["COMPONENTS"].ToString()),
					FEATURE_LIST = GetString(dr["FEATURE_LIST"].ToString()),
					GET_ROOM_LIST_ROOM_CATEGORY = GetString(dr["GET_ROOM_LIST_ROOM_CATEGORY"].ToString()),
					ACTIVE_DATE = GetString(dr["ACTIVE_DATE"].ToString()),
					DEF_OCCUPANCY = GetString(dr["DEF_OCCUPANCY"].ToString()),
					MEETINGROOM_YN = GetString(dr["MEETINGROOM_YN"].ToString()),
					HOUSEKEEPING = GetString(dr["HOUSEKEEPING"].ToString()),
					SEND_TO_INTERFACE_YN = GetString(dr["SEND_TO_INTERFACE_YN"].ToString()),
					RATE_AMOUNT = GetString(dr["RATE_AMOUNT"].ToString()),
					GENERIC_FLAG = GetString(dr["GENERIC_FLAG"].ToString()),
					SHORT_DESCRIPTION = GetString(dr["SHORT_DESCRIPTION"].ToString()),
					ROOM_CLASS = GetString(dr["ROOM_CLASS"].ToString()),
					NUMBER_ROOMS = GetString(dr["NUMBER_ROOMS"].ToString()),
					MAX_OCCUPANCY = GetString(dr["MAX_OCCUPANCY"].ToString()),
					MAX_ROLLAWAYS = GetString(dr["MAX_ROLLAWAYS"].ToString()),
					RATE_CODE = GetString(dr["RATE_CODE"].ToString()),
					PSUEDO_ROOM_TYPE = GetString(dr["PSUEDO_ROOM_TYPE"].ToString()),
					SUITE = GetString(dr["SUITE"].ToString()),
					YIELDABLE_YN = GetString(dr["YIELDABLE_YN"].ToString()),
					YIELD_CATEGORY = GetString(dr["YIELD_CATEGORY"].ToString()),
					ROOM_CATEGORY = GetString(dr["ROOM_CATEGORY"].ToString()),

				});
			}


			if (rowsH136.Count > 0)
			{
				((Model.HotelAnalytics)HotelAnalyticsContext).BulkInsert(rowsH136);
			}

			return errorList;
		}

		public override List<ErrorItem> ValidateStructure()
		{
			var errorList = new List<ErrorItem>();

			var reader = ReadAsLines(FilePath);

			var readerStart = reader.Skip(startline);

			if (hasHeader)
			{
				if (headerText != readerStart.First())
				{
					errorList.Add(new ErrorItem("STRUCTURE", "Invalid Header"));
				}
			}

			return errorList;
		}
	}
}
