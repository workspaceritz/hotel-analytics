﻿using HotelAnalytics.Model;
using OfficeOpenXml;
using System;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public abstract class BaseImporterTextFileDelimited : BaseImporter
	{
		protected string FilePath { get; }

		protected BaseImporterTextFileDelimited(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
			FilePath = filePath;
		}

		public int LastRow(ExcelWorksheet sheet)
		{
			var row = sheet.Dimension.End.Row;
			while (row >= 1)
			{
				var range = sheet.Cells[row, 1, row, sheet.Dimension.End.Column];
				if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
				{
					break;
				}
				row--;
			}
			return row;
		}

		public int LastCol(ExcelWorksheet sheet)
		{
			var col = sheet.Dimension.End.Column;
			while (col >= 1)
			{
				var range = sheet.Cells[1, col, sheet.Dimension.End.Row, col];
				if (range.Any(c => !string.IsNullOrEmpty(c.Text)))
				{
					break;
				}
				col--;
			}
			return col;
		}
	}
}
