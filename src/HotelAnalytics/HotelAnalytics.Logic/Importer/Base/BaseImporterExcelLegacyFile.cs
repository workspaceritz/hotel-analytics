﻿using HotelAnalytics.Model;
using System;

namespace HotelAnalytics.Logic.Importer
{
	public abstract class BaseImporterExcelLegacyFile : BaseImporter
	{
		protected string ConnectionString { get; }

		protected BaseImporterExcelLegacyFile(string filePath, string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
			: base(dataSourceCode, hotelCode, fileName, dataPeriod, username, hotelAnalyticsContext)
		{
			ConnectionString = $@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={filePath}; Jet OLEDB:Engine Type = 5; Extended Properties =""Excel 8.0;IMEX=1;HDR=YES;""";
		}
	}
}
