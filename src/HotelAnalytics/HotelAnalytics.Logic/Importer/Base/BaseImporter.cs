﻿using HotelAnalytics.Logic.Importer.Structures;
using HotelAnalytics.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;

namespace HotelAnalytics.Logic.Importer
{
	public abstract class BaseImporter
	{
		protected IHotelAnalytics HotelAnalyticsContext { get; private set; }


		protected int BatchId { get; private set; }

		protected string DataSourceCode { get; private set; }

		protected string HotelCode { get; private set; }

		protected string Username { get; private set; }

		protected string FileName { get; private set; }

		protected DateTime DataPeriod { get; private set; }


		protected BaseImporter(string dataSourceCode, string hotelCode, string fileName, DateTime dataPeriod, string username, IHotelAnalytics hotelAnalyticsContext)
		{

			DataSourceCode = dataSourceCode;
			HotelCode = hotelCode;
			FileName = fileName;
			Username = username;
			HotelAnalyticsContext = hotelAnalyticsContext;
			DataPeriod = dataPeriod;

			var retValue = HotelAnalyticsContext.spGetBatch(DataSourceCode, HotelCode, fileName, Username)[0].idBatch;
			if (retValue != null)
			{
				BatchId = retValue.Value;
			}
		}

		public int GetBatchId()
		{
			return BatchId;
		}

		public abstract List<ErrorItem> ImportToStaging();

		public abstract List<ErrorItem> ValidateStructure();

		public List<ErrorItem> ValidateStaging()
		{
			var cmd = HotelAnalyticsContext.Database.Connection.CreateCommand();
			cmd.CommandText = "spValidateStaging @idBatch;";
			cmd.Parameters.Add(new SqlParameter("@idBatch", SqlDbType.Int) { Value = BatchId });
			cmd.CommandTimeout = HotelAnalyticsContext.CommandTimeout;

			if (HotelAnalyticsContext.Database.Connection.State != ConnectionState.Open)
			{
				HotelAnalyticsContext.Database.Connection.Open();
			}

			var reader = cmd.ExecuteReader();
			var errorList = new List<ErrorItem>();

			while (reader.Read())
			{
				var errorItem = new ErrorItem
				{
					ErrorType = reader["ErrorType"].ToString(),
					ErrorMessage = reader["ErrorMessage"].ToString()
				};
				errorList.Add(errorItem);
			}

			if (HotelAnalyticsContext.Database.Connection.State == ConnectionState.Open)
			{
				HotelAnalyticsContext.Database.Connection.Close();
			}

			return errorList;

		}

		public void ImportToDW()
		{
			HotelAnalyticsContext.Database.ExecuteSqlCommand("exec spImportToDW @idBatch, @dataPeriod, @username;",
				new SqlParameter("@idBatch", SqlDbType.Int) { Value = BatchId },
				new SqlParameter("@dataPeriod", SqlDbType.Date) { Value = DataPeriod },
				new SqlParameter("@username", SqlDbType.NVarChar) { Value = Username }
				);
		}

		public void WriteBatchMessages(List<ErrorItem> errors)
		{
			var stg = errors.Select(e => new BatchMessage() { BatchId = BatchId, ErrorType = e.ErrorType, ErrorMessage = e.ErrorMessage, CreatedBy = Username });
			HotelAnalyticsContext.BatchMessages.AddRange(stg);
			HotelAnalyticsContext.SaveChanges();
		}

		public DateTime GetFromAODate(string strDate)
		{
			var dtDate = DateTime.MinValue;
			if (Double.TryParse(strDate, out var d_date))
			{
				dtDate = DateTime.FromOADate(d_date);
			}

			return dtDate;
		}

		public DateTime GetDate(string strDate)
		{

			DateTime.TryParse(strDate, out var dtDate);
			return dtDate;
		}

		public decimal? GetDecimal(string strValue)
		{
			var temp = strValue ?? "";
			var tempText = temp.Replace('.', Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
		   .Replace(',', Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));

			if (decimal.TryParse(tempText, out var decimalValue))
			{
				return decimalValue;
			}

			return null;

		}

		public int? GetInt(string strValue)
		{
			if (int.TryParse(strValue, out var intValue))
			{
				return intValue;
			}

			return null;

		}

		public string GetString(string strValue)
		{
			return strValue;
		}

		public decimal GetDecimalNotNull(string strValue)
		{
			var temp = strValue.ToString();
			var tempText = temp.Replace('.', Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator))
		   .Replace(',', Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));

			if (decimal.TryParse(tempText, out var decimalValue))
			{
				return decimalValue;
			}

			return 0;

		}

		public IEnumerable<string> ReadAsLines(string filePath)
		{
			using (var reader = new StreamReader(filePath))
			{
				while (!reader.EndOfStream)
				{
					yield return reader.ReadLine();
				}
			}
		}

		#region EPPlus utils

		protected static string GetColumnName(int columnNumber)
		{
			var dividend = columnNumber;
			var columnName = String.Empty;

			const int alphabetLength = 'Z' - 'A' + 1;
			while (dividend > 0)
			{
				var modulo = (dividend - 1) % alphabetLength;
				columnName = Convert.ToChar('A' + modulo) + columnName;
				dividend = (dividend - modulo) / alphabetLength;
			}

			return columnName;
		}

		protected static int GetColumnNumber(string columnName)
		{
			var number = 0;
			var pow = 1;
			for (var i = columnName.Length - 1; i >= 0; i--)
			{
				number += (columnName[i] - 'A' + 1) * pow;
				pow *= 26;
			}

			return number;
		}

		protected static string IncrementColumn(string columnName, int incrementBy = 1)
		{
			return GetColumnName(GetColumnNumber(columnName) + incrementBy);
		}

		protected static string DecrementColumn(string columnName, int incrementBy = 1)
		{
			return GetColumnName(GetColumnNumber(columnName) - incrementBy);
		}

		protected static string RangeAddr(string begin, string end)
		{
			return $"{begin}:{end}";
		}

		protected static string RangeAddr(string colBegin, int rowBegin, string colEnd, int rowEnd)
		{
			return RangeAddr($"{colBegin}{rowBegin}", $"{colEnd}{rowEnd}");
		}

		protected static string RowAddr(int index)
		{
			return $"{index}:{index}";
		}

		protected static string ColAddr(string col)
		{
			return $"{col}:{col}";
		}

		protected static string CellAddr(string col, int row)
		{
			return $"{col}{row}";
		}

		#endregion
	}
}
