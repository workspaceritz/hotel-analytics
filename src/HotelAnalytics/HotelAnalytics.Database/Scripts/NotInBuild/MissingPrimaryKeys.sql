﻿SELECT c.name TABLE_SCHEMA, b.name TABLE_NAME
FROM sys.tables b 
INNER JOIN sys.schemas c ON b.schema_id = c.schema_id 
WHERE b.type = 'U' 
AND NOT EXISTS
	(SELECT a.name 
	FROM sys.key_constraints a 
	WHERE a.parent_object_id = b.OBJECT_ID 
	AND a.schema_id = c.schema_id 
	AND a.type = 'PK' )


SELECT SCHEMA_NAME(schema_id) AS SchemaName,name AS TableName
FROM sys.tables
WHERE OBJECTPROPERTY(OBJECT_ID,'TableHasPrimaryKey') = 0
ORDER BY SchemaName, TableName;
GO