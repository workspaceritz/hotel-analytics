﻿/*-----------------------GENERAL--------------------------*/

SET IDENTITY_INSERT [dbo].[LookupSet] ON 

GO
INSERT [dbo].[LookupSet] ([LookupSetId], [LookupSet], [Created], [CreatedBy], [Modified], [ModifiedBy]) VALUES (1, N'Configurations', CAST(N'2017-05-22T15:51:48.6157044' AS DateTime2), N'UB-ACUNHA\ACunha', CAST(N'2017-05-22T15:51:48.6157044' AS DateTime2), N'UB-ACUNHA\ACunha')
GO
SET IDENTITY_INSERT [dbo].[LookupSet] OFF
GO
SET IDENTITY_INSERT [dbo].[LookupValue] ON 

GO
INSERT [dbo].[LookupValue] ([LookupValueId], [LookupSetId], [LookupValue], [LookupMappedValue], [Created], [CreatedBy], [Modified], [ModifiedBy]) VALUES (1, 1, N'LogLevel', N'3', CAST(N'2017-05-22T15:51:50.2982085' AS DateTime2), N'UB-ACUNHA\ACunha', CAST(N'2017-05-22T15:51:50.2982085' AS DateTime2), N'UB-ACUNHA\ACunha')
GO
SET IDENTITY_INSERT [dbo].[LookupValue] OFF
GO


/*---------------------------DATA SOURCES------------------------------------*/

INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('F100','F100','F100')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('H136','H136','H136')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('H222','H222','H222')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('R122','R122','R122')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('D140','D140','D140')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('AD180','AD180','AD180')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('OperaPlanningBudget','OperaPlanningBudget','OperaPlanningBudget')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('OperaPlanningForecast','OperaPlanningForecast','OperaPlanningForecast')

INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('H176','H176','H176')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('H196','H196','H196')

INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('MicrosMenuItems','MicrosMenuItems','MicrosMenuItems')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('MicrosTenderMedia','MicrosTenderMedia','MicrosTenderMedia')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('MicrosSummaryTransactions','MicrosSummaryTransactions','MicrosSummaryTransactions')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('MicrosMenuItemFamilies','MicrosMenuItemFamilies','MicrosMenuItemFamilies')

INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('Recipe','Recipe','Recipe')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('ClubMembers','ClubMembers','ClubMembers')

INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('MarketCodeDefinitions','MarketCodeDefinitions','MarketCodeDefinitions')
INSERT INTO DataSource(DataSourceCode, DataSourceName, DataSourceDescription) VALUES('H226','H226','H226')

/*****************************SCENARIO SETUP*****************************/
declare @Scenario_Actual nvarchar(255) = 'Actual';
declare @Scenario_Budget nvarchar(255) = 'Budget';
declare @Scenario_Forecast nvarchar(255) = 'Forecast';

INSERT INTO DW.DimScenario(Scenario) VALUES(@Scenario_Actual)
INSERT INTO DW.DimScenario(Scenario) VALUES(@Scenario_Budget)
INSERT INTO DW.DimScenario(Scenario) VALUES(@Scenario_Forecast)

/*****************************HOTEL SETUP*****************************/

INSERT INTO Hotel(HotelCode, HotelDescription) VALUES('MARRIOTT','Hotel Marriot 1')


/*****************************DIM's NULLs*****************************/
 
SET IDENTITY_INSERT DW.DimRevenueCenter ON
INSERT INTO DW.DimRevenueCenter(DimRevenueCenterId, RevenueCenter) VALUES(0,'N/A')
SET IDENTITY_INSERT DW.DimRevenueCenter OFF

SET IDENTITY_INSERT DW.DimOrderType ON
INSERT INTO DW.DimOrderType(DimOrderTypeId, OrderType) VALUES(0,'N/A')
SET IDENTITY_INSERT DW.DimOrderType OFF

SET IDENTITY_INSERT DW.DimEmployee ON
INSERT INTO DW.DimEmployee(DimEmployeeId, Employee) VALUES(0,'N/A')
SET IDENTITY_INSERT DW.DimEmployee OFF

SET IDENTITY_INSERT DW.DimMenuItemFamily ON
INSERT INTO DW.DimMenuItemFamily (DimMenuItemFamilyId,ItemName,ItemNumber,Family, FamilyName,VAT,[Group],FamilyGroup) VALUES(0,'N/A','0','0','N/A','0','0','N/A')
SET IDENTITY_INSERT DW.DimMenuItemFamily OFF

SET IDENTITY_INSERT DW.DimClubMember ON
INSERT INTO DW.DimClubMember (DimClubMemberId,TelephoneClean,Number) VALUES(0,'NoClubMember','N/A')
SET IDENTITY_INSERT DW.DimClubMember OFF

SET IDENTITY_INSERT DW.DimRoomType ON
INSERT INTO DW.DimRoomType(DimRoomTypeId , Label, Class, Description, Suite, IsRoom) VALUES(0,'N/A','N/A','N/A',0,0)
SET IDENTITY_INSERT DW.DimRoomType OFF

SET IDENTITY_INSERT DW.DimRecipe ON
INSERT INTO DW.DimRecipe (DimRecipeId,RecipeNo,Recipe) VALUES(0,'0','N/A')
SET IDENTITY_INSERT DW.DimRecipe OFF

SET IDENTITY_INSERT DW.DimRecipeComponent ON
INSERT INTO DW.DimRecipeComponent (DimRecipeComponentId,ArticleNo,Component,Unit, BU) VALUES(0,'0','N/A','N/A','N/A')
SET IDENTITY_INSERT DW.DimRecipeComponent OFF

SET IDENTITY_INSERT DW.DimRoomNumber ON
INSERT INTO DW.DimRoomNumber (DimRoomNumberId,DimRoomTypeId,Label,Number) VALUES(0,0,'N/A','N/A')
SET IDENTITY_INSERT DW.DimRoomNumber OFF

/*****************************LOOKUP SETS*****************************/
insert into LookupSet (LookupSet) values ('ReservationStatus_Mapping');
declare @LookupSetId_ReservationStatus_Mapping int;
select @LookupSetId_ReservationStatus_Mapping = LookupSetId from LookupSet where LookupSet = 'ReservationStatus_Mapping';
insert into LookupValue (LookupSetId, LookupValue, LookupMappedValue) values
	(@LookupSetId_ReservationStatus_Mapping, 'act', 'ACT'),
	(@LookupSetId_ReservationStatus_Mapping, 'def', 'DEF'),
	(@LookupSetId_ReservationStatus_Mapping, 'ten1', 'TE1'),
	(@LookupSetId_ReservationStatus_Mapping, 'te1', 'TE1'),
	(@LookupSetId_ReservationStatus_Mapping, 'ten2', 'TE2'),
	(@LookupSetId_ReservationStatus_Mapping, 'te2', 'TE2'),
	(@LookupSetId_ReservationStatus_Mapping, 'pro', 'PRO'),
	(@LookupSetId_ReservationStatus_Mapping, 'los', 'LOS'),
	(@LookupSetId_ReservationStatus_Mapping, 'tra', 'TRA'),
	(@LookupSetId_ReservationStatus_Mapping, 'who', 'TRA')
;

insert into LookupSet (LookupSet) values ('Scenario_OperaPlanning_Mapping');
declare @Scenario_OperaPlanning_Mapping int;
select @Scenario_OperaPlanning_Mapping = LookupSetId from LookupSet where LookupSet = 'Scenario_OperaPlanning_Mapping';
insert into LookupValue (LookupSetId, LookupValue, LookupMappedValue) values
	(@Scenario_OperaPlanning_Mapping, 'RITZI_FCST', @Scenario_Forecast),
	(@Scenario_OperaPlanning_Mapping, 'RITZI_BUD', @Scenario_Budget)
;

/*****************************DIMS*****************************/
insert into DW.DimReservationStatus(ReservationStatusCode) values 
	('ACT'),
	('DEF'),
	('TE1'),
	('TE2'),
	('PRO'),
	('LOS'),
	('TRA')
;