﻿CREATE TABLE [Staging].[H226] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]        BIGINT         NOT NULL,
    [Created]        DATETIME2 (7)  CONSTRAINT [DF_H226_Created] DEFAULT (getdate()) NOT NULL,
    [TRX_CODE]       NVARCHAR (255) NULL,
    [DESCRIPTION]    NVARCHAR (255) NULL,
    [BOF_TRANS_CODE] NVARCHAR (255) NULL,
    [BOF_TRANS_TEXT] NVARCHAR (255) NULL,
    [BOF_VALUE]      NVARCHAR (255) NULL,
    CONSTRAINT [PK_H226] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_H226_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);

