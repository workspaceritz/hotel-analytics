﻿CREATE TABLE [Staging].[H222] (
    [Id]                    BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]               BIGINT         NOT NULL,
    [Created]               DATETIME2 (7)  CONSTRAINT [DF_H222_Created] DEFAULT (getdate()) NOT NULL,
    [MARKET_CODE] 	        NVARCHAR (255) NULL,
    [DESCRIPTION] 	        NVARCHAR (255) NULL,
    [BOF_TRANS_CODE]        NVARCHAR (255) NULL,	
    [BOF_TRANS_TEXT]        NVARCHAR (255) NULL,	
    [BOF_VALUE] 	        INT NULL,
    CONSTRAINT [PK_H222] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_H222_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);
GO
CREATE NONCLUSTERED INDEX [FK_H222_Batch]
    ON [Staging].[H222]([BatchId] ASC);
GO