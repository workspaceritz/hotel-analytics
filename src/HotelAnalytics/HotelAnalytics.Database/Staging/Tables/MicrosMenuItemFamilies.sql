﻿CREATE TABLE [Staging].[MicrosMenuItemFamilies] (
    [Id]          BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]     BIGINT         NOT NULL,
    [Created]     DATETIME2 (7)  CONSTRAINT [DF_MicrosMenuItemFamilies_Created] DEFAULT (getdate()) NOT NULL,
    [ItemName]    NVARCHAR (255) NULL,
    [ItemNumber]  NVARCHAR (255) NULL,
    [Family]      NVARCHAR (255) NULL,
    [FamilyName]  NVARCHAR (255) NULL,
    [VAT]         NVARCHAR (255) NULL,
    [GroupCode]   NVARCHAR (255) NULL,
    [FamilyGroup] NVARCHAR (255) NULL,
    CONSTRAINT [PK_MicrosMenuItemFamilies] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MicrosMenuItemFamilies_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);



