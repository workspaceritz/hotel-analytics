﻿CREATE TABLE [Staging].[ClubMembers] (
    [Id]             BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]        BIGINT         NOT NULL,
    [Created]        DATETIME2 (7)  CONSTRAINT [DF_ClubMembers_Created] DEFAULT (getdate()) NOT NULL,
    [Number]         NVARCHAR (255) NULL,
    [CardNr]         NVARCHAR (255) NULL,
    [LastName]       NVARCHAR (255) NULL,
    [FirstName]      NVARCHAR (255) NULL,
    [Civility]       NVARCHAR (255) NULL,
    [Gender]         NVARCHAR (255) NULL,
    [Address]        NVARCHAR (255) NULL,
    [City]           NVARCHAR (255) NULL,
    [ZipCode]        NVARCHAR (255) NULL,
    [Email]          NVARCHAR (255) NULL,
    [TelephoneClean] NVARCHAR (255) NULL,
    [DataNascimento] NVARCHAR (255) NULL,
    [Source]         NVARCHAR (255) NULL,
    [Aceita]         NVARCHAR (255) NULL,
    [Telephone]      NVARCHAR (255) NULL,
    [Country]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_ClubMembers] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClubMembers_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);

