﻿CREATE TABLE [Staging].[MarketCodeDefinitions] (
    [Id]              BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]         BIGINT         NOT NULL,
    [Created]         DATETIME2 (7)  CONSTRAINT [DF_MarketCodeDefinitions_Created] DEFAULT (getdate()) NOT NULL,
    [MarketGroup]     NVARCHAR (255) NULL,
    [MarketSegment]   NVARCHAR (255) NULL,
    [Account]         NVARCHAR (255) NULL,
    [AccountComments] NVARCHAR (255) NULL,
    [Type]            NVARCHAR (255) NULL,
    [WDWE]            NVARCHAR (255) NULL,
    [DptFrom]         NVARCHAR (50)  NULL,
    [DptTo]           NVARCHAR (50)  NULL,
    CONSTRAINT [PK_MarketCodeDefinitions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_MarketCodeDefinitions_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);



