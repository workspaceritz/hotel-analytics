﻿CREATE TABLE [Staging].[H196] (
    [Id]                     BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]                BIGINT         NOT NULL,
    [Created]                DATETIME2 (7)  CONSTRAINT [DF_H196_Created] DEFAULT (getdate()) NOT NULL,
    [RESORT]                 NVARCHAR (255) NULL,
    [TAX_INCLUSIVE_YN]       NVARCHAR (255) NULL,
    [FREQUENT_FLYER_YN]      NVARCHAR (255) NULL,
    [TRX_CODE]               NVARCHAR (255) NULL,
    [TC_GROUP]               NVARCHAR (255) NULL,
    [TC_SUBGROUP]            NVARCHAR (255) NULL,
    [DESCRIPTION]            NVARCHAR (255) NULL,
    [IS_MANUAL_POST_ALLOWED] NVARCHAR (255) NULL,
    [ADJ_TRX_CODE]           NVARCHAR (255) NULL,
    [IND_REVENUE_GP]         NVARCHAR (255) NULL,
    [CURRENCY1]              NVARCHAR (255) NULL,
    [IND_AR]                 NVARCHAR (255) NULL,
    [IND_BILLING]            NVARCHAR (255) NULL,
    [IND_CASH]               NVARCHAR (MAX) NULL,
    [IND_DEPOSIT_YN]         NVARCHAR (255) NULL,
    CONSTRAINT [PK_H196] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_H196_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);

