﻿CREATE TABLE [Staging].[H176] (
    [Id]                          BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]                     BIGINT         NOT NULL,
    [Created]                     DATETIME2 (7)  CONSTRAINT [DF_H176_Created] DEFAULT (getdate()) NOT NULL,
    [FLOW_THROUGH]                NVARCHAR (255) NULL,
    [LABEL]                       NVARCHAR (255) NULL,
    [ORDER_BY]                    NVARCHAR (255) NULL,
    [COMPONENTS]                  NVARCHAR (255) NULL,
    [FEATURE_LIST]                NVARCHAR (255) NULL,
    [GET_ROOM_LIST_ROOM_CATEGORY] NVARCHAR (MAX) NULL,
    [ACTIVE_DATE]                 NVARCHAR (255) NULL,
    [DEF_OCCUPANCY]               NVARCHAR (255) NULL,
    [MEETINGROOM_YN]              NVARCHAR (255) NULL,
    [HOUSEKEEPING]                NVARCHAR (255) NULL,
    [SEND_TO_INTERFACE_YN]        NVARCHAR (255) NULL,
    [RATE_AMOUNT]                 NVARCHAR (255) NULL,
    [GENERIC_FLAG]                NVARCHAR (255) NULL,
    [SHORT_DESCRIPTION]           NVARCHAR (MAX) NULL,
    [ROOM_CLASS]                  NVARCHAR (255) NULL,
    [NUMBER_ROOMS]                NVARCHAR (255) NULL,
    [MAX_OCCUPANCY]               NVARCHAR (255) NULL,
    [MAX_ROLLAWAYS]               NVARCHAR (255) NULL,
    [RATE_CODE]                   NVARCHAR (255) NULL,
    [PSUEDO_ROOM_TYPE]            NVARCHAR (255) NULL,
    [SUITE]                       NVARCHAR (255) NULL,
    [YIELDABLE_YN]                NVARCHAR (255) NULL,
    [YIELD_CATEGORY]              NVARCHAR (255) NULL,
    [ROOM_CATEGORY]               NVARCHAR (255) NULL,
    CONSTRAINT [PK_H176] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_H176_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);
GO
CREATE NONCLUSTERED INDEX [FK_H176_Batch]
    ON [Staging].[H176]([BatchId] ASC);
GO