﻿CREATE TABLE [Staging].[R122] (
    [Id]                    BIGINT          IDENTITY (1, 1) NOT NULL,
    [BatchId]               BIGINT          NOT NULL,
    [Created]               DATETIME2 (7)   CONSTRAINT [DF_R122_Created] DEFAULT (getdate()) NOT NULL,
    [RESERVATION_STATUS]    NVARCHAR (255)  NULL,
    [REPORT_ID]             NVARCHAR (255)  NULL,
    [RESERVATION_DATE]      DATE            NULL,
    [CHAR_RESERVATION_DATE] NVARCHAR (255)  NULL,
    [MARKET_CODE]           NVARCHAR (255)  NULL,
    [CF_MARKET_CODE_SEQ]    NVARCHAR (255)  NULL,
    [MARKET_CODE_DESC]      NVARCHAR (255)  NULL,
    [S_DEF_ROOMS_DAY]       INT             NULL,
    [S_DEF_ROOMS_MKT]       INT             NULL,
    [S_REV_DAY]             DECIMAL (18, 6) NULL,
    [S_REV_MKT]             DECIMAL (18, 6) NULL,
    [DAY_ROOMS]             INT             NULL,
    [TOT_ARR_DAY]           DECIMAL (18, 6) NULL,
    [TOT_ARR_MKT]           DECIMAL (18, 6) NULL,
    [ARRIVAL_RMS_DAY]       INT             NULL,
    [ARR_RMS_MKT]           INT             NULL,
    [GUEST_DAY]             INT             NULL,
    [GUEST_MKT]             INT             NULL,
    [DOUBLE_OCC_DAY]        INT             NULL,
    [DOUBLE_OCC_MKT]        INT             NULL,
    [SINGLE_OCC_DAY]        INT             NULL,
    [SINGLE_OCC_MKT]        INT             NULL,
    [PER_DOUBLE_MKT]        DECIMAL (18, 6) NULL,
    [PER_DOUBLE_OCC_DAY]    DECIMAL (18, 6) NULL,
    [PER_OCC_DAY]           DECIMAL (18, 6) NULL,
    [PER_OCC_MKT]           DECIMAL (18, 6) NULL,
    [CS_SUM_OO_MKT]         DECIMAL (18, 6) NULL,
    [CS_AVG_OO_DAY]         DECIMAL (18, 6) NULL,
    [NO_DEFINITE_ROOMS]     INT             NULL,
    [ARRIVAL_RMS]           INT             NULL,
    [NO_OF_GUESTS]          INT             NULL,
    [SINGLE_OCCUPANCY]      INT             NULL,
    [MULTI_OCCUPANCY]       INT             NULL,
    [TOTAL_REVENUE]         DECIMAL (18, 6) NULL,
    [OO_ROOMS]              INT             NULL,
    [PRINT_NO_OF_ROOMS]     INT             NULL,
    [GET_ARR]               INT             NULL,
    [PER_OCC]               INT             NULL,
    [MULTI_OCC_PER]         INT             NULL,
    CONSTRAINT [PK_R122] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_R122_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);



