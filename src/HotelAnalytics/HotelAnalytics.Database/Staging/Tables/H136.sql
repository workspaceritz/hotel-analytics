﻿CREATE TABLE [Staging].[H136] (
    [Id]                 BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]            BIGINT         NOT NULL,
    [Created]            DATETIME2 (7)  CONSTRAINT [DF_H136_Created] DEFAULT (getdate()) NOT NULL,
    [FLOW_THROUGH]       NVARCHAR (255) NULL,
    [PARENT_MARKET_CODE] NVARCHAR (255) NULL,
    [SELL_SEQUENCE]      INT            NULL,
    [MARKET_CODE]        NVARCHAR (255) NULL,
    [DESCRIPTION]        NVARCHAR (255) NULL,
    [DISPLAY_COLOR]      NVARCHAR (255) NULL,
    [INACTIVE_YN]        NVARCHAR (255) NULL,
    CONSTRAINT [PK_H136] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_H136_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);
GO
CREATE NONCLUSTERED INDEX [FK_H136_Batch]
    ON [Staging].[H136]([BatchId] ASC);
GO