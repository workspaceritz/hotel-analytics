﻿CREATE TABLE [Staging].[AD180] (
    [Id]                 BIGINT          IDENTITY (1, 1) NOT NULL,
    [BatchId]            BIGINT          NOT NULL,
    [Created]            DATETIME2 (7)   CONSTRAINT [DF_AD180_Created] DEFAULT (getdate()) NOT NULL,
    [PERIOD_START]       NVARCHAR (255)  NULL,
    [PERIOD_END]         NVARCHAR (255)  NULL,
    [PERIOD_DESCRIPTION] NVARCHAR (255)  NULL,
    [VIRTUAL]            NVARCHAR (255)  NULL,
    [MASTER_ORDER]       NVARCHAR (255)  NULL,
    [ORDER_BY]           NVARCHAR (255)  NULL,
    [STATUS]             NVARCHAR (255)  NULL,
    [TOTAL_AVGRATE]      DECIMAL (18, 6) NULL,
    [TOTAL_ROOMS]        DECIMAL (18, 6) NULL,
    [TOTAL_REVENUE]      DECIMAL (18, 6) NULL,
    [CUR_DATE]           DATETIME2 (7)   NULL,
    [EVENT_CODE]         NVARCHAR (255)  NULL,
    [NC_CODE]            NVARCHAR (255)  NULL,
    [RP_NC_CODE]         NVARCHAR (255)  NULL,
    [DAY_WORD]           NVARCHAR (255)  NULL,
    [DAY_NUMBER]         NVARCHAR (255)  NULL,
    [ROOMS]              NVARCHAR (255)  NULL,
    CONSTRAINT [PK_AD180] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_AD180_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);
GO
CREATE NONCLUSTERED INDEX [FK_AD180_Batch]
    ON [Staging].[AD180]([BatchId] ASC);
GO