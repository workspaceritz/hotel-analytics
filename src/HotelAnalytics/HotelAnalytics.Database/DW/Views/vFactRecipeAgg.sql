﻿
CREATE view [DW].[vFactRecipeAgg] as
select 
r.DimRecipeId,
r.RecipeNo,
SUM(f.COS) as TotalCOS
from Dw.FactRecipe f
inner join Dw.DimRecipe r on r.DimRecipeId = f.DimRecipeId 
group by r.DimRecipeId,r.RecipeNo