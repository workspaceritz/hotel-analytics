﻿create view DW.vDSVDimValueType as
select 0 as DimValueTypeId, 'N/A' as ValueType
union all
select 1 as DimValueTypeId, 'RN' as ValueType
union all
select 2 as DimValueTypeId, 'REV' as ValueType