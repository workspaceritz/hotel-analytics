﻿  create view DW.vDSVDimOperaTransaction as
  select 
[DimOperaTransactionId]
      ,[TransactionCode]
      ,[Description]
      ,[Account]
      ,[Dept]
      ,[Exclude]
      ,[InputVATFlag]
      ,[OutputVATFlag]
      ,[VATApplicableCode]
      ,[VATTaxPercentage]
      ,[OffsetAccount]
      ,[OffsetDept]
  from DW.DimOperaTransaction