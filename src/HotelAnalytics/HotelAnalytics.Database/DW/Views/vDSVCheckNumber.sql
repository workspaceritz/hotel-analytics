﻿CREATE view [DW].[vDSVCheckNumber] as
select distinct CheckNumber from DW.FactTender
UNION 
select distinct CheckNumber from DW.FactSummaryTransactions
UNION 
select distinct CheckNumber from DW.vDSVFactTransaction