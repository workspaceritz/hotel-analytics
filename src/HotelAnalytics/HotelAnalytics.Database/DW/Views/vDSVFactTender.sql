﻿

CREATE view [DW].[vDSVFactTender] as
with 
cteItemNumberNameBase as 
(
select
DimHotelId,
DimRevenueCenterId,
DimOrderTypeId,
TransactionDateTime,
LineTotal,
CheckNumber,
ItemName,
ItemNumber,
ReferenceInfo,
case when ItemName = 'Menu' then 1
else 0
end as IsMenu,
COUNT(1) over (partition by DimHotelId,
								DimRevenueCenterId,
								DimOrderTypeId,
								TransactionDateTime,
								LineTotal,
								CheckNumber
								order by ItemName
								) as RowC,
ROW_NUMBER() over (partition by DimHotelId,
								DimRevenueCenterId,
								DimOrderTypeId,
								TransactionDateTime,
								LineTotal,
								CheckNumber
								order by ItemName
								) as RowN,
ROW_NUMBER() over (partition by DimHotelId,
								DimRevenueCenterId,
								DimOrderTypeId,
								TransactionDateTime,
								LineTotal,
								CheckNumber
								order by 
									case when ItemName ='Menu' then 99999
									else 1
									end
								) as RowO
from DW.FactMenuItems
where 1=1
and LineTotal is not null
),
cteMenuItem as
(
	select 
	1 as VPart
	,*
	from cteItemNumberNameBase
	where RowC = RowN and RowC = 1
	union all 
	select 
	2 as VPart
	,*
	from cteItemNumberNameBase
	where RowC > 1 and RowO = 1

),
cteTender as
(
select
t.DimHotelId,
t.DimRevenueCenterId,
t.DimOrderTypeId,
t.TransactionDateTime,
t.LineCount,
t.LineTotal,
t.CheckNumber,
t.BusinessDate,
t.ReferenceInfo,
t.ReportLineCount,
--cast(t.DimRevenueCenterId as nvarchar) + cast(t.DimOrderTypeId as nvarchar) + cast(t.TransactionDateTime as nvarchar)+ cast(isnull(t.LineTotal,0) as nvarchar) as LinkToMenuItem,
cast(t.DimRevenueCenterId as nvarchar) +  cast(t.CheckNumber as nvarchar) + cast(t.BusinessDate as nvarchar) as LinkToSummaryTransaction,
CheckNumber + Convert(CHAR(8),BusinessDate,112) as MicroLink,
t.PriceLevel
from DW.FactTender t
where 1=1
--and t.ReportLineCount > 0
and t.PriceLevel =1
--and CheckNumber = 3921
)
select 
	t.DimHotelId,
	t.DimRevenueCenterId,
	t.DimOrderTypeId,
	t.BusinessDate,
	t.TransactionDateTime,
	t.LineCount,
	t.LineTotal as LineTotal,
	round((t.LineTotal / (1+isnull(cast(f.VAT as decimal(2,2)),0))),2) as LineTotalNoVat,
	t.CheckNumber,
	t.ReferenceInfo as Reference,
	m.ItemNumber,
	m.ItemName,
	isnull(f.DimMenuItemFamilyId,0) as DimMenuItemFamilyId,
	isnull(c.DimClubMemberId,0) as DimClubMemberId,
	dbo.fnDayOfWeek(t.BusinessDate) as Weekday,
	isnull(f.VAT,'0') as VAT,
	isnull(rc.DimRecipeId,0) as DimRecipeId,
	rc.TotalCOS as RecipeCost,
	t.MicroLink
--t.*
from cteTender t
inner join cteMenuItem m on 
		m.DimHotelId = t.DimHotelId
		and m.DimRevenueCenterId = t.DimRevenueCenterId
		and m.DimOrderTypeId = t.DimOrderTypeId
		and m.TransactionDateTime = t.TransactionDateTime
		and t.LineTotal = m.LineTotal
		and m.CheckNumber = t.CheckNumber
left join DW.vClubMemberCheckNumber c on c.BusinessDate = t.BusinessDate and c.CheckNumber = t.CheckNumber
left join DW.DimMenuItemFamily f on f.ItemNumber = m.ItemNumber	
left join DW.vFactRecipeAgg rc on rc.RecipeNo = m.ItemNumber

--where t.CheckNumber = '3921'
--UNION ALL
--select 
--'vp2' as ViewPart,
--	t.DimHotelId,
--	t.DimRevenueCenterId,
--	t.DimOrderTypeId,
--	t.BusinessDate,
--	t.TransactionDateTime,
--	t.LineCount,
--	t.LineTotal,
--	t.CheckNumber,
--	t.ReferenceInfo as Reference,
--	null as ItemNumber,
--	null as ItemName,
--	0 as DimMenuItemFamilyId,
--	isnull(c.DimClubMemberId,0) as DimClubMemberId,
--	dbo.fnDayOfWeek(t.BusinessDate) as Weekday,
--	'0' as VAT,
--	0 as RecipeCost,
--	t.MicroLink
----t.*
--from cteTender t
--left join DW.vClubMemberCheckNumber c on c.BusinessDate = t.BusinessDate and c.CheckNumber = t.CheckNumber
--where 1=1
--and t.LineTotal < 0
--and t.PriceLevel =1