﻿

CREATE view [DW].[vDSVDimDate] as 
select 
the_date, 
year,
quarter_of_year,
month_of_year,
day_of_month,
quarter,
month,
day,
quarter_name,
month_name,
day_name,
day_of_week_name,
day_of_week
 from Dw.DimDate