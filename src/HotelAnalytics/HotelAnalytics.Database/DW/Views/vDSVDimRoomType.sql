﻿
  CREATE view [DW].[vDSVDimRoomType] as
  select 
  DimRoomTypeId,
  Label,
  Class,
  Description,
  Suite,
  IsRoom
  From DW.DimRoomType