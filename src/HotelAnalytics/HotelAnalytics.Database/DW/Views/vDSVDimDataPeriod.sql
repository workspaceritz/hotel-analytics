﻿
CREATE view DW.vDSVDimDataPeriod as 
select 
the_date, 
year,
quarter_of_year,
month_of_year
from DW.DimDate d
inner join (select distinct DataPeriod from DW.FactReservation) r on r.DataPeriod = d.the_date