﻿
CREATE view [DW].[vDSVDimTransactionType] as
select 
DimTransactionTypeId,
TransactionTypeCode,
[Group],
SubGroup,
Description 
from DW.DimTransactionType