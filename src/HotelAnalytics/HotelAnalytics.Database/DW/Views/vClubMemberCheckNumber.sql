﻿create view DW.vClubMemberCheckNumber as 
select 
	t.BusinessDate,
	t.CheckNumber,
	t.ReferenceInfo,
	c.DimClubMemberId
	 from (
		select distinct BusinessDate, CheckNumber, ReferenceInfo
		from DW.FactTender t
		where 1=1
		and ISNUMERIC(ReferenceInfo) = 1
		and Len(ReferenceInfo) >8
	) t
	inner join DW.DimClubMember c on c.TelephoneClean = t.ReferenceInfo