﻿CREATE view [DW].[vDSVDimMenuItemFamily] as
select 
DimMenuItemFamilyId,
ItemName,
ItemNumber,
Family,
FamilyName,
VAT,
[Group],
FamilyGroup
from Dw.DimMenuItemFamily