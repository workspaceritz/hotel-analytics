﻿CREATE view [DW].[vDSVDimClubMember] as
select 
DimClubMemberId,
case when DimClubMemberId = 0 Then 'No'
else 'Yes'
end IsClubMember,
TelephoneClean as Code,
Number,
CardNr,
LastName,
FirstName,
Civility,
Gender,
City,
Country
from DW.DimClubMember