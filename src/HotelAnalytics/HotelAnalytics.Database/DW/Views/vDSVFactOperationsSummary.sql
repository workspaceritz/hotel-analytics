﻿CREATE view DW.vDSVFactOperationsSummary as
select 
t.BusinessDate,
t.DimHotelId,
t.DimMarketId,
t.RN,
t.REV,
t.PAX,
t.ADR,
t.PercentOccupancy
from Dw.FactOperationsSummary t
