﻿alter view [DW].[vDSVFactReservation] as
with
DistinctPeriods as (
	select distinct DataPeriod from DW.FactReservation
),
PeriodsBeforeAndAfter as (
	select 	
	p.DataPeriod,
	lag(p.DataPeriod) over (order by DataPeriod) as PreviousDataPeriod,
	lead(p.DataPeriod) over (order by DataPeriod) as NextDataPeriod
	from 
	DistinctPeriods p 
),
BaseQuery as (
	select 
	f.FactReservationId,
	f.DataPeriod,
	f.ReservationDate,
	f.DimMarketId,
	f.DimReservationStatusId,
	f.DimHotelId,
	f.RN,
	f.REV,
	f.PAX,
	f.ReportID,
	p.PreviousDataPeriod,
	p.NextDataPeriod
	from DW.FactReservation f
	inner join DW.DimReservationStatus rs on rs.DimReservationStatusId = f.DimReservationStatusId
	inner join DW.DimMarket m on m.DimMarketId = f.DimMarketId
	inner join DW.DimParentMarket pm on pm.DimParentMarketId = m.DimParentMarketId
	inner join PeriodsBeforeAndAfter p on p.DataPeriod = f.DataPeriod
	where 1=1
	and (
		(
			rs.ReservationStatusCode = 'TRA'
			and pm.ParentMarketCode in ('TRA', 'CMP', 'OTH')
		)
		or
		(
			rs.ReservationStatusCode in ('DEF', 'TE1', 'TE2', 'PRO')
			and pm.ParentMarketCode = 'GRP'
		)
	)	
),
QueryPart2 as (
-- query part 2 - start from "yesterday" and work our way to today and generate facts
select
'2' as ViewPart,
yesterday.FactReservationId * -1 as FactReservationId,
yesterday.NextDataPeriod as DataPeriod,
yesterday.ReservationDate,
yesterday.DimMarketId,
yesterday.DimReservationStatusId,
yesterday.DimHotelId,
0 as RN,
0 as REV,
0 as PAX,
yesterday.ReportID as ReportID,
yesterday.RN as RN_LP,
yesterday.REV as REV_LP,
yesterday.PAX as PAX_LP,
yesterday.RN * -1 as RN_P,
yesterday.REV * -1 as REV_P,
yesterday.PAX * -1 as PAX_P,
yesterday.PreviousDataPeriod,
yesterday.NextDataPeriod
from BaseQuery yesterday
left join DW.FactReservation today on
	yesterday.DimReservationStatusId = today.DimReservationStatusId and
	yesterday.DimMarketId = today.DimMarketId and
	yesterday.ReservationDate = today.ReservationDate and
	yesterday.DimHotelId = today.DimHotelId and
	today.DataPeriod = yesterday.NextDataPeriod
where 1=1 
and today.FactReservationId is null
and yesterday.NextDataPeriod is not null
and yesterday.ReservationDate > yesterday.NextDataPeriod
),
QueryPart1 as (
-- query part 1 - start from "today" and work our way back
select
'1' as ViewPart,
today.FactReservationId,
today.DataPeriod,
today.ReservationDate,
today.DimMarketId,
today.DimReservationStatusId,
today.DimHotelId,
today.RN,
today.REV,
today.PAX,
today.ReportID,
isnull(yesterday.RN, yesterday2.RN) as RN_LP,
isnull(yesterday.REV, yesterday2.REV) as REV_LP,
isnull(yesterday.PAX, yesterday2.PAX) as PAX_LP,
today.RN - isnull(yesterday.RN, yesterday2.RN) as RN_P,
today.REV - isnull(yesterday.REV, yesterday2.REV) as REV_P,
today.PAX - isnull(yesterday.PAX, yesterday2.PAX) as PAX_P,
today.PreviousDataPeriod,
today.NextDataPeriod
from BaseQuery today
left join BaseQuery yesterday on
	today.DimReservationStatusId = yesterday.DimReservationStatusId and
	today.DimMarketId = yesterday.DimMarketId and
	today.ReservationDate = yesterday.ReservationDate and
	today.DimHotelId = yesterday.DimHotelId and
	yesterday.DataPeriod = today.PreviousDataPeriod
left join QueryPart2 yesterday2 on
	today.DimReservationStatusId = yesterday2.DimReservationStatusId and
	today.DimMarketId = yesterday2.DimMarketId and
	today.ReservationDate = yesterday2.ReservationDate and
	today.DimHotelId = yesterday2.DimHotelId and
	yesterday2.DataPeriod = today.PreviousDataPeriod
),
QueryPart3 as (
-- query part 3 - populate "past" data where ReservationDate < DataPeriod
select
'3' as ViewPart,
null as FactReservationId,
dp.DataPeriod,
bq.ReservationDate,
bq.DimMarketId,
bq.DimReservationStatusId,
bq.DimHotelId,
bq.RN,
bq.REV,
bq.PAX,
bq.ReportID,
null as RN_LP,
null as REV_LP,
null as PAX_LP,
null as RN_P,
null as REV_P,
null as PAX_P,
null as PreviousDataPeriod,
null as NextDataPeriod
from 
(
	select
	bq.*
	from 
	(
		select
		bq.*,
		count(DataPeriod) over (partition by bq.ReservationDate, bq.DimMarketId, bq.DimReservationStatusId, DimHotelId) as CountOver,
		row_number() over (partition by bq.ReservationDate, bq.DimMarketId, bq.DimReservationStatusId, DimHotelId order by DataPeriod) as RowNumberOver
		from
		BaseQuery bq	
	) bq
	where 1=1
	and bq.CountOver = bq.RowNumberOver 
) bq
inner join DistinctPeriods dp on dp.DataPeriod > bq.ReservationDate and year(ReservationDate) between year(dp.DataPeriod) - 1 and year(dp.DataPeriod)
)
select
v.ViewPart,
v.FactReservationId,
v.DataPeriod,
v.ReservationDate,
v.DimMarketId,
v.DimReservationStatusId,
v.DimHotelId,
v.RN,
v.REV,
v.PAX,
v.RN_LP,
v.REV_LP,
v.PAX_LP,
v.RN_P,
v.REV_P,
v.PAX_P,
cast(v.ReportID as bigint) as ReportID
from
(
	select * from QueryPart1 pq1
	union all
	select * from QueryPart2 pq2
	union all
	select * from QueryPart3 pq3
) v
;