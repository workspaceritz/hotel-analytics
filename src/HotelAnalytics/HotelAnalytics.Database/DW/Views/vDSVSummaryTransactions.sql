﻿CREATE view [DW].[vDSVSummaryTransactions] as 
select
FactSummaryTransactionsId,
DimHotelId,
OpenDate,
DimRevenueCenterId,
DimOrderTypeId,
DimEmployeeId,
t.CheckNumber as CheckNumber,
SubTotal,
IVA6,
IVA13,
IVA23,
NumberofItems,
NumberofGuests as Covers,
DATEDIFF(Minute,OpenDateTime, CASE when CloseDateTime <=OpenDateTime THEN OpenDateTime else CloseDateTime end ) as LenghtofServiceMinutes,
SubTotal / NULLIF(NumberofItems ,0) as AverageSoldPerItem,
NumberofItems / NULLIF(NumberofGuests,0) as ItemsPerCover,
SubTotal /  NULLIF(NumberofGuests,0) as AverageSoldPerCover,
isnull(c.DimClubMemberId,0) as DimClubMemberId 
from DW.FactSummaryTransactions t
left join DW.vClubMemberCheckNumber c on c.BusinessDate = t.OpenDate and c.CheckNumber = t.CheckNumber
where 1=1