﻿

CREATE view [DW].[vDSVFactTransaction] as

with cteTransaction as
(
	select DimTransactionTypeId,TransactionTypeCode,
	case when TransactionTypeCode like '10%' then 1
	else 0
	end as IsRoom
	 from DW.DimTransactionType
),
cteRoomType as
(
	select rt.DimRoomTypeId, Class,Number, rn.DimRoomNumberId from DW.DimRoomType rt
	left join Dw.DimRoomNumber rn on rt.Label = rn.Label
)
select 
t.DataPeriod as Period, 
t.DimHotelId,
t.DimScenarioId,
t.DimMarketId,
t.DimTransactionTypeId,
t.DimRoomClassId,
t.DebitValue,
t.CreditValue,
tr.IsRoom as RN,
Left(t.ChequeNumber,11) as MicroLink,
iif(LEN(t.ChequeNumber)>14, SUBSTRING(t.ChequeNumber, 1, LEN(t.ChequeNumber) - 14),null) as CheckNumber,
isnull(c.DimClubMemberId,0) as DimClubMemberId,
ot.DimOperaTransactionId,
isnull(rt.DimRoomTypeId,0) as DimRoomTypeId,
isnull(rt.DimRoomNumberId,0) as DimRoomNumberId
from Dw.FactTransaction t
inner join cteTransaction tr on tr.DimTransactionTypeId = t.DimTransactionTypeId
inner join DW.DimRoomClass rc on rc.DimRoomClassId = t.DimRoomClassId
left join cteRoomType rt on rt.class = rc.RoomClassCode and rt.Number = t.Room
inner join DW.DimOperaTransaction ot on ot.TransactionCode = tr.TransactionTypeCode
left join DW.vClubMemberCheckNumber c on c.BusinessDate = iif(len(t.ChequeNumber) >13, try_cast(left(right(t.ChequeNumber,14),8) as Date),'1950-01-01')
						and c.CheckNumber = iif(LEN(t.ChequeNumber)>14, SUBSTRING(t.ChequeNumber, 1, LEN(t.ChequeNumber) - 14),null)