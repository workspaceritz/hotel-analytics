﻿CREATE view [DW].[vDSVDimMarket] as
select 0 as DimMarketId, 'N/A' as MarketCode, 'N/A' as MarketDescription, 0 as DimParentMarketId, 'N/A' as ParentMarketCode
union all
select 
m.DimMarketId,
m.MarketCode,
m.MarketDescription,
pm.DimParentMarketId,
pm.ParentMarketCode
From DW.DimMarket m 
inner join DW.DimParentMarket pm on pm.DimParentMarketId = m.DimParentMarketId