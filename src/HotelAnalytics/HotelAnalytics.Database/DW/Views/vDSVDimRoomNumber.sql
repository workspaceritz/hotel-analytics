﻿
CREATE view [DW].[vDSVDimRoomNumber] as 


select 
DimRoomNumberId,
Label,
Number,
case when ISNUMERIC(Number)=0 
	then null
	else iif(len(Number) <> 3,null,substring(Number,1,1))
	end as Floor,
case when ISNUMERIC(Number)=0 
	then null
	else
		case when Number between 101 and 119 then 'A Wing'
			 when Number between 120 and 131 then 'B Wing'
			 when Number between 201 and 222 then 'A Wing'
			 when Number between 231 and 244 then 'B Wing'
			 when Number = 300  then 'Central Wing'
			 when Number between 301 and 330 then 'A Wing'
			 when Number between 331 and 352 then 'B Wing'
			 when Number between 361 and 376 then 'Central Wing'
			 when Number between 401 and 430 then 'A Wing'
			 when Number between 431 and 452 then 'B Wing'
			 when Number between 461 and 478 then 'Central Wing'
		else null
		end
	end as Wing
from Dw.DimRoomNumber