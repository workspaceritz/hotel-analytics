﻿create view DW.vDSVDimHotel as 
select HotelId as DimHotelId,
HotelCode as Code,
HotelDescription as Description
from Hotel