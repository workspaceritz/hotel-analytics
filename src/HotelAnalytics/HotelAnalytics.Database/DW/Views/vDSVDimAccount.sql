﻿
CREATE view [DW].[vDSVDimAccount] as 
select 
DimAccountId,
AccountCode,
ISNULL(AccountComments,'N/A') as AccountComments,
ISNULL(MarketGroup,'N/A') as MarketGroup,
ISNULL(MarketSegment,'N/A') as MarketSegment,
ISNULL(Type,'N/A') as Type,
ISNULL(WDWE,'N/A') as WDWE
from DW.DimAccount