﻿

CREATE view [DW].[vDSVFactPlanning] as 
with 
cteAccount as
(
	select DimAccountId, AccountCode from DW.DimAccount
),
cteMarket as 
(
	select DimMarketId, MarketCode from DW.DimMarket where MarketCode IN ('GRP','TRA')
),
cteValueType as
(
	select * from DW.vDSVDimValueType
),
cteAccountMarket as 
(
	select DimAccountId,
	case when a.AccountCode like '96133%' --RN
			or a.AccountCode like '96173%' --RN 
			or a.AccountCode like '36133%' --REV
			or a.AccountCode like '36173%' --REV
			then
	(select DimMarketId from cteMarket where MarketCode = 'GRP')
	when a.AccountCode like '9610%' --RN
			or a.AccountCode like '9615%' --RN 
			or a.AccountCode like '3610%' --REV
			or a.AccountCode like '3612%' --REV
			or a.AccountCode like '3615%' --REV
			then
	 (select DimMarketId from cteMarket where MarketCode = 'TRA')
	 else
	 0
	 end as DimMarketId,
	 case when a.AccountCode like '96133%' --RN
			or a.AccountCode like '96173%' --RN 
			or a.AccountCode like '9610%' --RN
			or a.AccountCode like '9615%' --RN 
			
			then
	(select DimValueTypeId from cteValueType where ValueType = 'RN')
		when 
			 a.AccountCode like '36133%' --REV
			or a.AccountCode like '36173%' --REV
			or a.AccountCode like '3610%' --REV
			or a.AccountCode like '3612%' --REV
			or a.AccountCode like '3615%' --REV
			then
	 (select DimValueTypeId from cteValueType where ValueType = 'REV')
	 else
	 0
	 end as DimValueTypeId
	from cteAccount a
)
select
p.DataPeriod as Period,
p.DimAccountId,
p.DimScenarioId,
p.DimHotelId,
p.DimOperatingUnitId,
p.DimDepartmentId,
p.DimLegalEntityId,
p.Value,
am.DimMarketId,
am.DimValueTypeId
from Dw.FactPlanning p
inner join DW.DimAccount a on a.DimAccountId = p.DimAccountId
inner join DW.DimScenario s on s.DimScenarioId = p.DimScenarioId
inner join cteAccountMarket am on am.DimAccountId = a.DimAccountId
where 1=1
--and p.DataPeriod >= '2021-04-01' and p.DataPeriod < '2021-05-01'
--and a.AccountCode like '961%'
--and s.Scenario = 'Budget'