﻿
CREATE view DW.vDSVFactRecipe as
select 
r.DimHotelId,
r.DimRecipeId,
t.DimRevenueCenterId,
t.DimOrderTypeId,
t.DimMenuItemFamilyId,
t.DimClubMemberId,
t.CheckNumber,
t.BusinessDate,
isnull(r.DimRecipeComponentId,0) as DimRecipeComponentId,
t.Count as TenderCount,
--r.Component,
r.POTQTY,
--r.Unit,
r.ACTQTY,
r.COS,
(t.count * r.ACTQTY) as TotACTQTY,
(t.count * r.COS) as TotCOS
from Dw.FactRecipe r
inner join (
select DimHotelId, DimRevenueCenterId,DimOrderTypeId,BusinessDate, DimRecipeId, DimMenuItemFamilyId,DimClubMemberId,CheckNumber, SUM(LineCount) as Count 
from DW.vDSVFactTender 
--where DimRecipeId = 1930 and BusinessDate = '2021-02-20'
group by DimHotelId, DimRevenueCenterId,DimOrderTypeId,BusinessDate, DimRecipeId,DimMenuItemFamilyId,DimClubMemberId,CheckNumber
) t on t.DimHotelId = r.DimHotelId and t.DimRecipeId = r.DimRecipeId