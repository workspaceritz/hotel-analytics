﻿CREATE TABLE [DW].[DimRoomNumber] (
    [DimRoomNumberId] INT            IDENTITY (1, 1) NOT NULL,
    [DimRoomTypeId]   INT            NOT NULL,
    [Label]           VARCHAR (256)  NOT NULL,
    [Number]          VARCHAR (256)  NOT NULL,
    [Created]         DATETIME       CONSTRAINT [DimRoomNumber_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       NVARCHAR (128) CONSTRAINT [DimRoomNumber_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]        DATETIME       CONSTRAINT [DimRoomNumber_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      NVARCHAR (128) CONSTRAINT [DimRoomNumber_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimRoomNumber] PRIMARY KEY CLUSTERED ([DimRoomNumberId] ASC),
    CONSTRAINT [UNQ_DimRoomNumber] UNIQUE NONCLUSTERED ([DimRoomTypeId] ASC, [Label] ASC, [Number] ASC)
);

