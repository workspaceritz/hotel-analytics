﻿CREATE TABLE [DW].[DimOperaTransaction] (
    [DimOperaTransactionId] INT            IDENTITY (1, 1) NOT NULL,
    [TransactionCode]       VARCHAR (256)  NOT NULL,
    [Description]           VARCHAR (MAX)  NOT NULL,
    [Account]               VARCHAR (256)  NOT NULL,
    [Dept]                  VARCHAR (256)  NOT NULL,
    [Exclude]               BIT            NOT NULL,
    [InputVATFlag]          BIT            NOT NULL,
    [OutputVATFlag]         BIT            NOT NULL,
    [VATApplicableCode]     VARCHAR (256)  NOT NULL,
    [VATTaxPercentage]      VARCHAR (256)  NOT NULL,
    [OffsetAccount]         VARCHAR (256)  NOT NULL,
    [OffsetDept]            VARCHAR (256)  NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimOperaTransaction_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimOperaTransaction_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimOperaTransaction_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimOperaTransaction_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimOperaTransaction] PRIMARY KEY CLUSTERED ([DimOperaTransactionId] ASC),
    CONSTRAINT [UNQ_DimOperaTransaction] UNIQUE NONCLUSTERED ([TransactionCode] ASC)
);

