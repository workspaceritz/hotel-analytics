﻿CREATE TABLE [DW].[DimRecipe] (
    [DimRecipeId] INT            IDENTITY (1, 1) NOT NULL,
    [RecipeNo]    VARCHAR (256)  NOT NULL,
    [Recipe]      VARCHAR (256)  NOT NULL,
    [Created]     DATETIME       CONSTRAINT [DimRepice_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]   NVARCHAR (128) CONSTRAINT [DimRepice_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]    DATETIME       CONSTRAINT [DimRepice_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]  NVARCHAR (128) CONSTRAINT [DimRepice_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimRepice] PRIMARY KEY CLUSTERED ([DimRecipeId] ASC),
    CONSTRAINT [UNQ_DimRepice] UNIQUE NONCLUSTERED ([RecipeNo] ASC)
);

