﻿CREATE TABLE [DW].[DimMenuItemFamily] (
    [DimMenuItemFamilyId] INT            IDENTITY (1, 1) NOT NULL,
    [ItemName]            VARCHAR (256)  NOT NULL,
    [ItemNumber]          VARCHAR (256)  NOT NULL,
    [Family]              VARCHAR (256)  NOT NULL,
    [FamilyName]          VARCHAR (256)  NOT NULL,
    [Group]               VARCHAR (256)  NOT NULL,
    [FamilyGroup]         VARCHAR (256)  NOT NULL,
    [VAT]                 VARCHAR (256)  NOT NULL,
    [Created]             DATETIME       CONSTRAINT [DimMenuItemFamily_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]           NVARCHAR (128) CONSTRAINT [DimMenuItemFamily_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]            DATETIME       CONSTRAINT [DimMenuItemFamily_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]          NVARCHAR (128) CONSTRAINT [DimMenuItemFamily_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimMenuItemFamily] PRIMARY KEY CLUSTERED ([DimMenuItemFamilyId] ASC),
    CONSTRAINT [UNQ_DimMenuItemFamily] UNIQUE NONCLUSTERED ([ItemNumber] ASC)
);



