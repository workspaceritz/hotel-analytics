﻿CREATE TABLE [DW].[DimEmployee] (
    [DimEmployeeId] INT            IDENTITY (1, 1) NOT NULL,
    [Employee]      VARCHAR (256)  NOT NULL,
    [Created]       DATETIME       CONSTRAINT [DimEmployee_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     NVARCHAR (128) CONSTRAINT [DimEmployee_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]      DATETIME       CONSTRAINT [DimEmployee_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    NVARCHAR (128) CONSTRAINT [DimEmployee_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimEmployee] PRIMARY KEY CLUSTERED ([DimEmployeeId] ASC),
    CONSTRAINT [UNQ_DimEmployee] UNIQUE NONCLUSTERED ([Employee] ASC)
);

