﻿CREATE TABLE [DW].[DimReservationStatus] (
    [DimReservationStatusId]      INT IDENTITY (1, 1) NOT NULL,
    [ReservationStatusCode]       NVARCHAR (255) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimReservationStatus_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimReservationStatus_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimReservationStatus_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimReservationStatus_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimReservationStatus] PRIMARY KEY CLUSTERED ([DimReservationStatusId] ASC),
    CONSTRAINT [UNQ_DimReservationStatus] UNIQUE NONCLUSTERED ([ReservationStatusCode] ASC)
);

