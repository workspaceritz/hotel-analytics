﻿CREATE TABLE [DW].[DimLegalEntity] (
    [DimLegalEntityId]      INT IDENTITY (1, 1) NOT NULL,
    [LegalEntityCode]       VARCHAR(256) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimLegalEntity_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimLegalEntity_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimLegalEntity_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimLegalEntity_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimLegalEntity] PRIMARY KEY CLUSTERED ([DimLegalEntityId] ASC),
    CONSTRAINT [UNQ_DimLegalEntity] UNIQUE NONCLUSTERED ([LegalEntityCode])
);
GO
