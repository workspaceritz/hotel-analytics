﻿CREATE TABLE [DW].[DimMarketOperaMapping] (
    [DimMarketOperaMappingId] INT            IDENTITY (1, 1) NOT NULL,
    [DimMarketId]             INT            NULL,
    [BofTransCode]            NVARCHAR (255) NOT NULL,
    [BofTransText]            NVARCHAR (255) NOT NULL,
    [BofValue]                INT            NOT NULL,
    [Created]                 DATETIME       CONSTRAINT [MarketOperaMapping_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]               NVARCHAR (128) CONSTRAINT [MarketOperaMapping_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]                DATETIME       CONSTRAINT [MarketOperaMapping_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]              NVARCHAR (128) CONSTRAINT [MarketOperaMapping_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_MarketOperaMapping] PRIMARY KEY CLUSTERED ([DimMarketOperaMappingId] ASC),
    CONSTRAINT [FK_MarketOperaMapping_DimMarket] FOREIGN KEY ([DimMarketId]) REFERENCES [DW].[DimMarket] ([DimMarketId]),
    CONSTRAINT [UNQ_MarketOperaMapping] UNIQUE NONCLUSTERED ([DimMarketId] ASC, [BofTransCode] ASC, [BofValue] ASC)
);


GO
CREATE NONCLUSTERED INDEX [FK_MarketOperaMapping_DimMarket]
    ON [DW].[DimMarketOperaMapping]([DimMarketId] ASC);

