﻿CREATE TABLE [DW].[FactTransaction] (
    [FactTransactionId]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [DataPeriod]                DATE            NOT NULL,
    [DimHotelId]                INT             NOT NULL,
    [DimScenarioId]             INT             NOT NULL,
    [DimMarketId]               INT             NOT NULL,
    [DimTransactionTypeId]      INT             NOT NULL,
    [DimRoomClassId]            INT             NOT NULL,    
    [ReceiptNo]                 NVARCHAR (255)  NOT NULL,
    [GuestFullName]             NVARCHAR (255)  NOT NULL,
    [TrxDesc]                   NVARCHAR (255)  NOT NULL,
    [Reference]                 NVARCHAR (2048)  NOT NULL,
    [TrxNo]                     NVARCHAR (255)  NOT NULL,
    [Room]                      NVARCHAR (255)  NOT NULL,
    [CreditCardSupplement]      NVARCHAR (2048)  NOT NULL,
    [Remark]                    NVARCHAR (255)  NOT NULL,
    [ChequeNumber]              NVARCHAR (255)  NOT NULL,
    [DebitValue]                DECIMAL (18, 6) NOT NULL,
    [CreditValue]               DECIMAL (18, 6) NOT NULL,
    [BatchId]                   BIGINT          NOT NULL,
    [Created]                   DATETIME        CONSTRAINT [FactTransaction_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                 NVARCHAR (128)  CONSTRAINT [FactTransaction_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]                  DATETIME        CONSTRAINT [FactTransaction_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]                NVARCHAR (128)  CONSTRAINT [FactTransaction_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_FactTransaction] PRIMARY KEY CLUSTERED ([FactTransactionId] ASC),
    CONSTRAINT [FK_FactTransaction_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [FK_FactTransaction_DimDate] FOREIGN KEY ([DataPeriod]) REFERENCES [DW].[DimDate] ([the_date]),
    CONSTRAINT [FK_FactTransaction_DimHotel] FOREIGN KEY ([DimHotelId]) REFERENCES [Hotel] ([HotelId]),
    CONSTRAINT [FK_FactTransaction_DimScenario] FOREIGN KEY ([DimScenarioId]) REFERENCES [DW].[DimScenario] ([DimScenarioId]),
    CONSTRAINT [FK_FactTransaction_DimMarket] FOREIGN KEY ([DimMarketId]) REFERENCES [DW].[DimMarket] ([DimMarketId]),
    CONSTRAINT [FK_FactTransaction_DimTransactionType] FOREIGN KEY ([DimTransactionTypeId]) REFERENCES [DW].[DimTransactionType] ([DimTransactionTypeId]),
    CONSTRAINT [FK_FactTransaction_DimRoomClass] FOREIGN KEY ([DimRoomClassId]) REFERENCES [DW].[DimRoomClass] ([DimRoomClassId]),    
);
GO

CREATE NONCLUSTERED INDEX [IX_FactTransaction_N1]
    ON [DW].[FactTransaction]([DataPeriod] ASC, [DimHotelId] ASC, [DimScenarioId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactTransaction_Batch]
    ON [DW].[FactTransaction]([BatchId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_FactTransaction_DataPeriod]
    ON [DW].[FactTransaction]([DataPeriod] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactTransaction_DimHotel]
    ON [DW].[FactTransaction]([DimHotelId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactTransaction_DimScenario]
    ON [DW].[FactTransaction]([DimScenarioId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactTransaction_DimMarket]
    ON [DW].[FactTransaction]([DimMarketId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactTransaction_DimTransactionType]
    ON [DW].[FactTransaction]([DimTransactionTypeId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactTransaction_DimRoomClass]
    ON [DW].[FactTransaction]([DimRoomClassId] ASC);
GO
