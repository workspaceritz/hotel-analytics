﻿CREATE TABLE [DW].[FactReservation] (
    [FactReservationId]      BIGINT          IDENTITY (1, 1) NOT NULL,
    [DataPeriod]             DATE            NOT NULL,
    [ReservationDate]        DATE            NOT NULL,
    [DimMarketId]            INT             NOT NULL,
    [DimReservationStatusId] INT             NOT NULL,
    [DimHotelId]             INT             NOT NULL,
    [RN]                     INT             NOT NULL,
    [REV]                    DECIMAL (18, 6) NOT NULL,
    [PAX]                    INT             NOT NULL,
    [ReportID]               NVARCHAR (128)  NOT NULL,
    [BatchId]                BIGINT          NOT NULL,
    [Created]                DATETIME        CONSTRAINT [FactReservation_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]              NVARCHAR (128)  CONSTRAINT [FactReservation_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]               DATETIME        CONSTRAINT [FactReservation_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]             NVARCHAR (128)  CONSTRAINT [FactReservation_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_FactReservation] PRIMARY KEY CLUSTERED ([FactReservationId] ASC),
    CONSTRAINT [FK_FactReservation_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [FK_FactReservation_DimDate] FOREIGN KEY ([ReservationDate]) REFERENCES [DW].[DimDate] ([the_date]),
    CONSTRAINT [FK_FactReservation_DimMarket] FOREIGN KEY ([DimMarketId]) REFERENCES [DW].[DimMarket] ([DimMarketId]),
    CONSTRAINT [FK_FactReservation_DimReservationStatus] FOREIGN KEY ([DimReservationStatusId]) REFERENCES [DW].[DimReservationStatus] ([DimReservationStatusId])
);
GO

CREATE NONCLUSTERED INDEX [FK_FactReservation_DimReservationStatus]
    ON [DW].[FactReservation]([DimReservationStatusId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactReservation_DimMarket]
    ON [DW].[FactReservation]([DimMarketId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactReservation_DimDate]
    ON [DW].[FactReservation]([ReservationDate] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactReservation_Batch]
    ON [DW].[FactReservation]([BatchId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_FactReservation_DataPeriod]
    ON [DW].[FactReservation]([DataPeriod] ASC);
GO
