﻿CREATE TABLE [DW].[DimDate] (
    [the_date]             DATE          NOT NULL,
    [year]                 SMALLINT      NOT NULL,
    [quarter_of_year]      TINYINT       NOT NULL,
    [month_of_year]        TINYINT       NOT NULL,
    [day_of_year]          INT           NOT NULL,
    [day_of_month]         TINYINT       NOT NULL,
    [quarter]              INT           NOT NULL,
    [month]                INT           NOT NULL,
    [day]                  INT           NOT NULL,
    [day_of_week]          TINYINT       NOT NULL,
    [is_working_day]       INT           NOT NULL,
    [is_weekend]           INT           NOT NULL,
    [quarter_name]         NVARCHAR (20) NOT NULL,
    [month_name]           NVARCHAR (20) NOT NULL,
    [day_name]             NVARCHAR (15) NOT NULL,
    [day_of_week_name]     NVARCHAR (15) NOT NULL,
    [quarter_of_year_name] NVARCHAR (50) NOT NULL,
    [month_of_year_name]   NVARCHAR (20) NOT NULL,
    [is_working_day_name]  NVARCHAR (20) NOT NULL,
    [week]                 INT           NOT NULL,
    [week_of_year]         INT           NOT NULL,
    [week_name]            NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_Data] PRIMARY KEY CLUSTERED ([the_date] ASC)
);

