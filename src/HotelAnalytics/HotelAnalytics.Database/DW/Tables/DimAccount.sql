﻿CREATE TABLE [DW].[DimAccount] (
    [DimAccountId]    INT            IDENTITY (1, 1) NOT NULL,
    [AccountCode]     VARCHAR (256)  NOT NULL,
    [MarketGroup]     VARCHAR (256)  NULL,
    [MarketSegment]   VARCHAR (256)  NULL,
    [AccountComments] VARCHAR (256)  NULL,
    [Type]            VARCHAR (256)  NULL,
    [WDWE]            VARCHAR (256)  NULL,
    [DptFrom]         VARCHAR (256)  NULL,
    [DptTo]           VARCHAR (256)  NULL,
    [Created]         DATETIME       CONSTRAINT [DimAccount_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]       NVARCHAR (128) CONSTRAINT [DimAccount_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]        DATETIME       CONSTRAINT [DimAccount_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]      NVARCHAR (128) CONSTRAINT [DimAccount_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimAccount] PRIMARY KEY CLUSTERED ([DimAccountId] ASC),
    CONSTRAINT [UNQ_DimAccount] UNIQUE NONCLUSTERED ([AccountCode] ASC)
);




GO
