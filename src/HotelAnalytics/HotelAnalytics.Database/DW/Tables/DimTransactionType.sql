﻿CREATE TABLE [DW].[DimTransactionType] (
    [DimTransactionTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [TransactionTypeCode]  VARCHAR (256)  NOT NULL,
    [Group]                VARCHAR (256)  NOT NULL,
    [SubGroup]             VARCHAR (256)  NOT NULL,
    [Description]          VARCHAR (MAX)  NOT NULL,
    [TaxInclusive]         BIT            CONSTRAINT [DF_DimTransactionType_TaxInclusive] DEFAULT ((0)) NOT NULL,
    [FrequentFlyer]        BIT            CONSTRAINT [DF_DimTransactionType_FrequentFlyer] DEFAULT ((0)) NOT NULL,
    [ManualPostAllowed]    BIT            CONSTRAINT [DF_DimTransactionType_ManualPostAllowed] DEFAULT ((0)) NOT NULL,
    [AdjTrxCode]           VARCHAR (256)  NULL,
    [IndRevenueGP]         BIT            CONSTRAINT [DF_DimTransactionType_IndRevenueGP] DEFAULT ((0)) NOT NULL,
    [IndAR]                BIT            CONSTRAINT [DF_DimTransactionType_IndAR] DEFAULT ((0)) NOT NULL,
    [IndBilling]           BIT            CONSTRAINT [DF_DimTransactionType_IndBilling] DEFAULT ((0)) NOT NULL,
    [IndCash]              BIT            CONSTRAINT [DF_DimTransactionType_IndCash] DEFAULT ((0)) NOT NULL,
    [IndDeposit]           BIT            CONSTRAINT [DF_DimTransactionType_IndDeposit] DEFAULT ((0)) NOT NULL,
    [Created]              DATETIME       CONSTRAINT [DimTransactionType_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            NVARCHAR (128) CONSTRAINT [DimTransactionType_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]             DATETIME       CONSTRAINT [DimTransactionType_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           NVARCHAR (128) CONSTRAINT [DimTransactionType_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimTransactionType] PRIMARY KEY CLUSTERED ([DimTransactionTypeId] ASC),
    CONSTRAINT [UNQ_DimTransactionType] UNIQUE NONCLUSTERED ([TransactionTypeCode] ASC)
);


GO
