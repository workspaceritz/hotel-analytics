﻿CREATE TABLE [DW].[DimMarket] (
    [DimMarketId]           INT IDENTITY (1, 1) NOT NULL,
    [MarketCode]            VARCHAR(256) NOT NULL,
    [MarketDescription]     VARCHAR(256) NULL,
    [SellSequence]          INT NULL,
    [DimParentMarketId]      INT NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimMarket_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimMarket_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimMarket_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimMarket_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimMarket] PRIMARY KEY CLUSTERED ([DimMarketId] ASC),
    CONSTRAINT [FK_DimMarket_DimParentMarket] FOREIGN KEY ([DimParentMarketId]) REFERENCES [DW].[DimParentMarket] ([DimParentMarketId]),
    CONSTRAINT [UNQ_DimMarket] UNIQUE NONCLUSTERED ([MarketCode])
);
GO

CREATE NONCLUSTERED INDEX [FK_DimMarket_DimParentMarket]
    ON [DW].[DimMarket]([DimParentMarketId] ASC);
GO
