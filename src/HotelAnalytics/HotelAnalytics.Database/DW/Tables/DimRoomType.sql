﻿CREATE TABLE [DW].[DimRoomType] (
    [DimRoomTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Label]         VARCHAR (256)  NOT NULL,
    [Class]         VARCHAR (256)  NOT NULL,
    [Description]   VARCHAR (256)  NOT NULL,
    [Suite]         BIT            NOT NULL,
    [IsRoom]        BIT            NOT NULL,
    [Created]       DATETIME       CONSTRAINT [DimRoomType_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]     NVARCHAR (128) CONSTRAINT [DimRoomType_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]      DATETIME       CONSTRAINT [DimRoomType_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]    NVARCHAR (128) CONSTRAINT [DimRoomType_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimRoomType] PRIMARY KEY CLUSTERED ([DimRoomTypeId] ASC),
    CONSTRAINT [UNQ_DimRoomType] UNIQUE NONCLUSTERED ([Label] ASC)
);



