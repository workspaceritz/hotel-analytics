﻿CREATE TABLE [DW].[FactPlanning] (
    [FactPlanningId]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [DataPeriod]         DATE            NOT NULL,
    [DimHotelId]         INT             NOT NULL,
    [DimScenarioId]      INT             NOT NULL,
    [DimOperatingUnitId] INT             NOT NULL,
    [DimDepartmentId]    INT             NOT NULL,
    [DimLegalEntityId]   INT             NOT NULL,
    [DimAccountId]       INT             NOT NULL,
    [Value]              DECIMAL (18, 6) NOT NULL,
    [BatchId]            BIGINT          NOT NULL,
    [Created]            DATETIME        CONSTRAINT [FactPlanning_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          NVARCHAR (128)  CONSTRAINT [FactPlanning_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]           DATETIME        CONSTRAINT [FactPlanning_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         NVARCHAR (128)  CONSTRAINT [FactPlanning_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_FactPlanning] PRIMARY KEY CLUSTERED ([FactPlanningId] ASC),
    CONSTRAINT [FK_FactPlanning_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [FK_FactPlanning_DimAccount] FOREIGN KEY ([DimAccountId]) REFERENCES [DW].[DimAccount] ([DimAccountId]),
    CONSTRAINT [FK_FactPlanning_DimDate] FOREIGN KEY ([DataPeriod]) REFERENCES [DW].[DimDate] ([the_date]),
    CONSTRAINT [FK_FactPlanning_DimDepartment] FOREIGN KEY ([DimDepartmentId]) REFERENCES [DW].[DimDepartment] ([DimDepartmentId]),
    CONSTRAINT [FK_FactPlanning_DimHotel] FOREIGN KEY ([DimHotelId]) REFERENCES [dbo].[Hotel] ([HotelId]),
    CONSTRAINT [FK_FactPlanning_DimLegalEntity] FOREIGN KEY ([DimLegalEntityId]) REFERENCES [DW].[DimLegalEntity] ([DimLegalEntityId]),
    CONSTRAINT [FK_FactPlanning_DimOperatingUnit] FOREIGN KEY ([DimOperatingUnitId]) REFERENCES [DW].[DimOperatingUnit] ([DimOperatingUnitId]),
    CONSTRAINT [FK_FactPlanning_DimScenario] FOREIGN KEY ([DimScenarioId]) REFERENCES [DW].[DimScenario] ([DimScenarioId]),
    CONSTRAINT [UNQ_FactPlanning] UNIQUE NONCLUSTERED ([DataPeriod] ASC, [DimHotelId] ASC, [DimScenarioId] ASC, [DimOperatingUnitId] ASC, [DimDepartmentId] ASC, [DimLegalEntityId] ASC, [DimAccountId] ASC)
);




GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_Batch]
    ON [DW].[FactPlanning]([BatchId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_FactPlanning_DataPeriod]
    ON [DW].[FactPlanning]([DataPeriod] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_DimHotel]
    ON [DW].[FactPlanning]([DimHotelId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_DimScenario]
    ON [DW].[FactPlanning]([DimScenarioId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_DimOperatingUnit]
    ON [DW].[FactPlanning]([DimOperatingUnitId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_DimDepartment]
    ON [DW].[FactPlanning]([DimDepartmentId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_DimLegalEntity]
    ON [DW].[FactPlanning]([DimLegalEntityId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactPlanning_DimAccount]
    ON [DW].[FactPlanning]([DimAccountId] ASC);
GO
