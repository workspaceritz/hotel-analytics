﻿CREATE TABLE [DW].[DimRevenueCenter] (
    [DimRevenueCenterId] INT            IDENTITY (1, 1) NOT NULL,
    [RevenueCenter]      VARCHAR (256)  NOT NULL,
    [Created]            DATETIME       CONSTRAINT [DimRevenueCenter_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]          NVARCHAR (128) CONSTRAINT [DimRevenueCenter_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]           DATETIME       CONSTRAINT [DimRevenueCenter_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]         NVARCHAR (128) CONSTRAINT [DimRevenueCenter_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimRevenueCenter] PRIMARY KEY CLUSTERED ([DimRevenueCenterId] ASC),
    CONSTRAINT [UNQ_DimRevenueCenter] UNIQUE NONCLUSTERED ([RevenueCenter] ASC)
);

