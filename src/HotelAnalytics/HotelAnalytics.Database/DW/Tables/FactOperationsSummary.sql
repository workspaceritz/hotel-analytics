﻿CREATE TABLE [DW].[FactOperationsSummary] (
    [FactOperationsSummaryId]         BIGINT          IDENTITY (1, 1) NOT NULL,
    [BusinessDate]                DATE            NOT NULL,
    [DimHotelId]                INT             NOT NULL,
    [DimMarketId]               INT             NOT NULL,
    [RN]                        DECIMAL (18, 6) NOT NULL,
	[REV]                       DECIMAL (18, 6) NOT NULL,
	[PAX]                       DECIMAL (18, 6) NOT NULL,
	[ADR]                       DECIMAL (18, 6) NOT NULL,
	[PercentOccupancy]          DECIMAL (18, 6) NOT NULL,    
    [BatchId]                   BIGINT          NOT NULL,
    [Created]                   DATETIME        CONSTRAINT [FactOperationsSummary_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]                 NVARCHAR (128)  CONSTRAINT [FactOperationsSummary_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]                  DATETIME        CONSTRAINT [FactOperationsSummary_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]                NVARCHAR (128)  CONSTRAINT [FactOperationsSummary_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_FactOperationsSummary] PRIMARY KEY CLUSTERED ([FactOperationsSummaryId] ASC),
    CONSTRAINT [FK_FactOperationsSummary_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId]),
    CONSTRAINT [FK_FactOperationsSummary_DimDate] FOREIGN KEY ([BusinessDate]) REFERENCES [DW].[DimDate] ([the_date]),
    CONSTRAINT [FK_FactOperationsSummary_DimHotel] FOREIGN KEY ([DimHotelId]) REFERENCES [Hotel] ([HotelId]),
    CONSTRAINT [FK_FactOperationsSummary_DimMarket] FOREIGN KEY ([DimMarketId]) REFERENCES [DW].[DimMarket] ([DimMarketId]),
    CONSTRAINT [UNQ_FactOperationsSummary] UNIQUE NONCLUSTERED ([BusinessDate] ASC, [DimHotelId] ASC, [DimMarketId] ASC),
);
GO

CREATE NONCLUSTERED INDEX [FK_FactOperationsSummary_Batch]
    ON [DW].[FactOperationsSummary]([BatchId] ASC);
GO

CREATE NONCLUSTERED INDEX [IX_FactOperationsSummary_BusinessDate]
    ON [DW].[FactOperationsSummary]([BusinessDate] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactOperationsSummary_DimHotel]
    ON [DW].[FactOperationsSummary]([DimHotelId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_FactOperationsSummary_DimMarket]
    ON [DW].[FactOperationsSummary]([DimMarketId] ASC);
GO
