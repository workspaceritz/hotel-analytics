﻿CREATE TABLE [DW].[DimRecipeComponent] (
    [DimRecipeComponentId] INT            IDENTITY (1, 1) NOT NULL,
    [ArticleNo]            NVARCHAR (256) NULL,
    [Component]            NVARCHAR (256) NOT NULL,
    [Unit]                 NVARCHAR (256) NOT NULL,
    [BU]                   NVARCHAR (256) NOT NULL,
    [Created]              DATETIME       CONSTRAINT [DimRecipeComponente_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]            NVARCHAR (128) CONSTRAINT [DimRecipeComponent_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]             DATETIME       CONSTRAINT [DimRecipeComponent_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]           NVARCHAR (128) CONSTRAINT [DimRecipeComponent_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimRecipeComponent] PRIMARY KEY CLUSTERED ([DimRecipeComponentId] ASC)
);

