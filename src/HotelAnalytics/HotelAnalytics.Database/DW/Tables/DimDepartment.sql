﻿CREATE TABLE [DW].[DimDepartment] (
    [DimDepartmentId]       INT IDENTITY (1, 1) NOT NULL,
    [DepartmentCode]        VARCHAR(256) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimDepartment_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimDepartment_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimDepartment_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimDepartment_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimDepartment] PRIMARY KEY CLUSTERED ([DimDepartmentId] ASC),
    CONSTRAINT [UNQ_DimDepartment] UNIQUE NONCLUSTERED ([DepartmentCode])
);
GO
