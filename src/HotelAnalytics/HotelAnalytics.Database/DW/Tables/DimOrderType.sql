﻿CREATE TABLE [DW].[DimOrderType] (
    [DimOrderTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [OrderType]      VARCHAR (256)  NOT NULL,
    [Created]        DATETIME       CONSTRAINT [DimOrderType_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]      NVARCHAR (128) CONSTRAINT [DimOrderType_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]       DATETIME       CONSTRAINT [DimOrderType_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]     NVARCHAR (128) CONSTRAINT [DimOrderType_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimOrderType] PRIMARY KEY CLUSTERED ([DimOrderTypeId] ASC),
    CONSTRAINT [UNQ_DimOrderType] UNIQUE NONCLUSTERED ([OrderType] ASC)
);

