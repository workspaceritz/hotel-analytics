﻿CREATE TABLE [DW].[DimScenario] (
    [DimScenarioId]         INT IDENTITY (1, 1) NOT NULL,
    [Scenario]              VARCHAR(256) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimScenario_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimScenario_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimScenario_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimScenario_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimScenario] PRIMARY KEY CLUSTERED ([DimScenarioId] ASC),
    CONSTRAINT [UNQ_DimScenario] UNIQUE NONCLUSTERED ([Scenario])
);
GO
