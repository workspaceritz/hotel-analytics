﻿CREATE TABLE [DW].[DimRoomClass] (
    [DimRoomClassId]        INT IDENTITY (1, 1) NOT NULL,
    [RoomClassCode]         VARCHAR(256) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimRoomClass_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimRoomClass_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimRoomClass_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimRoomClass_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimRoomClass] PRIMARY KEY CLUSTERED ([DimRoomClassId] ASC),
    CONSTRAINT [UNQ_DimRoomClass] UNIQUE NONCLUSTERED ([RoomClassCode])
);
GO
