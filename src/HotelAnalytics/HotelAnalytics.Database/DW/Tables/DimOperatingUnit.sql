﻿CREATE TABLE [DW].[DimOperatingUnit] (
    [DimOperatingUnitId]    INT IDENTITY (1, 1) NOT NULL,
    [OperatingUnitCode]     VARCHAR(256) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimOperatingUnit_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimOperatingUnit_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimOperatingUnit_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimOperatingUnit_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimOperatingUnit] PRIMARY KEY CLUSTERED ([DimOperatingUnitId] ASC),
    CONSTRAINT [UNQ_DimOperatingUnit] UNIQUE NONCLUSTERED ([OperatingUnitCode])
);
GO
