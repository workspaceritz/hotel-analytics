﻿CREATE TABLE [DW].[DimParentMarket] (
    [DimParentMarketId]      INT IDENTITY (1, 1) NOT NULL,
    [ParentMarketCode]       NVARCHAR (255) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DimParentMarket_Created_df] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DimParentMarket_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME       CONSTRAINT [DimParentMarket_Modified_df] DEFAULT (getdate()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128) CONSTRAINT [DimParentMarket_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimParentMarket] PRIMARY KEY CLUSTERED ([DimParentMarketId] ASC),
    CONSTRAINT [UNQ_DimParentMarket] UNIQUE NONCLUSTERED ([ParentMarketCode] ASC)
);

