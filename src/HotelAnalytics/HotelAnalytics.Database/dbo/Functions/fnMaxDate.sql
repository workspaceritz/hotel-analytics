﻿
CREATE FUNCTION [dbo].[fnMaxDate] (@value1 date, @value2 date)  
	RETURNS date
AS
BEGIN 
	DECLARE @Max date
	SELECT @Max = CASE
					WHEN @value1 > @value2
					THEN @value1
					ELSE @value2
				  END
	RETURN @Max
END