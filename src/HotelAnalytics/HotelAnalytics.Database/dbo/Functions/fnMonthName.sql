﻿CREATE function [dbo].[fnMonthName](@m tinyint)
returns nvarchar(30)
as
begin
	return 
		case @m
		when 1 then N'January'
		when 2 then N'February'
		when 3 then N'March'
		when 4 then N'April'
		when 5 then N'May'
		when 6 then N'June'
		when 7 then N'July'
		when 8 then N'August'
		when 9 then N'September'
		when 10 then N'October'
		when 11 then N'November'
		when 12 then N'December'
		end
end