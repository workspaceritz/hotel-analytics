﻿create function GenerateMonthlyDates
(	
	@DateFrom date, 
	@DateTo date
)
returns table 
as
return 
(
	with T(date)
	as
	( 
		select @DateFrom 
		union all
		select dateadd(month,1,T.date) from T where T.date < format(@DateTo, 'yyyyMM') + '01'
	)
	select 
	cast(date as date) as date,
	format(@DateTo, 'yyyyMM') + '01' as RealEndDate
	from T 
);