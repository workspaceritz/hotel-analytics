﻿CREATE FUNCTION [dbo].[fnGetConfiguration](
	@LookupValue as nvarchar(512))
RETURNS nvarchar(512)
AS
BEGIN
	DECLARE @LookupSetId int = (SELECT LookupSetId FROM LookupSet WHERE LookupSet = N'Configurations')
	DECLARE @retLookupMappedValue nvarchar(512)
	SELECT @retLookupMappedValue = LookupMappedValue
	FROM LookupValue
	WHERE LookupSetId = @LookupSetId AND LookupValue = @LookupValue
	
	IF (@retLookupMappedValue IS NULL)
		RETURN CAST('Could not find configuration with value ''' + @LookupValue + '''.' as int)

	RETURN @retLookupMappedValue
END