﻿CREATE function [dbo].[fnQuarterName](@q as int)
returns nvarchar(20)
as
begin
	return 'Q' + cast(@q as nchar(1)) 
end