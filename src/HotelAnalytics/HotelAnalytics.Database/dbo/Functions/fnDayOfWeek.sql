﻿CREATE function [dbo].[fnDayOfWeek](@date datetime2(7)) returns tinyint
as
begin
	return (datepart(weekday, @date)  + @@DATEFIRST - 1 - 1) % 7 + 1
end