﻿CREATE  function [dbo].[fnWorkDayName](@wd tinyint)
returns nvarchar(20)
as
begin
	return 
		case when  @wd <= 5 then N'Work day' else N'Non-work day' end
end