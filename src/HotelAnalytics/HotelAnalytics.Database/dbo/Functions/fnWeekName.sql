﻿CREATE function [dbo].[fnWeekName](@wk as int, @date as datetime2(7))
returns nvarchar(20)
as
begin
	return 'Semana ' + dbo.fnFirstWeekDay(@date) + ' a ' +  dbo.fnLastWeekDay(@date)
end