﻿create function GenerateDailyDates
(	
	@DateFrom date, 
	@DateTo date
)
returns table 
as
return 
(
	with T(date)
	as
	( 
		select @DateFrom 
		union all
		select dateadd(day,1,T.date) from T where T.date < format(@DateTo, 'yyyyMMdd')
	)
	select 
	cast(date as date) as date,
	format(@DateTo, 'yyyyMMdd') as RealEndDate
	from T 
);
go