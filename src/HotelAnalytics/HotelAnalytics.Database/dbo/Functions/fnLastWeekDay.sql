﻿CREATE function [dbo].[fnLastWeekDay](@date datetime2(7)) 
returns nvarchar(5)
as
begin
	declare @dayNum nvarchar(2)
	SET @dayNum = convert(nvarchar(2),DATEPART(day,DATEADD(dd,-(DATEPART(dw, @date) - 7),@date)))
	declare @monthNum nvarchar(2)
	SET @monthNum = convert(nvarchar(2),DATEPART(month,DATEADD(dd,-(DATEPART(dw, @date) - 7),@date)))
	return  @dayNum + '-' + @monthNum
END