﻿CREATE  function [dbo].[fnWeekDayName](@wd tinyint)
returns nvarchar(20)
as
begin
	return 
		case @wd
			when 1 then N'Monday'
			when 2 then N'Tuesday'
			when 3 then N'Wednesday'
			when 4 then N'Thursday'
			when 5 then N'Friday'
			when 6 then N'Saturday'
			when 7 then N'Sunday' 
		end
end