﻿
CREATE FUNCTION [dbo].[fnMinDate] (@value1 date, @value2 date)  
	RETURNS date
AS
BEGIN 
	DECLARE @Min date
	SELECT @Min = CASE
					WHEN @value1 < @value2
					THEN @value1
					ELSE @value2
				  END
	RETURN @Min
END