﻿CREATE  FUNCTION [dbo].[fnDateOnly] (@date datetime) RETURNS datetime
AS
BEGIN
	RETURN CONVERT(datetime,CONVERT(varchar,@date,112),1)
END