﻿CREATE TABLE [dbo].[LookupSet] (
    [LookupSetId] INT            IDENTITY (1, 1) NOT NULL,
    [LookupSet]   NVARCHAR (255) NOT NULL,
    [Created]     DATETIME2 (7)  CONSTRAINT [LookupSet_Created_df] DEFAULT (sysdatetime()) NOT NULL,
    [CreatedBy]   NVARCHAR (128) CONSTRAINT [LookupSet_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]    DATETIME2 (7)  CONSTRAINT [LookupSet_Modified_df] DEFAULT (sysdatetime()) NOT NULL,
    [ModifiedBy]  NVARCHAR (128) CONSTRAINT [LookupSet_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_LookupSet] PRIMARY KEY CLUSTERED ([LookupSetId] ASC),
    CONSTRAINT [UQ_LookupSet] UNIQUE NONCLUSTERED ([LookupSet] ASC)
);

