﻿CREATE TABLE [dbo].[BatchMessage] (
    [BatchMessageId]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [BatchId]               BIGINT         NOT NULL,
    [ErrorType]             NVARCHAR (100) NOT NULL,
    [ErrorMessage]          NVARCHAR (4000) NOT NULL,
    [Created]               DATETIME       CONSTRAINT [DF_BatchMessage_Created] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]             NVARCHAR (128) CONSTRAINT [DF_BatchMessage_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_BatchMessage] PRIMARY KEY CLUSTERED ([BatchMessageId] ASC),
    CONSTRAINT [FK_BatchMessage_Batch] FOREIGN KEY ([BatchId]) REFERENCES [dbo].[Batch] ([BatchId])
);
GO

CREATE NONCLUSTERED INDEX [FK_BatchMessage_Batch]
    ON [dbo].[BatchMessage]([BatchId] ASC);
GO
