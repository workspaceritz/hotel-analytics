﻿CREATE TABLE [dbo].[DataSource] (
    [DataSourceId]          INT             IDENTITY (1, 1) NOT NULL,
    [DataSourceCode]        NVARCHAR (256)  NOT NULL,
    [DataSourceName]        NVARCHAR (512)  NOT NULL,
    [DataSourceDescription] NVARCHAR (4000) NULL,
    [Created]               DATETIME2 (7)   CONSTRAINT [DataSource_Created_df] DEFAULT (sysdatetime()) NOT NULL,
    [CreatedBy]             NVARCHAR (128)  CONSTRAINT [DataSource_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]              DATETIME2 (7)   CONSTRAINT [DataSource_Modified_df] DEFAULT (sysdatetime()) NOT NULL,
    [ModifiedBy]            NVARCHAR (128)  CONSTRAINT [DataSource_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DataSource] PRIMARY KEY CLUSTERED ([DataSourceId] ASC),
    CONSTRAINT [UNQ_DataSource_BK] UNIQUE NONCLUSTERED ([DataSourceCode] ASC)
);






GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Column added by AutoAudit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DataSource', @level2type = N'COLUMN', @level2name = N'ModifiedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Column added by AutoAudit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DataSource', @level2type = N'COLUMN', @level2name = N'Modified';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Column added by AutoAudit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DataSource', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Column added by AutoAudit', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'DataSource', @level2type = N'COLUMN', @level2name = N'Created';


GO


