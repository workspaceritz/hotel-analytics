﻿CREATE TABLE [dbo].[Batch] (
    [BatchId]      BIGINT         IDENTITY (1, 1) NOT NULL,
    [IsProcessed]  BIT            NOT NULL,
    [DataSourceId] INT            NOT NULL,
    [HotelId]      INT            NOT NULL,
    [FileName]     NVARCHAR (255) NOT NULL,
    [UserName]     NVARCHAR (50)  NOT NULL,
    [Created]      DATETIME       CONSTRAINT [DF_Batch_Created] DEFAULT (getdate()) NOT NULL,
    [CreatedBy]    NVARCHAR (128) CONSTRAINT [DF_Batch_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_Batch] PRIMARY KEY CLUSTERED ([BatchId] ASC),
    CONSTRAINT [FK_Batch_DataSource] FOREIGN KEY ([DataSourceId]) REFERENCES [dbo].[DataSource] ([DataSourceId]),
    CONSTRAINT [FK_Batch_Hotel] FOREIGN KEY ([HotelId]) REFERENCES [dbo].[Hotel] ([HotelId])
);
GO

CREATE NONCLUSTERED INDEX [FK_Batch_DataSource]
    ON [dbo].[Batch]([DataSourceId] ASC);
GO

CREATE NONCLUSTERED INDEX [FK_Batch_Hotel]
    ON [dbo].[Batch]([HotelId] ASC);
GO