﻿CREATE TABLE [dbo].[Hotel] (
    [HotelId]          INT            IDENTITY (1, 1) NOT NULL,
    [HotelCode]        NVARCHAR (50)  NOT NULL,
    [HotelDescription] NVARCHAR (255) NOT NULL,
    [Created]          DATETIME2 (7)  CONSTRAINT [DF_DimHotel_Created] DEFAULT (sysdatetime()) NOT NULL,
    [CreatedBy]        NVARCHAR (128) CONSTRAINT [DF_DimHotel_CreatedBy] DEFAULT (suser_sname()) NOT NULL,
    [Modified]         DATETIME2 (7)  CONSTRAINT [DF_DimHotel_Modified] DEFAULT (sysdatetime()) NOT NULL,
    [ModifiedBy]       NVARCHAR (128) CONSTRAINT [DF_DimHotel_ModifiedBy] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_DimHotel] PRIMARY KEY CLUSTERED ([HotelId] ASC)
);

