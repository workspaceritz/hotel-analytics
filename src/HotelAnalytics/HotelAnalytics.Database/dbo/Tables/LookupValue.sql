﻿CREATE TABLE [dbo].[LookupValue] (
    [LookupValueId]     INT            IDENTITY (1, 1) NOT NULL,
    [LookupSetId]       INT            NOT NULL,
    [LookupValue]       NVARCHAR (512) NOT NULL,
    [LookupMappedValue] NVARCHAR (512) NULL,
    [UniqueHash]        AS             (CONVERT([varbinary](20),hashbytes('SHA2_512',CONVERT([nvarchar](1024),[LookupSetId])+[LookupValue]))) PERSISTED,
    [Created]           DATETIME2 (7)  CONSTRAINT [LookupValue_Created_df] DEFAULT (sysdatetime()) NOT NULL,
    [CreatedBy]         NVARCHAR (128) CONSTRAINT [LookupValue_CreatedBy_df] DEFAULT (suser_sname()) NOT NULL,
    [Modified]          DATETIME2 (7)  CONSTRAINT [LookupValue_Modified_df] DEFAULT (sysdatetime()) NOT NULL,
    [ModifiedBy]        NVARCHAR (128) CONSTRAINT [LookupValue_ModifiedBy_df] DEFAULT (suser_sname()) NOT NULL,
    CONSTRAINT [PK_LookupValue] PRIMARY KEY CLUSTERED ([LookupValueId] ASC),
    CONSTRAINT [FK_LookupValue_LookupSet] FOREIGN KEY ([LookupSetId]) REFERENCES [dbo].[LookupSet] ([LookupSetId]),
    CONSTRAINT [UQ_LookupValue] UNIQUE NONCLUSTERED ([UniqueHash] ASC)
);




GO
CREATE NONCLUSTERED INDEX [FK_LookupValue_LookupSet]
    ON [dbo].[LookupValue]([LookupSetId] ASC);

