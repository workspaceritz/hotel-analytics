﻿CREATE TABLE [dbo].[LogTable] (
    [LogTableId] INT             IDENTITY (1, 1) NOT NULL,
    [SPID]       SMALLINT        CONSTRAINT [DF_LogTable_SPID] DEFAULT (@@spid) NOT NULL,
    [Date]       DATETIME2 (7)   CONSTRAINT [DF_LogTable_Date] DEFAULT (sysdatetime()) NOT NULL,
    [LogLevel]   INT             NOT NULL,
    [Module]     NVARCHAR (255)  NOT NULL,
    [Msg]        NVARCHAR (2048) NULL,
    CONSTRAINT [PK_LogTable] PRIMARY KEY CLUSTERED ([LogTableId] ASC)
);

