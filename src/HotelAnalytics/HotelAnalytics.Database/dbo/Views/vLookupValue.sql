﻿CREATE VIEW vLookupValue
AS
SELECT        
lv.LookupValueId,
lv.LookupSetId,
ls.LookupSet,
lv.LookupValue,
lv.LookupMappedValue,
lv.UniqueHash,
lv.Created,
lv.CreatedBy,
lv.Modified,
lv.ModifiedBy
FROM            
LookupValue lv
INNER JOIN LookupSet ls ON lv.LookupSetId = ls.LookupSetId