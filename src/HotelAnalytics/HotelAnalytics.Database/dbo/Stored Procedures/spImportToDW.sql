﻿CREATE PROCEDURE [dbo].[spImportToDW]
	@batchId int,
	@dataPeriod date,
	@username nvarchar(128)
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRAN

			EXEC LogInfo 'spImportToDW', 'Begin'
			declare @debugMsg varchar(400)
			declare @countErrors int

			declare @dataSourceCode nvarchar(50)
			declare @dataSourceName nvarchar(250)

			select @dataSourceCode = ds.DataSourceCode, 
			@dataSourceName = ds.DataSourceName
			from DataSource ds
			inner join Batch b on ds.DataSourceId = b.DataSourceId
			where b.BatchId = @batchId


			set @debugMsg = 'ImportToDW loader ' + @datasourcename + ' for BatchId ' + cast(@batchId as nvarchar(10))
			exec LogInfo 'spImportToDW', @debugMsg

			DECLARE @sql nvarchar(MAX)
			DECLARE @paramlist nvarchar(4000)
			SELECT @sql = 'EXEC spImportToDW_«DATASOURCE» @paramidBatch, @paramusername, @paramdataperiod'
			SELECT @sql = REPLACE(@sql, '«DATASOURCE»', @dataSourceCode)
			SELECT @paramlist = '@paramidBatch int, @paramusername nvarchar(128), @paramdataperiod date'
			EXEC sp_executesql @sql, @paramlist, @batchId, @username, @dataPeriod

			UPDATE Batch SET IsProcessed = 1 WHERE BatchId = @batchId
			
			EXEC LogInfo 'spImportToDW', 'End'
		commit tran
	end try
	BEGIN CATCH
		--might need to rollback
			IF XACT_STATE() = -1
			BEGIN
				ROLLBACK TRAN
			END
			ELSE
			BEGIN
				IF (@@TRANCOUNT = 1)
				BEGIN
					ROLLBACK TRAN
				END
				ELSE IF (@@TRANCOUNT > 1)
				BEGIN
					-- Nested transaction. We can't rollback from here or else we will
					-- get the SQL Msg 266 about erroneous transaction counts after SP
					-- completion. Instead we Commit this block of work trusting that
					-- the caller will eventually Rollback at the outermost level when
					-- it sees my error code.
					COMMIT TRAN
				END
			END
			DECLARE @ErrMsg nvarchar(MAX), @ErrSeverity int, @ErrSource nvarchar(50)
			SELECT @ErrMsg = ERROR_MESSAGE(),@ErrSeverity = ERROR_SEVERITY(), @ErrSource = ERROR_PROCEDURE()
			IF @ErrSource IS NOT NULL OR @ErrSource <> N'' SELECT @ErrMsg = @ErrMsg +  N'. Source: ' + @ErrSource
			exec LogError 'spImportToDW', @ErrMsg
			RAISERROR(@ErrMsg, @ErrSeverity, 1)
	END CATCH

	EXEC LogInfo 'spImportToDW', 'End'
END