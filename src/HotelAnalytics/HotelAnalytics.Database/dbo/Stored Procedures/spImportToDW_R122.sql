﻿create procedure [dbo].[spImportToDW_R122]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	declare @DatetimeBeforeMergeOperation datetime;
	declare @DatetimeAfterMergeOperation datetime;
		
	exec LogInfo @SP, 'Begin';
		
	declare @DimReservationStatusId int;
	declare @HotelId int;
	declare @myDataPeriod date;

	select
	s.RESERVATION_DATE as ReservationDate,
	--s.MARKET_CODE,
	m.DimMarketId,
	--s.RESERVATION_STATUS,
	rs.DimReservationStatusId,
	s.NO_DEFINITE_ROOMS as RN,
	s.TOTAL_REVENUE as REV,
	s.NO_OF_GUESTS as PAX,
	s.REPORT_ID as ReportID,
	@batchid as BatchId	
	into #tempR122
	from Staging.R122 s
	inner join DW.DimMarket m on m.MarketCode = s.MARKET_CODE
	inner join vLookupValue c on c.LookupSet = 'ReservationStatus_Mapping' and s.RESERVATION_STATUS = c.LookupValue
	inner join DW.DimReservationStatus rs on rs.ReservationStatusCode = c.LookupMappedValue
	where BatchId = @batchid;

	select @myDataPeriod = min(ReservationDate) from #tempR122;

	select top 1 @DimReservationStatusId = DimReservationStatusId from #tempR122;

	select @HotelId = HotelId from Batch where BatchId = @batchid;

	--filldimdate
	declare @maxDate datetime;
	declare @minDate datetime;

	select @maxDate = max(ReservationDate), @minDate = min(ReservationDate) from #tempR122;

	exec spFillDimDate @minDate, @maxDate;

	exec spFillDimDate @myDataPeriod, @myDataPeriod;
		
	exec LogInfo @SP, 'Merge FactReservation Begin';
	merge DW.FactReservation as target 
	using (
		select
		@myDataPeriod as DataPeriod,
		t.*
		from
		#tempR122 t
	) as source
	on 1=1
	and source.DimReservationStatusId = target.DimReservationStatusId
	and source.DimMarketId = target.DimMarketId
	and source.ReservationDate = target.ReservationDate
	and source.DataPeriod = target.DataPeriod
	and target.DimHotelId = @HotelId
	when not matched by target then
		insert (
			DataPeriod,
			ReservationDate,
			DimMarketId,
			DimReservationStatusId,
			DimHotelId,
			RN,
			REV,
			PAX,
			ReportID,
			BatchId,
			CreatedBy, 
			ModifiedBy
		) values (
			source.DataPeriod,
			source.ReservationDate,
			source.DimMarketId,
			source.DimReservationStatusId,
			@HotelId,
			source.RN,
			source.REV,
			source.PAX,
			source.ReportID,
			source.BatchId,
			@username, 
			@username
		)
	when matched then
		update set
			target.RN = source.RN,
			target.REV = source.REV,
			target.PAX = source.PAX,
			target.ReportID = source.ReportID,
			target.BatchId = source.BatchId,
			target.ModifiedBy = @username
	--when not matched by source
	--		and target.DimReservationStatusId = @DimReservationStatusId
	--		and target.DataPeriod = @dataPeriod 
	--		and target.DimHotelId = @HotelId
	--then
	--	delete
	;

	exec LogInfo @SP, 'Merge FactReservation End';

	drop table #tempR122;


	exec LogInfo @SP, 'End';
end;