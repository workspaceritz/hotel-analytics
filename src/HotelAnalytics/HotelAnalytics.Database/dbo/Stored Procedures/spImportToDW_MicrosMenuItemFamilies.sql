﻿

CREATE procedure [dbo].[spImportToDW_MicrosMenuItemFamilies]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		select * 
		into #tmpMicrosMenuItemFamilies
		from (
			select 
			ItemName,
			ItemNumber,
			Family,
			FamilyName,
			VAT,
			GroupCode as [Group],
			FamilyGroup,
			ROW_NUMBER()  over(PARTITION BY ItemNumber order by ItemNumber) as RowRank
		
			from Staging.MicrosMenuItemFamilies
			where 1=1
			and ItemNumber is not null
			and ItemName is not null
			and Family is not null
			and FamilyName is not null
			and VAT is not null
			and GroupCode is not null
			and FamilyGroup is not null
			and Batchid = @batchid
		) a
		where RowRank = 1

		exec LogInfo @SP, 'Merge DW.DimMenuItemFamily';
		merge DW.DimMenuItemFamily as target 
		using (
			select distinct 
			ItemName,
			ItemNumber,
			Family,
			FamilyName,
			VAT,
			[Group],
			FamilyGroup
			from
			#tmpMicrosMenuItemFamilies
		
		) as source
		on source.ItemNumber = target.ItemNumber
		when not matched by target then
			insert (
				ItemName,
				ItemNumber,
				Family,
				FamilyName,
				VAT,
				[Group],
				FamilyGroup,
				CreatedBy, 
				ModifiedBy
			) values (
				source.ItemName,
				source.ItemNumber,
				source.Family,
				source.FamilyName,
				source.VAT,
				source.[Group],
				source.FamilyGroup,
				@username, 
				@username
			)
		when matched then
			update set
				target.ItemName = source.ItemName,
				target.Family= source.Family,
				target.FamilyName= source.FamilyName,
				target.VAT= source.VAT,
				target.[Group]= source.[Group],
				target.FamilyGroup= source.FamilyGroup,
				target.ModifiedBy = @username
		;


		drop table #tmpMicrosMenuItemFamilies;

		exec LogInfo @SP, 'End';
end;