﻿CREATE PROCEDURE [dbo].[spImportToDW_OperaPlanningCommon]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
AS
BEGIN
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	exec LogInfo @SP, 'Begin';
	declare @DatetimeBeforeMergeOperation datetime;
	declare @DatetimeAfterMergeOperation datetime;

	select *
	into #tmp
	from Staging.OperaPlanning
	where BatchId = @batchid;
	
	-- populate dims

	exec LogInfo @SP, 'Merge DimOperatingUnit';
	merge DW.DimOperatingUnit as target 
	using (
		select distinct OperatingUnit from #tmp
	) as source
	on source.OperatingUnit  = target.OperatingUnitCode
	when not matched by target then
		insert (
			OperatingUnitCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.OperatingUnit,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimDepartment';
	merge DW.DimDepartment as target 
	using (
		select distinct Department from #tmp
	) as source
	on source.Department  = target.DepartmentCode
	when not matched by target then
		insert (
			DepartmentCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.Department,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimLegalEntity';
	merge DW.DimLegalEntity as target 
	using (
		select distinct LegalEntity from #tmp
	) as source
	on source.LegalEntity  = target.LegalEntityCode
	when not matched by target then
		insert (
			LegalEntityCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.LegalEntity,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimAccount';
	merge DW.DimAccount as target 
	using (
		select distinct Account from #tmp
	) as source
	on source.Account  = target.AccountCode
	when not matched by target then
		insert (
			AccountCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.Account,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Prepare intermediate table (unpivot staging)';

	declare @Year int;
	select top 1 @Year = Year from #tmp;

	create table #tmpUnpivoted (
		DataPeriod		date,
		Scenario		nvarchar(255),
		OperatingUnit	nvarchar(255),
		Department		nvarchar(255),
		LegalEntity		nvarchar(255),
		Account			nvarchar(255),
		Value			decimal(18, 6)
	);

	declare @Period datetime;
	declare @cPeriods cursor;
	set @cPeriods = cursor local fast_forward for
	select date from GenerateMonthlyDates(cast(@Year as nvarchar(255)) + '-01-01', cast(@Year as nvarchar(255)) + '-12-01');
	open @cPeriods;
	fetch next from @cPeriods into @Period;
	while @@FETCH_STATUS = 0
	begin
		declare @sql nvarchar(max);
		declare @paramlist nvarchar(4000);
		select @sql = 
		   '
			insert into #tmpUnpivoted
			select 
			@DataPeriod as Period,
			stg.Scenario, 
			stg.OperatingUnit,
			stg.Department, 
			stg.LegalEntity, 
			stg.Account, 
			stg.CY_M«MM» as Value			
			from #tmp stg
			';
		
		select @sql = replace(@sql, '«MM»', format(@Period, 'MM'));
		select @paramlist = '@DataPeriod datetime';
		execute sp_executesql @sql, @paramlist, @Period;

		fetch next from @cPeriods into @Period;
	end;
	close @cPeriods;
	deallocate @cPeriods;

	----filldimdate
	declare @maxDate datetime;
	declare @minDate datetime;

	select @maxDate = max(DataPeriod), @minDate = min(DataPeriod) from #tmpUnpivoted;

	exec spFillDimDate @minDate, @maxDate;

	declare @HotelId int;
	select @HotelId = HotelId from Batch where BatchId = @batchid;

	declare @DimScenarioId int;
	select top 1 @DimScenarioId = s.DimScenarioId
	from 
	(select top 1 Scenario from #tmpUnpivoted) t
	inner join vLookupValue lv on lv.LookupSet = 'Scenario_OperaPlanning_Mapping' and lv.LookupValue = t.Scenario
	inner join DW.DimScenario s on s.Scenario = lv.LookupMappedValue

	exec LogInfo @SP, 'Delete FactPlanning';
	delete from DW.FactPlanning
	where 1=1
	and DimHotelId = @HotelId
	and DimScenarioId = @DimScenarioId
	and DataPeriod in (
		select distinct DataPeriod from #tmpUnpivoted
	);

	exec LogInfo @SP, 'Merge FactPlanning Begin';
	merge DW.FactPlanning as target 
	using (
		select
		t.DataPeriod,
		@HotelId as DimHotelId,
		@DimScenarioId as DimScenarioId,
		ou.DimOperatingUnitId,
		d.DimDepartmentId,
		le.DimLegalEntityId,
		a.DimAccountId,
		t.Value,
		@batchid as BatchId
		from
		#tmpUnpivoted t
		inner join DW.DimOperatingUnit ou on ou.OperatingUnitCode = t.OperatingUnit
		inner join DW.DimDepartment d on d.DepartmentCode = t.Department
		inner join DW.DimLegalEntity le on le.LegalEntityCode = t.LegalEntity
		inner join DW.DimAccount a on a.AccountCode = t.Account
		where 1=1
		and t.Value is not null
	) as source
	on 1=1
	and source.DataPeriod = target.DataPeriod
	and source.DimHotelId = target.DimHotelId
	and source.DimScenarioId = target.DimScenarioId
	and source.DimOperatingUnitId = target.DimOperatingUnitId
	and source.DimDepartmentId = target.DimDepartmentId
	and source.DimLegalEntityId = target.DimLegalEntityId
	and source.DimAccountId = target.DimAccountId
	when not matched by target then
		insert (
			DataPeriod,
			DimHotelId,
			DimScenarioId,
			DimOperatingUnitId,
			DimDepartmentId,
			DimLegalEntityId,
			DimAccountId,
			Value,
			BatchId,
			CreatedBy,
			ModifiedBy
		) values (
			source.DataPeriod,
			source.DimHotelId,
			source.DimScenarioId,
			source.DimOperatingUnitId,
			source.DimDepartmentId,
			source.DimLegalEntityId,
			source.DimAccountId,
			source.Value,
			source.BatchId,
			@username, 
			@username
		)
	when matched then
		update set
			target.Value = source.Value,
			target.BatchId = source.BatchId,
			target.ModifiedBy = @username
	;

	exec LogInfo @SP, 'Merge FactPlanning End';

	drop table #tmpUnpivoted;
	drop table #tmp;


	exec LogInfo @SP, 'End';
END