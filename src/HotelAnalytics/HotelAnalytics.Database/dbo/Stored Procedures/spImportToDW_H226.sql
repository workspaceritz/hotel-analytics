﻿
CREATE procedure [dbo].[spImportToDW_H226]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
	exec LogInfo @SP, 'Begin';
	declare @DatetimeBeforeMergeOperation datetime;
	declare @DatetimeAfterMergeOperation datetime;

	select *
	into #tmpH226
	from Staging.H226
	where 1=1
	and TRX_CODE is not null
	and Batchid = @batchid;

	exec LogInfo @SP, 'Merge DimOperaTransaction';
	merge DW.DimOperaTransaction as target 
	using (
				select 
					TRX_CODE as TransactionCode,
					Description,
					BOF_CODE1 as Account,
					BOF_CODE2 as Dept,
					BOF_CODE3 as OffsetAccount,
					BOF_CODE4 as OffsetDept,
					BOF_CODE5 as VATApplicableCode,
					BOF_CODE6 as VATTaxPercentage,
					case when BOF_CODE16='Y' then 1 else 0 end as InputVATFlag,
					case when BOF_CODE17= 'Y' then 1 else 0 end as OutputVATFlag,
					case WHEN BOF_CODE18='Y' then 1 else 0 end as Exclude
					from(
					select TRX_CODE,Description, BOF_TRANS_CODE, BOF_VALUE from #tmpH226
					) as s
					PIVOT
					(
					max(BOF_VALUE)
					FOR BOF_TRANS_CODE IN (BOF_CODE1,BOF_CODE2,BOF_CODE3,BOF_CODE4,BOF_CODE5,BOF_CODE6,BOF_CODE16,BOF_CODE17,BOF_CODE18)
					) as pvt
	) as source
	on source.TransactionCode = target.TransactionCode
	when not matched by target then
		insert (
			TransactionCode,
			Description,
			Account,
			Dept,
			Exclude,
			InputVATFlag,
			OutputVATFlag,
			VATApplicableCode,
			VATTaxPercentage,
			OffsetAccount,
			OffsetDept,
			CreatedBy, 
			ModifiedBy
		) values (
			source.TransactionCode,
			source.Description,
			source.Account,
			source.Dept,
			source.Exclude,
			source.InputVATFlag,
			source.OutputVATFlag,
			source.VATApplicableCode,
			source.VATTaxPercentage,
			source.OffsetAccount,
			source.OffsetDept,
			@username, 
			@username
		)
		when matched then
		update set
			target.Description = source.Description,
			target.Account = source.Account,
			target.Dept = source.Dept,
			target.Exclude = source.Exclude,
			target.InputVATFlag = source.InputVATFlag,
			target.OutputVATFlag = source.OutputVATFlag,
			target.VATApplicableCode = source.VATApplicableCode,
			target.VATTaxPercentage = source.VATTaxPercentage,
			target.OffsetAccount = source.OffsetAccount,
			target.OffsetDept = source.OffsetDept

	;

		
	drop table #tmpH226;

	exec LogInfo @SP, 'End';
end;