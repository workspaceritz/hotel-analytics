﻿CREATE PROCEDURE [dbo].[spDeleteStaging_OperaPlanningBudget]
	@batchId int
AS
BEGIN	
	SET NOCOUNT ON;
	DELETE FROM Staging.OperaPlanning where BatchId = @batchId;
END