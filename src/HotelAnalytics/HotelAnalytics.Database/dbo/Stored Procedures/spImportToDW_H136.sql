﻿CREATE procedure [dbo].[spImportToDW_H136]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		select * 
		into #tmpH136
		from Staging.H136
		where BatchId = @batchid;

		exec LogInfo @SP, 'Merge DimParentMarket';
		merge DW.DimParentMarket as target 
		using (
			select distinct 
			PARENT_MARKET_CODE
			from
			#tmpH136
		) as source
		on source.PARENT_MARKET_CODE = target.ParentMarketCode
		when not matched by target then
			insert (
				ParentMarketCode,
				CreatedBy, 
				ModifiedBy
			) values (
				source.PARENT_MARKET_CODE,
				@username, 
				@username
			)
		;


		exec LogInfo @SP, 'Merge DimMarket';
		merge DW.DimMarket as target 
		using (
				select 
			MarketCode,
			MarketDescription,
			SellSequence,
			dmg.DimParentMarketId
			from (
			select 
				stg.MARKET_CODE as MarketCode,
				stg.DESCRIPTION as MarketDescription,
				PARENT_MARKET_CODE,
				stg.SELL_SEQUENCE as SellSequence
			--dmg.DimParentMarketId
			from
			#tmpH136 stg
			UNION ALL
				select distinct 
				PARENT_MARKET_CODE as MarketCode,
				PARENT_MARKET_CODE as MarketDescription,
				PARENT_MARKET_CODE,
				0 as SellSequence
				from
				#tmpH136
				where PARENT_MARKET_CODE NOT IN (select MARKET_CODE from #tmpH136)
			) a 
			inner join DW.DimParentMarket dmg on dmg.ParentMarketCode = a.PARENT_MARKET_CODE
		) as source
		on source.MarketCode = target.MarketCode
		when not matched by target then
			insert (
				MarketCode,
				MarketDescription,
				SellSequence,
				DimParentMarketId,
				CreatedBy, 
				ModifiedBy
			) values (
				source.MarketCode,
				source.MarketDescription,
				source.SellSequence,
				source.DimParentMarketId,
				@username, 
				@username
			)
		when matched then
			update set
				target.MarketDescription = source.MarketDescription,
				target.SellSequence = source.SellSequence,
				target.DimParentMarketId = source.DimParentMarketId
		;
		
		drop table #tmpH136;

		exec LogInfo @SP, 'End';
end;
go