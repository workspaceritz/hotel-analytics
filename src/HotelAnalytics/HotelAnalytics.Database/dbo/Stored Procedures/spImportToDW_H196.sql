﻿
CREATE procedure [dbo].[spImportToDW_H196]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		select * 
		into #tmpH196
		from (
			select * 
			,ROW_NUMBER() over(partition by TRX_CODE order by TRX_CODE) as RowC
			from Staging.H196
			where 1=1
			and TRX_CODE is not null
			and Batchid = @batchid
			) a
		where RowC = 1

		exec LogInfo @SP, 'Merge DimTransactionType';
		merge DW.DimTransactionType as target 
		using (

			select 
			TRX_CODE as TransactionTypeCode
			,TC_GROUP as [Group]
			,TC_SUBGROUP as SubGroup
			,DESCRIPTION as Description
			,REPLACE(REPLACE(TAX_INCLUSIVE_YN,'Y',1),'N',0) as TaxInclusive
			,REPLACE(REPLACE(FREQUENT_FLYER_YN,'Y',1),'N',0) as FrequentFlyer
			,REPLACE(REPLACE(IS_MANUAL_POST_ALLOWED,'Y',1),'N',0) as ManualPostAllowed
			,ADJ_TRX_CODE as AdjTrxCode
			,REPLACE(REPLACE(IND_REVENUE_GP,'Y',1),'N',0) as IndRevenueGP
			,REPLACE(REPLACE(IND_AR,'Y',1),'N',0) as IndAR
			,REPLACE(REPLACE(IND_BILLING,'Y',1),'N',0) as IndBilling
			,REPLACE(REPLACE(IND_CASH,'Y',1),'N',0) as IndCash
			,REPLACE(REPLACE(IND_DEPOSIT_YN,'Y',1),'N',0) as IndDeposit
			from #tmpH196
		) as source
		on source.TransactionTypeCode = target.TransactionTypeCode
		when not matched by target then
			insert (
				TransactionTypeCode,
				[Group],
				SubGroup,
				Description,
				TaxInclusive,
				FrequentFlyer,
				ManualPostAllowed,
				AdjTrxCode,
				IndRevenueGP,
				IndAR,
				IndBilling,
				IndCash,
				IndDeposit,
				CreatedBy, 
				ModifiedBy
			) values (
				source.TransactionTypeCode,
				source.[Group],
				source.SubGroup,
				source.Description,
				source.TaxInclusive,
				source.FrequentFlyer,
				source.ManualPostAllowed,
				source.AdjTrxCode,
				source.IndRevenueGP,
				source.IndAR,
				source.IndBilling,
				source.IndCash,
				source.IndDeposit,
				@username, 
				@username
			)
		when matched then
		update set
			[Group] = source.[Group],
			SubGroup = source.SubGroup ,
			Description= source.Description ,
			TaxInclusive= source.TaxInclusive ,
			FrequentFlyer= source.FrequentFlyer ,
			ManualPostAllowed= source.ManualPostAllowed ,
			AdjTrxCode= source.AdjTrxCode ,
			IndRevenueGP= source.IndRevenueGP ,
			IndAR= source.IndAR ,
			IndBilling= source.IndBilling ,
			IndCash= source.IndCash ,
			IndDeposit= source.IndDeposit ,
			ModifiedBy = @username
	;

		drop table #tmpH196;

		exec LogInfo @SP, 'End';
end;