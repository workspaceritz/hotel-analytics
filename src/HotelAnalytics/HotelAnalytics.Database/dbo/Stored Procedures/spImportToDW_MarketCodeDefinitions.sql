﻿

CREATE procedure [dbo].[spImportToDW_MarketCodeDefinitions]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		select *
		into #tmpMarketCodeDefinitions
		from Staging.MarketCodeDefinitions
		where  Batchid = @batchid


		exec LogInfo @SP, 'Merge DW.DimAccount';
		merge DW.DimAccount as target 
		using (
			select  
				Account as AccountCode,
				MarketGroup,
				MarketSegment,
				AccountComments,
				Type,
				WDWE,
				DptFrom,
				DptTo
			from
			#tmpMarketCodeDefinitions
		) as source
		on source.AccountCode = target.AccountCode
		when not matched by target then
			insert (
				AccountCode,
				MarketGroup,
				MarketSegment,
				AccountComments,
				Type,
				WDWE,
				DptFrom,
				DptTo,
				CreatedBy, 
				ModifiedBy
			) values (
				source.AccountCode,
				source.MarketGroup,
				source.MarketSegment,
				source.AccountComments,
				source.Type,
				source.WDWE,
				source.DptFrom,
				source.DptTo,
				@username, 
				@username
			)
		when matched then
			update set
				target.MarketGroup= source.MarketGroup,
				target.MarketSegment= source.MarketSegment,
				target.AccountComments= source.AccountComments,
				target.Type= source.Type,
				target.WDWE= source.WDWE,
				target.DptFrom=source.DptFrom,
				target.DptTo=source.DptTo,
				target.ModifiedBy = @username
		;


		drop table #tmpMarketCodeDefinitions;

		exec LogInfo @SP, 'End';
end;