﻿
CREATE procedure [dbo].[spImportToDW_MicrosMenuItems]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		select * 
		into #tmpMicrosMenuItems
		from Staging.MicrosMenuItems
		where 1=1
		and TransactionDateTime is not null
		and Batchid = @batchid

		declare @BusinessDates as date
		select top 1 @BusinessDates = BusinessDates from #tmpMicrosMenuItems


		declare @HotelId int;
		select @HotelId = HotelId from Batch where BatchId = @batchid;
		--filldimdate
		declare @maxDate datetime;
		declare @minDate datetime;

		select @maxDate = max(TransactionDateTime), @minDate = min(TransactionDateTime) from #tmpMicrosMenuItems;


		exec LogInfo @SP, 'Merge DimRevenueCenter';
		merge DW.DimRevenueCenter as target 
		using (
			select distinct 
			RevenueCenter
			from
			#tmpMicrosMenuItems
			where RevenueCenter is not null
		) as source
		on source.RevenueCenter = target.RevenueCenter
		when not matched by target then
			insert (
				RevenueCenter,
				CreatedBy, 
				ModifiedBy
			) values (
				source.RevenueCenter,
				@username, 
				@username
			)
		;

		exec LogInfo @SP, 'Merge DimOrderType';
		merge DW.DimOrderType as target 
		using (
			select distinct 
			OrderType
			from
			#tmpMicrosMenuItems
			where OrderType is not null
		) as source
		on source.OrderType = target.OrderType
		when not matched by target then
			insert (
				OrderType,
				CreatedBy, 
				ModifiedBy
			) values (
				source.OrderType,
				@username, 
				@username
			)
		;

		exec LogInfo @SP, 'Merge DimEmployee';
		--TODO check if relevant, multiple employee columns


		exec LogInfo @SP, 'Delete Current BusinessDates from FactMenuItems Begin';

		delete from DW.FactMenuItems where BusinessDates = @BusinessDates


		exec LogInfo @SP, 'Insert FactMenuItems Begin';
		insert into DW.FactMenuItems (DimHotelId,
			BusinessDates,
			DimRevenueCenterId,
			DimOrderTypeId,
			CheckNumber,
			BusinessDate,
			TransactionDateTime,
			SeatNumber,
			PriceLevel,
			ItemName,
			ItemNumber,
			LineCount,
			LineTotal,
			LineTotal1,
			ReportLineCount,
			ReportLineTotal,
			Voids,
			ManagerVoids,
			Returns,
			CheckEmployee,
			TransactionEmployee,
			AuthorizationEmployee,
			MealEmployee,
			ReasonCode,
			ReferenceInfo,
			SwipeType,
			BatchId,
			CreatedBy,
			ModifiedBy)
		select 
			@HotelId as DimHotelId,
			BusinessDates,
			isnull(rc.DimRevenueCenterId,0) as DimRevenueCenterId,
			isnull(o.DimOrderTypeId, 0) as DimOrderTypeId,
			t.CheckNumber,
			t.BusinessDate,
			t.TransactionDateTime,
			t.SeatNumber,
			t.PriceLevel,
			t.ItemName,
			t.ItemNumber,
			t.LineCount,
			t.LineTotal,
			t.LineTotal1,
			t.ReportLineCount,
			t.ReportLineTotal,
			t.Voids,
			t.ManagerVoids,
			t.Returns,
			t.CheckEmployee,
			t.TransactionEmployee,
			t.AuthorizationEmployee,
			t.MealEmployee,
			t.ReasonCode,
			t.ReferenceInfo,
			t.SwipeType,
			@batchid as BatchId,
			@username as CreatedBy, 
			@username as ModifiedBy
			from #tmpMicrosMenuItems t
			left join DW.DimRevenueCenter rc on rc.RevenueCenter =t.RevenueCenter
			left join DW.DimOrderType o on o.OrderType = t.OrderType


	--	exec LogInfo @SP, 'Merge FactSummaryTransaction Begin';
	--	merge DW.FactMenuItems as target 
	--	using (
	--		select 
	--		@HotelId as DimHotelId,
	--		isnull(rc.DimRevenueCenterId,0) as DimRevenueCenterId,
	--		isnull(o.DimOrderTypeId, 0) as DimOrderTypeId,
	--		t.CheckNumber,
	--		t.BusinessDate,
	--		t.TransactionDateTime,
	--		t.SeatNumber,
	--		t.PriceLevel,
	--		t.ItemName,
	--		t.ItemNumber,
	--		t.LineCount,
	--		t.LineTotal,
	--		t.LineTotal1,
	--		t.ReportLineCount,
	--		t.ReportLineTotal,
	--		t.Voids,
	--		t.ManagerVoids,
	--		t.Returns,
	--		t.CheckEmployee,
	--		t.TransactionEmployee,
	--		t.AuthorizationEmployee,
	--		t.MealEmployee,
	--		t.ReasonCode,
	--		t.ReferenceInfo,
	--		t.SwipeType
	--		from #tmpMicrosMenuItems t
	--		left join DW.DimRevenueCenter rc on rc.RevenueCenter =t.RevenueCenter
	--		left join DW.DimOrderType o on o.OrderType = t.OrderType

	--) as source
	--on 1=1
	--and source.BusinessDate = target.BusinessDate
	--and source.CheckNumber = target.CheckNumber
	--and source.DimHotelId = target.DimHotelId
	--and source.DimRevenueCenterId = target.DimRevenueCenterId
	--and source.DimOrderTypeId = target.DimOrderTypeId
	--and source.LineTotal = target.LineTotal
	--when not matched by target then
	--	insert (
	--		DimHotelId,
	--		DimRevenueCenterId,
	--		DimOrderTypeId,
	--		CheckNumber,
	--		BusinessDate,
	--		TransactionDateTime,
	--		SeatNumber,
	--		PriceLevel,
	--		ItemName,
	--		ItemNumber,
	--		LineCount,
	--		LineTotal,
	--		LineTotal1,
	--		ReportLineCount,
	--		ReportLineTotal,
	--		Voids,
	--		ManagerVoids,
	--		Returns,
	--		CheckEmployee,
	--		TransactionEmployee,
	--		AuthorizationEmployee,
	--		MealEmployee,
	--		ReasonCode,
	--		ReferenceInfo,
	--		SwipeType,
	--		BatchId,
	--		CreatedBy,
	--		ModifiedBy
	--	) values (
	--		source.DimHotelId,
	--		source.DimRevenueCenterId,
	--		source.DimOrderTypeId,
	--		source.CheckNumber,
	--		source.BusinessDate,
	--		source.TransactionDateTime,
	--		source.SeatNumber,
	--		source.PriceLevel,
	--		source.ItemName,
	--		source.ItemNumber,
	--		source.LineCount,
	--		source.LineTotal,
	--		source.LineTotal1,
	--		source.ReportLineCount,
	--		source.ReportLineTotal,
	--		source.Voids,
	--		source.ManagerVoids,
	--		source.Returns,
	--		source.CheckEmployee,
	--		source.TransactionEmployee,
	--		source.AuthorizationEmployee,
	--		source.MealEmployee,
	--		source.ReasonCode,
	--		source.ReferenceInfo,
	--		source.SwipeType,
	--		@batchid,
	--		@username, 
	--		@username
	--	)
	--when matched then
	--	update set
	--		target.TransactionDateTime = source.TransactionDateTime,
	--		target.SeatNumber = source.SeatNumber,
	--		target.PriceLevel = source.PriceLevel,
	--		target.ItemName = source.ItemName,
	--		target.ItemNumber = source.ItemNumber,
	--		target.LineCount = source.LineCount,
	--		--target.LineTotal = source.LineTotal,
	--		target.LineTotal1 = source.LineTotal1,
	--		target.ReportLineCount = source.ReportLineCount,
	--		target.ReportLineTotal = source.ReportLineTotal,
	--		target.Voids = source.Voids,
	--		target.ManagerVoids = source.ManagerVoids,
	--		target.Returns = source.Returns,
	--		target.CheckEmployee = source.CheckEmployee,
	--		target.TransactionEmployee = source.TransactionEmployee,
	--		target.AuthorizationEmployee = source.AuthorizationEmployee,
	--		target.MealEmployee = source.MealEmployee,
	--		target.ReasonCode = source.ReasonCode,
	--		target.ReferenceInfo = source.ReferenceInfo,
	--		target.SwipeType = source.SwipeType,
	--		target.BatchId = @batchid,
	--		target.ModifiedBy = @username
	--;
		
		drop table #tmpMicrosMenuItems;

		exec LogInfo @SP, 'End';
end;