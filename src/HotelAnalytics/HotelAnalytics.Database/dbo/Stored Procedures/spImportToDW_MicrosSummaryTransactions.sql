﻿
CREATE procedure [dbo].[spImportToDW_MicrosSummaryTransactions]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';


		select * 
		into #tmpMicrosSummaryTransactions
		from Staging.MicrosSummaryTransactions
		where 1=1
		and OpenBusinessDate is not null
		and CloseBusinessDate is not null
		and Batchid = @batchid;


		declare @BusinessDates as date
		select top 1 @BusinessDates = BusinessDates from #tmpMicrosSummaryTransactions
		
		declare @HotelId int;
		select @HotelId = HotelId from Batch where BatchId = @batchid;
		--filldimdate
		declare @maxDate datetime;
		declare @minDate datetime;

		select @maxDate = max(OpenBusinessDate), @minDate = min(OpenBusinessDate) from #tmpMicrosSummaryTransactions;


		exec LogInfo @SP, 'Merge DimRevenueCenter';
		merge DW.DimRevenueCenter as target 
		using (
			select distinct 
			RevenueCenter
			from
			#tmpMicrosSummaryTransactions
			where RevenueCenter is not null
		) as source
		on source.RevenueCenter = target.RevenueCenter
		when not matched by target then
			insert (
				RevenueCenter,
				CreatedBy, 
				ModifiedBy
			) values (
				source.RevenueCenter,
				@username, 
				@username
			)
		;


		exec LogInfo @SP, 'Merge DimOrderType';
		merge DW.DimOrderType as target 
		using (
			select distinct 
			OrderType
			from
			#tmpMicrosSummaryTransactions
			where OrderType is not null
		) as source
		on source.OrderType = target.OrderType
		when not matched by target then
			insert (
				OrderType,
				CreatedBy, 
				ModifiedBy
			) values (
				source.OrderType,
				@username, 
				@username
			)
		;


		exec LogInfo @SP, 'Merge DimEmployee';
		merge DW.DimEmployee as target 
		using (
			select distinct 
			Employee
			from
			#tmpMicrosSummaryTransactions
			where Employee is not null
		) as source
		on source.Employee = target.Employee
		when not matched by target then
			insert (
				Employee,
				CreatedBy, 
				ModifiedBy
			) values (
				source.Employee,
				@username, 
				@username
			)
		;

		exec LogInfo @SP, 'Delete Current BusinessDates from  FactSummaryTransaction Begin';

		delete from DW.FactSummaryTransactions where BusinessDates = @BusinessDates


		exec LogInfo @SP, 'Insert FactSummaryTransaction Begin';

		INSERT INTO DW.FactSummaryTransactions (
			DimHotelId,
			BusinessDates,
			OpenDateTime,
			CloseDateTime,
			OpenDate,
			CloseDate,
			DimRevenueCenterId,
			DimOrderTypeId, 
			CustomerNumber,
			CustomerName,
			CheckNumber,
			CheckReference,
			TableReference,
			TableGroupNumber,
			DimEmployeeId,
			NumberofGuests,
			NumberofItems,
			DiscountTotal,
			TipTotal,
			TaxTotal,
			SubTotal,
			CheckTotal,
			TaxExemptTotal,
			TaxExemptReference,
			VoidsTotal,
			ManagerVoidsTotal,
			ErrorCorrectTotal,
			InfoLines,
			CheckDuration,
			DiningTime,
			TransferToCheckNumber,
			NumberofTimePrinted,
			KDSStation,
			PreparationTime,
			IVA6,
			IVA13,
			IVA23,
			TaxRate4,
			TaxRate5,
			TaxRate6,
			TaxRate7,
			TaxRate8,
			BatchId,
			CreatedBy,
			ModifiedBy)
		select 
		@HotelId as DimHotelId,
		t.BusinessDates,
		t.OpenDateTime,
		t.CloseDateTime,
		t.OpenBusinessDate as OpenDate,
		t.CloseBusinessDate as CloseDate,
		isnull(rc.DimRevenueCenterId,0) as DimRevenueCenterId,
		isnull(o.DimOrderTypeId, 0) as DimOrderTypeId,
		t.CustomerNumber,
		t.CustomerName,
		t.CheckNumber,
		t.CheckReference,
		t.TableReference,
		t.TableGroupNumber,
		isnull(e.DimEmployeeId,0) as DimEmployeeId,
		t.NumberofGuests,
		t.NumberofItems,
		t.DiscountTotal,
		t.TipTotal,
		t.TaxTotal,
		t.SubTotal,
		t.CheckTotal,
		t.TaxExemptTotal,
		t.TaxExemptReference,
		t.VoidsTotal,
		t.ManagerVoidsTotal,
		t.ErrorCorrectTotal,
		t.InfoLines,
		t.CheckDuration,
		t.DiningTime,
		t.TransferToCheckNumber,
		t.NumberofTimePrinted,
		t.KDSStation,
		t.PreparationTime,
		t.IVA6,
		t.IVA13,
		t.IVA23,
		t.TaxRate4,
		t.TaxRate5,
		t.TaxRate6,
		t.TaxRate7,
		t.TaxRate8,
		@batchid as BatchId,
		@username as CreatedBy, 
		@username as ModifiedBy
		from #tmpMicrosSummaryTransactions t
		left join DW.DimRevenueCenter rc on rc.RevenueCenter =t.RevenueCenter
		left join DW.DimOrderType o on o.OrderType = t.OrderType
		left join DW.DimEmployee e on e.Employee = t.Employee


		drop table #tmpMicrosSummaryTransactions;

		exec LogInfo @SP, 'End';
end;