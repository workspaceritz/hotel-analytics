﻿CREATE PROCEDURE [dbo].[spFillDimDate]
	@DataInicio date, 
	@DataFim date
as
begin
	declare @date date = @DataInicio 
	
	declare @m int, @q int, @d int, @y int, @wd int, @dy int,
		@ystr nchar(4), @qstr nvarchar(20), @mstr nvarchar(20),
		@wk int, @wkstr nvarchar(20)
	
	while @date <= @DataFim
	begin
		if not exists (select 1 from DW.DimDate where the_date = @date)
		begin
			select 
				@m = datepart(month, @date), 
				@q = datepart(quarter, @date), 
				@d = datepart(day, @date), 
				@y = datepart(year, @date),
				@wk = datepart(wk,@date),
				@dy = datepart(dy,@date)				
				
			select @wd = dbo.fnDayOfWeek(@date)
			select @ystr = cast(@y as nchar(4))
			select @qstr = dbo.fnQuarterName(@q)
			select @mstr = dbo.fnMonthName(@m)
			select @wkstr = dbo.fnWeekName(@wk,@date)
			
			insert into DW.DimDate(
				the_date,
				year,
				quarter_of_year,
				month_of_year,
				day_of_year,
				day_of_month,
				quarter,
				month,
				day,
				day_of_week,
				is_working_day,
				is_weekend,
				quarter_name,				
				month_name,				
				day_name,				
				day_of_week_name,				
				quarter_of_year_name,					
				month_of_year_name,
				is_working_day_name,					
				week,
				week_of_year,
				week_name
			)
			select 
			@date,
			@y,
			@q,
			@m,
			@dy,-- day of year .... 
			@d,
			@y*10 + @q,
			@y*100 + @m,
			@y*10000 + @m*100 + @d,
			@wd,
			case when @wd <= 5 then 1 else 0 end,
			case when @wd > 5 then 1 else 0 end,
			@qstr + ' ' + @ystr, --QuarterName				
			@mstr + ' ' + @ystr, -- MonthName				
			format(@date, 'dd/MM/yyyy'),
			dbo.fnWeekDayName(@wd),  --DayOfWeekName				
			@qstr,				
			@mstr,				
			dbo.fnWorkDayName(@wd),				
			@wk,
			@y*100 + @wk,
			@wkstr
								
		end		
		set @date = DATEADD(day, 1, @date)
	end
end