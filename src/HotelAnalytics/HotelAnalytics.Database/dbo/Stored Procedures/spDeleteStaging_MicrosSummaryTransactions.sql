﻿

CREATE PROCEDURE [dbo].[spDeleteStaging_MicrosSummaryTransactions]
	@batchId int
AS
BEGIN	
	SET NOCOUNT ON;
	DELETE FROM Staging.MicrosSummaryTransactions where BatchId = @batchId;


END