﻿CREATE PROCEDURE [dbo].[spGetBatch]
	@dataSourceCode nvarchar(50),
	@hotelCode nvarchar(50),
	@fileName nvarchar(250),
	@username nvarchar(50)
AS
BEGIN
	DECLARE @idBatch int

	declare @dataSourceId int
	declare @hotelId int

	select @dataSourceId = DataSourceId from DataSource where DataSourceCode = @dataSourceCode

	select @hotelId = HotelId from Hotel where HotelCode = @hotelCode

	INSERT INTO Batch (IsProcessed, DataSourceId, HotelId, FileName, UserName)
	VALUES (0, @dataSourceId ,@hotelId, @fileName,  @username)
	SELECT @idBatch = CAST(SCOPE_IDENTITY() AS INT)

	SELECT @idBatch as idBatch
END