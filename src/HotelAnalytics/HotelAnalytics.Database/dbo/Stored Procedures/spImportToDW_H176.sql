﻿
CREATE procedure [dbo].[spImportToDW_H176]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		select * 
		into #tmpH176
		from Staging.H176
		where 1=1
		and LABEL is not null
		and ROOM_CLASS IS NOT null
		and SUITE is not null
		and Batchid = @batchid;


		exec LogInfo @SP, 'Merge DimRoomType';
		merge DW.DimRoomType as target 
		using (
			select
			LABEL as Label,
			ROOM_CLASS as Class,
			SHORT_DESCRIPTION as Description,
			case when SUITE='Y' then 1 else 0 end as Suite,
			case when LEN(PSUEDO_ROOM_TYPE) >1 then 0 else 1 end as IsRoom
			from
			#tmpH176
		) as source
		on source.Label = target.Label
		when not matched by target then
			insert (
				Label,
				Class,
				Description,
				Suite,
				IsRoom,
				CreatedBy, 
				ModifiedBy
			) values (
				source.Label,
				source.Class,
				source.Description,
				source.Suite,
				source.IsRoom,
				@username, 
				@username
			)
			when matched then
			update set
				target.Label = source.Label,
				target.Class = source.Class,
				target.Description = source.Description,
				target.Suite = source.Suite,
				target.IsRoom = source.IsRoom
		;
		

		exec LogInfo @SP, 'Merge DimRoomNumber';
		merge DW.DimRoomNumber as target 
		using (
				select rt.DimRoomTypeId, t.LABEL as Label, TRIM(rn.value) as Number
				from #tmpH176 t
				cross apply STRING_SPLIT (t.GET_ROOM_LIST_ROOM_CATEGORY, ',') rn
				left join DW.DimRoomType rt on rt.Label = t.Label
		) as source
		on source.DimRoomTypeId = target.DimRoomTypeId
		and source.Label = target.Label
		and source.Number = target.Number
		when not matched by target then
			insert (
				DimRoomTypeId,
				Label,
				Number,
				CreatedBy, 
				ModifiedBy
			) values (
				source.DimRoomTypeId,
				source.Label,
				source.Number,
				@username, 
				@username
			)
		;


		drop table #tmpH176;

		exec LogInfo @SP, 'End';
end;