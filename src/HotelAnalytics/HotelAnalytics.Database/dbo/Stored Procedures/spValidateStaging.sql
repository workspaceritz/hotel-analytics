﻿CREATE PROCEDURE [dbo].[spValidateStaging]
	@batchId int
AS
BEGIN
	EXEC LogInfo 'spValidateStaging', 'Begin'
	declare @debugMsg varchar(400)
	declare @countErrors int

	CREATE TABLE #errorList (ErrorMessage nvarchar(4000), ErrorType nvarchar(100))

	declare @dataSourceCode nvarchar(50)
	declare @dataSourceName nvarchar(250)

	select @dataSourceCode = ds.DataSourceCode, 
	@dataSourceName = ds.DataSourceName
	from DataSource ds
	inner join Batch b on ds.DataSourceId = b.DataSourceId
	where b.BatchId = @batchId

	IF @@ROWCOUNT = 0 
	BEGIN
		exec LogError 'spValidateStaging', 'Could not find Batch...'
		INSERT INTO #errorList VALUES('Could not find Batch...', 'SYSTEM_ERROR')
		SELECT * FROM #errorList
		RETURN
	END

	set @debugMsg = 'Validating loader ' + @dataSourceName + ' for BatchId ' + cast(@batchId as nvarchar(10))
	exec LogInfo 'spValidateStaging', @debugMsg

	
	DECLARE @sql nvarchar(MAX)
	DECLARE @paramlist nvarchar(4000)
	SELECT @sql = 'INSERT INTO #errorList EXEC spValidateStaging_«DATASOURCE» @paramidBatch'
	SELECT @sql = REPLACE(@sql, '«DATASOURCE»', @dataSourceCode)
	SELECT @paramlist = '@paramidBatch int'
	EXEC sp_executesql @sql, @paramlist, @batchId
	
	SELECT * FROM #errorList
	ORDER BY ErrorType, ErrorMessage
	select @countErrors = count(1) from #errorList

	EXEC LogInfo 'spValidateStaging', 'End'
	return @countErrors
END