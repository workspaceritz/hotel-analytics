﻿CREATE PROCEDURE [dbo].[spImportToDW_OperaPlanningBudget]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
AS
BEGIN
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	EXEC LogInfo @SP, 'Begin'

	EXEC spImportToDW_OperaPlanningCommon @batchid, @username, @dataPeriod;

	EXEC LogInfo @SP, 'End'
END