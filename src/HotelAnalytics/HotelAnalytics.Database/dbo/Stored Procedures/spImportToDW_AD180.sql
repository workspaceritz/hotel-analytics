﻿CREATE PROCEDURE [dbo].[spImportToDW_AD180]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
AS
BEGIN
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	DECLARE @DatetimeBeforeMergeOperation datetime;
	DECLARE @DatetimeAfterMergeOperation datetime;
		
	EXEC LogInfo @SP, 'Begin'

	select * 
	into #tmpAD180
	from Staging.AD180 
	where BatchId = @batchid

	declare @maxDate datetime
	declare @minDate datetime

	select @maxDate = MAX(CUR_DATE), @minDate = MIN(CUR_DATE) from #tmpAD180

	exec spFillDimDate @minDate, @maxDate


	drop table #tmpAD180

	EXEC LogInfo @SP, 'End'
END;