﻿CREATE PROCEDURE [dbo].[spDeleteStaging]
	@batchId int
AS
BEGIN
	EXEC LogInfo 'spDeleteStaging', 'Begin'

	declare @debugMsg varchar(400)

	DECLARE @errorList TABLE (ErrorMessage nvarchar(4000), ErrorType nvarchar(100))

	declare @dataSourceCode nvarchar(50)
	declare @dataSourceName nvarchar(250)

	select @dataSourceCode = ds.DataSourceCode, 
	@dataSourceName = ds.DataSourceName
	from DataSource ds
	inner join Batch b on ds.DataSourceId = b.DataSourceId
	where b.BatchId = @batchId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO @errorList VALUES ('Could not find Batch...', 'SYSTEM_ERROR')
		SELECT * FROM @errorList
		RETURN
	END
	exec LogInfo 'spDeleteStaging', @debugMsg

	DECLARE @sql nvarchar(MAX)
	DECLARE @paramlist nvarchar(4000)
	SELECT @sql = 'EXEC dbo.spDeleteStaging_«DATASOURCE» @paramidBatch'
	SELECT @sql = REPLACE(@sql, '«DATASOURCE»', @dataSourceCode)
	SELECT @paramlist = '@paramidBatch int'
	EXEC sp_executesql @sql, @paramlist, @batchId

	SELECT * FROM @errorList

	EXEC LogInfo 'spDeleteStaging', 'End'
END