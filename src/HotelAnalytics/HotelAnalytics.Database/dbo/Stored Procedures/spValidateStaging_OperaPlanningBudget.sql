﻿CREATE PROCEDURE [dbo].[spValidateStaging_OperaPlanningBudget]
	@batchId int
AS
BEGIN
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	EXEC LogInfo @SP, 'Begin'

	EXEC spValidateStaging_OperaPlanningCommon @batchId;

	EXEC LogInfo @SP, 'End'
END