﻿CREATE PROCEDURE [dbo].[spImportToDW_D140]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
AS
BEGIN
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	exec LogInfo @SP, 'Begin';
	declare @DatetimeBeforeMergeOperation datetime;
	declare @DatetimeAfterMergeOperation datetime;

	select *
	into #tmp
	from Staging.D140
	where BatchId = @batchid;
	
	-- populate dims
	exec LogInfo @SP, 'Merge DimParentMarket';

	merge DW.DimParentMarket as target 
	using (
		select distinct MARKET_CODE from #tmp
	) as source
	on source.MARKET_CODE  = target.ParentMarketCode
	when not matched by target then
		insert (
			ParentMarketCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.MARKET_CODE,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimMarket';
	merge DW.DimMarket as target 
	using (
		select
		s.MARKET_CODE as MarketCode,
		s.MARKET_CODE as MarketDescription,
		0 as SellSequence,
		pm.DimParentMarketId
		from (
			select distinct MARKET_CODE from #tmp
		) s
		inner join DW.DimParentMarket pm on pm.ParentMarketCode = s.MARKET_CODE
	) as source
	on source.MarketCode = target.MarketCode
	when not matched by target then
		insert (
			MarketCode,
			MarketDescription,
			SellSequence,
			DimParentMarketId,
			CreatedBy, 
			ModifiedBy
		) values (
			source.MarketCode,
			source.MarketDescription,
			source.SellSequence,
			source.DimParentMarketId,
			@username, 
			@username
		)
	;


	exec LogInfo @SP, 'Merge DimTransactionType';

	--AC: create the transactionType code if not exist, this dim is maintained by H196
	merge DW.DimTransactionType as target 
	using (
		select distinct TRX_CODE as TransactionTypeCode from #tmp
	) as source
	on source.TransactionTypeCode  = target.TransactionTypeCode
	when not matched by target then
		insert (
			TransactionTypeCode,
			[Group],
			SubGroup,
			Description,
			CreatedBy, 
			ModifiedBy
		) values (
			source.TransactionTypeCode,
			source.TransactionTypeCode,
			source.TransactionTypeCode,
			source.TransactionTypeCode,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimRoomClass';
	merge DW.DimRoomClass as target 
	using (
		select distinct ROOM_CLASS as RoomClass from #tmp
	) as source
	on source.RoomClass  = target.RoomClassCode
	when not matched by target then
		insert (
			RoomClassCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.RoomClass,
			@username, 
			@username
		)
	;

	-- prep temp table
	select 
	convert(date, t.BUSINESS_FORMAT_DATE, 4) as DataPeriod,
	m.DimMarketId,
	tt.DimTransactionTypeId,
    rc.DimRoomClassId,
	t.RECEIPT_NO as ReceiptNo,
	t.GUEST_FULL_NAME as GuestFullName,
	t.TRX_DESC as TrxDesc,
	t.REFERENCE as Reference,
	t.TRX_NO as TrxNo,
	t.ROOM as Room,
	t.CREDIT_CARD_SUPPLEMENT as CreditCardSupplement,
	t.REMARK as Remark,
	t.CHEQUE_NUMBER as ChequeNumber,
	t.CASHIER_DEBIT as DebitValue,
	t.CASHIER_CREDIT as CreditValue
	into #tmpPrepped
	from #tmp t
	inner join DW.DimMarket m on m.MarketCode = t.MARKET_CODE
	inner join DW.DimTransactionType tt on tt.TransactionTypeCode = t.TRX_CODE
	inner join DW.DimRoomClass rc on rc.RoomClassCode = t.ROOM_CLASS
	;

	-- filldimdate
	declare @maxDate datetime;
	declare @minDate datetime;

	select @maxDate = max(DataPeriod), @minDate = min(DataPeriod) from #tmpPrepped;

	exec spFillDimDate @minDate, @maxDate;

	declare @HotelId int;
	select @HotelId = HotelId from Batch where BatchId = @batchid;
	declare @DimScenarioId_Actual int;
	select @DimScenarioId_Actual = DimScenarioId from DW.DimScenario where Scenario = 'Actual';

	exec LogInfo @SP, 'Delete FactTransaction';
	delete from DW.FactTransaction
	where 1=1
	and DimHotelId = @HotelId
	and DimScenarioId = @DimScenarioId_Actual
	and DataPeriod = @minDate;

	exec LogInfo @SP, 'Insert FactTransaction';
	insert into DW.FactTransaction (
		DataPeriod,
		DimHotelId,
		DimScenarioId,
		DimMarketId,
		DimTransactionTypeId,
		DimRoomClassId,
		ReceiptNo,
		GuestFullName,
		TrxDesc,
		Reference,
		TrxNo,
		Room,
		CreditCardSupplement,
		Remark,
		ChequeNumber,
		DebitValue,
		CreditValue,
		BatchId,
		CreatedBy,
		ModifiedBy
	)
	select
	t.DataPeriod,
	@HotelId as DimHotelId,
	@DimScenarioId_Actual as DimScenarioId,
	t.DimMarketId,
	t.DimTransactionTypeId,
	t.DimRoomClassId,
	t.ReceiptNo,
	t.GuestFullName,
	t.TrxDesc,
	t.Reference,
	t.TrxNo,
	t.Room,
	t.CreditCardSupplement,
	t.Remark,
	t.ChequeNumber,
	t.DebitValue,
	t.CreditValue,
	@batchid as BatchId,
	@username as CreatedBy,
	@username as ModifiedBy
	from
	#tmpPrepped t
	;

	drop table #tmpPrepped;
	drop table #tmp;


	exec LogInfo @SP, 'End';
END;