﻿CREATE procedure [dbo].[spImportToDW_H222]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
	exec LogInfo @SP, 'Begin';
	declare @DatetimeBeforeMergeOperation datetime;
	declare @DatetimeAfterMergeOperation datetime;

	select *
	into #tmpH222
	from Staging.H222
	where Batchid = @batchid;

	exec LogInfo @SP, 'Merge DimParentMarket';

	merge DW.DimParentMarket as target 
	using (
		select distinct MARKET_CODE from #tmpH222
	) as source
	on source.MARKET_CODE  = target.ParentMarketCode
	when not matched by target then
		insert (
			ParentMarketCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.MARKET_CODE,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimMarket';
	merge DW.DimMarket as target 
	using (
		select
		s.MARKET_CODE as MarketCode,
		s.MARKET_CODE as MarketDescription,
		0 as SellSequence,
		pm.DimParentMarketId
		from (
			select distinct MARKET_CODE from #tmpH222
		) s
		inner join DW.DimParentMarket pm on pm.ParentMarketCode = s.MARKET_CODE
	) as source
	on source.MarketCode = target.MarketCode
	when not matched by target then
		insert (
			MarketCode,
			MarketDescription,
			SellSequence,
			DimParentMarketId,
			CreatedBy, 
			ModifiedBy
		) values (
			source.MarketCode,
			source.MarketDescription,
			source.SellSequence,
			source.DimParentMarketId,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge MarketOperaMapping';
	merge DW.DimMarketOperaMapping as target 
	using (
		select
		m.DimMarketId,
		s.BOF_TRANS_CODE as BofTransCode,
		s.BOF_TRANS_TEXT as BofTransText,
		s.BOF_VALUE as BofValue,
		CreatedBy, 
		ModifiedBy
		from
		#tmpH222 s
		inner join DW.DimMarket m on m.MarketCode = s.MARKET_CODE
	) as source
	on 1=1 
	and source.DimMarketId = target.DimMarketId
	and source.BofTransCode = target.BofTransCode
	and source.BofValue = target.BofValue
	when not matched by target then
		insert (
			DimMarketId,
			BofTransCode,
			BofTransText,
			BofValue,
			CreatedBy, 
			ModifiedBy
		) values (
			source.DimMarketId,
			source.BofTransCode,
			source.BofTransText,
			source.BofValue,
			@username, 
			@username
		)
	when matched then
		update set
			BofTransText = source.BofTransText,
			ModifiedBy = @username
	;
		
	drop table #tmpH222;

	exec LogInfo @SP, 'End';
end;
go