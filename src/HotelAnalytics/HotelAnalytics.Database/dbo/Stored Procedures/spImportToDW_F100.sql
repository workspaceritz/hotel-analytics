﻿CREATE PROCEDURE [dbo].[spImportToDW_F100]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
AS
BEGIN
	declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);	
	exec LogInfo @SP, 'Begin';
	declare @DatetimeBeforeMergeOperation datetime;
	declare @DatetimeAfterMergeOperation datetime;

	declare @HotelId int;
	select @HotelId = HotelId from Batch where BatchId = @batchid;

	select *
	into #tmp
	from Staging.F100
	where BatchId = @batchid;

	-- populate dims
	exec LogInfo @SP, 'Merge DimParentMarket';

	merge DW.DimParentMarket as target 
	using (
		select distinct PARENT_MARKET_CODE from #tmp
	) as source
	on source.PARENT_MARKET_CODE  = target.ParentMarketCode
	when not matched by target then
		insert (
			ParentMarketCode,
			CreatedBy, 
			ModifiedBy
		) values (
			source.PARENT_MARKET_CODE,
			@username, 
			@username
		)
	;

	exec LogInfo @SP, 'Merge DimMarket';
	merge DW.DimMarket as target 
	using (
		select distinct
		s.GRP2_CODE as MarketCode,
		s.DESCRIPTION as MarketDescription,
		0 as SellSequence,
		pm.DimParentMarketId
		from #tmp s
		inner join DW.DimParentMarket pm on pm.ParentMarketCode = s.PARENT_MARKET_CODE
	) as source
	on source.MarketCode = target.MarketCode
	when not matched by target then
		insert (
			MarketCode,
			MarketDescription,
			SellSequence,
			DimParentMarketId,
			CreatedBy, 
			ModifiedBy
		) values (
			source.MarketCode,
			source.MarketDescription,
			source.SellSequence,
			source.DimParentMarketId,
			@username, 
			@username
		)
	;

	select
	@HotelId as DimHotelId,
	s.BUSINESS_DATE as BusinessDate,
	m.DimMarketId,
	s.ROOMS_DAY as RN,
	s.ROOM_REV_DAY as REV,
	s.GUEST_DAY as PAX,
	s.ADR_DAY as ADR,
	s.PER_OCC_DAY as PercentOccupancy,
	@batchid as BatchId,
	@username as CreatedBy,
	@username as ModifiedBy
	into #tmpF100
	from #tmp s
	inner join DW.DimMarket m on m.MarketCode = s.GRP2_CODE
	where 1=1
	and BatchId = @batchid
	and (
		s.ROOMS_DAY != 0 or
		s.ROOM_REV_DAY != 0 or
		s.GUEST_DAY != 0 or
		s.ADR_DAY != 0 or
		s.PER_OCC_DAY != 0
	);

	-- filldimdate
	declare @maxDate datetime;
	declare @minDate datetime;

	select @maxDate = max(BusinessDate), @minDate = min(BusinessDate) from #tmpF100;

	exec spFillDimDate @minDate, @maxDate;

	exec LogInfo @SP, 'Delete FactOperationsSummary';
	delete from DW.FactOperationsSummary
	where 1=1
	and DimHotelId = @HotelId
	and BusinessDate = @minDate;


	exec LogInfo @SP, 'Insert FactOperationsSummary';
	insert into DW.FactOperationsSummary (
		DimHotelId,
		BusinessDate,
		DimMarketId,
		RN,
		REV,
		PAX,
		ADR,
		PercentOccupancy,
		BatchId,
		CreatedBy,
		ModifiedBy
	)
	select
	*
	from #tmpF100
	;

	drop table #tmpF100;
	drop table #tmp;

	exec LogInfo @SP, 'End';
END;