﻿CREATE PROCEDURE [dbo].[LogInternal]
	@LogLevel int,
	@module nvarchar(255),
	@msg nvarchar(2048),
	@spid smallint
as
begin
	set nocount on
	insert into LogTable (SPID, LogLevel, Module, Msg) values (@spid, @LogLevel, @module, @msg)
end