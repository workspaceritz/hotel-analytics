﻿
CREATE procedure [dbo].[spImportToDW_Recipe]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		declare @HotelId int;
		select @HotelId = HotelId from Batch where BatchId = @batchid;

		select 
		t.RecipeNo,
		case when NULLIF(t.Recipe,'') is not null then t.Recipe
		else
			(select top 1 r.Recipe from Staging.Recipe r where 1=1 and r.BatchId = t.BatchId and r.id < t.id and NULLIF(r.Recipe,'') is not null order by r.id desc)
		end as Recipe,
		t.A,
		t.T,
		t.ArticleNo,
		t.Component,
		t.POTQTY,
		t.Unit,
		t.Text,
		t.Loss,
		t.ACTQTY,
		t.QTYBU,
		t.BU,
		t.AVE,
		t.COS,
		t.Note,
		t.Pos,
		t.PlannedPrice,
		t.SumPlannedPrice,
		t.FuturePlannedPrice,
		t.SumFuturePlannedPrice
		into #tmpRecipe
		From Staging.Recipe t
		where 1=1
		and BatchId = @batchid

		exec LogInfo @SP, 'Merge DimRecipe';
		merge DW.DimRecipe as target 
		using (

			select distinct RecipeNo, Recipe from (
				select 
				RecipeNo, Recipe,
				ROW_NUMBER() over(partition by RecipeNo order by RecipeNo) as RowC
				from #tmpRecipe
				where 1=1
				and NULLIF(RecipeNo,'') is not null
				and NULLIF(Recipe,'') is not null
				) a
			where RowC = 1
			--and RecipeNo = 101110012
		) as source
		on source.RecipeNo = target.RecipeNo
		when not matched by target then
			insert (
				RecipeNo,
				Recipe,
				CreatedBy, 
				ModifiedBy
			) values (
				source.RecipeNo,
				source.Recipe,
				@username, 
				@username
			)
		;

		
		exec LogInfo @SP, 'Merge DimRecipeComponent';
		merge DW.DimRecipeComponent as target 
		using (

			select distinct ArticleNo, Component, Unit,BU from #tmpRecipe
		) as source
		on source.ArticleNo = target.ArticleNo
			and source.Component = target.Component
			and source.Unit = target.Unit
			and source.BU = target.BU
		when not matched by target then
			insert (
				ArticleNo,
				Component,
				Unit,
				BU,
				CreatedBy, 
				ModifiedBy
			) values (
				source.ArticleNo,
				source.Component,
				source.Unit,
				source.BU,
				@username, 
				@username
			)
		;


		select
		@HotelId as DimHotelId,
		r.DimRecipeId,
		t.A,
		t.T,
		t.ArticleNo,
		t.Component,
		t.POTQTY,
		t.Unit,
		t.Text,
		t.Loss,
		t.ACTQTY,
		t.QTYBU,
		t.BU,
		t.AVE,
		t.COS,
		t.Note,
		t.Pos,
		t.PlannedPrice,
		t.SumPlannedPrice,
		t.FuturePlannedPrice,
		t.SumFuturePlannedPrice,
		isnull(c.DimRecipeComponentId,0) as DimRecipeComponentId,
		@batchid as BatchId,
		@username as CreatedBy,
		@username as ModifiedBy
		into #tmpRecipe2
		from #tmpRecipe t
		inner join DW.DimRecipe r on r.RecipeNo =t.RecipeNo and r.Recipe = t.Recipe
		left join DW.DimRecipeComponent c on c.ArticleNo = t.ArticleNo and c.Component = t.Component and c.Unit = t.Unit and c.BU =t.BU
		where 1=1
		and NULLIF(t.RecipeNo,'') is not null
		and NULLIF(t.Recipe,'') is not null

		exec LogInfo @SP, 'Delete FactRecipe';
		delete from DW.FactRecipe where DimRecipeId in 
			(
				select distinct DimRecipeId from #tmpRecipe2
				)


		INSERT INTO DW.FactRecipe(	
						DimHotelId,
						DimRecipeId,
						DimRecipeComponentId,
						A,
						T,
						ArticleNo,
						Component,
						POTQTY,
						Unit,
						Text,
						Loss,
						ACTQTY,
						QTYBU,
						BU,
						AVE,
						COS,
						Note,
						Pos,
						PlannedPrice,
						SumPlannedPrice,
						FuturePlannedPrice,
						SumFuturePlannedPrice,
						BatchId,
						CreatedBy, 
						ModifiedBy)
		select 
						DimHotelId,
						DimRecipeId,
						DimRecipeComponentId,
						A,
						T,
						ArticleNo,
						Component,
						POTQTY,
						Unit,
						Text,
						Loss,
						ACTQTY,
						QTYBU,
						BU,
						AVE,
						COS,
						Note,
						Pos,
						PlannedPrice,
						SumPlannedPrice,
						FuturePlannedPrice,
						SumFuturePlannedPrice,
						BatchId,
						CreatedBy,
						ModifiedBy
					from #tmpRecipe2
		
		drop table #tmpRecipe;
		drop table #tmpRecipe2;

		exec LogInfo @SP, 'End';
end;