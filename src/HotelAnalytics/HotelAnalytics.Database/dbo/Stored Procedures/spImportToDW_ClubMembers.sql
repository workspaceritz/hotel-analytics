﻿


CREATE procedure [dbo].[spImportToDW_ClubMembers]
	@batchid int,
	@username nvarchar(128),
	@dataPeriod date
as
begin
		declare @DatetimeBeforeMergeOperation datetime;
		declare @DatetimeAfterMergeOperation datetime;
		declare @SP nvarchar(500) = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + object_name(@@PROCID);
		exec LogInfo @SP, 'Begin';

		--1530
		--Number must be a number
		--TelephoneClean is key | unique numbers
		--TelephoneClean must be a number >0

		select 
		* 
		into #tmpClubMembers
		from (
			select 
				s.Number,
				s.CardNr,
				s.LastName,
				s.FirstName,
				s.Civility,
				s.Gender,
				s.Address,
				s.City,
				s.ZipCode,
				s.Email,
				s.TelephoneClean,
				s.Telephone,
				s.Country,
				ROW_NUMBER() over (partition by s.TelephoneClean order by s.Number desc) as RowC
			from Staging.ClubMembers s
			where 1=1
			and ISNUMERIC(Number) =1
			and ISNUMERIC(TelephoneClean) = 1
			and TelephoneClean <> '0'
			and Batchid = @batchid
		) a 
		where 1=1
		and RowC =1

		exec LogInfo @SP, 'Merge DW.DimClubMember';
		merge DW.DimClubMember as target 
		using (
			select  
				Number,
				CardNr,
				LastName,
				FirstName,
				Civility,
				Gender,
				Address,
				City,
				ZipCode,
				Email,
				TelephoneClean,
				Telephone,
				Country
			from
			#tmpClubMembers
		) as source
		on source.TelephoneClean = target.TelephoneClean
		when not matched by target then
			insert (
				Number,
				CardNr,
				LastName,
				FirstName,
				Civility,
				Gender,
				Address,
				City,
				ZipCode,
				Email,
				TelephoneClean,
				Telephone,
				Country,
				CreatedBy, 
				ModifiedBy
			) values (
				source.Number,
				source.CardNr,
				source.LastName,
				source.FirstName,
				source.Civility,
				source.Gender,
				source.Address,
				source.City,
				source.ZipCode,
				source.Email,
				source.TelephoneClean,
				source.Telephone,
				source.Country,
				@username, 
				@username
			)
		when matched then
			update set
				target.Number = source.Number,
				target.CardNr= source.CardNr,
				target.LastName= source.LastName,
				target.FirstName= source.FirstName,
				target.Civility= source.Civility,
				target.Gender= source.Gender,
				target.Address= source.Address,
				target.City= source.City,
				target.ZipCode= source.ZipCode,
				target.Email= source.Email,
				target.Telephone= source.Telephone,
				target.Country= source.Country,
				target.ModifiedBy = @username
		;


		drop table #tmpClubMembers;

		exec LogInfo @SP, 'End';
end;