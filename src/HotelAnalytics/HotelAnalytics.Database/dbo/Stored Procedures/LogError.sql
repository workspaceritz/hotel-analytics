﻿CREATE PROCEDURE [dbo].[LogError]
	@module NVARCHAR(255),
	@msg NVARCHAR(2048)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @datetext VARCHAR(20) = FORMAT(GETDATE(), 'yyyy-MM-dd HH:mm:ss')

	IF CAST(dbo.fnGetConfiguration('LogLevel') AS INT) < 2
		RETURN
	
	IF EXISTS (
		SELECT ''
		FROM    tempdb.dbo.sysobjects
		WHERE   id = OBJECT_ID(N'[tempdb].[dbo].[#ShowLogPrint]')
	)
	PRINT @dateText + ' - Module: ' + @module + ' - Msg: ' + @msg

    --EXEC loopback.KWPBFP.dbo.LogInternal 1, @module, @msg, @@spid
	EXEC dbo.LogInternal 1, @module, @msg, @@spid
END