﻿CREATE PROCEDURE [dbo].[LogInfo]
	@module NVARCHAR(255),
	@msg NVARCHAR(2048)
AS
BEGIN
	SET NOCOUNT ON

	IF CAST(dbo.fnGetConfiguration('LogLevel') AS INT) < 2
		RETURN

	IF EXISTS (
		SELECT ''
		FROM    tempdb.dbo.sysobjects
		WHERE   id = OBJECT_ID(N'[tempdb].[dbo].[#ShowLogPrint]')
	)
	PRINT FORMAT(SYSDATETIME(), 'yyyy-MM-dd HH:mm:ss') + ' - Module: ' + @module + ' - Msg: ' + @msg
	--PRINT convert(varchar(64), getdate(), 120) + ' - Module: ' + @module + ' - Msg: ' + @msg

    --EXEC loopback.KWPBFP.dbo.LogInternal 1, @module, @msg, @@spid
	EXEC dbo.LogInternal 1, @module, @msg, @@spid
END