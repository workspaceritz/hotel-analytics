﻿CREATE PROCEDURE [dbo].[spDeleteStaging_OperaPlanningForecast]
	@batchId int
AS
BEGIN	
	SET NOCOUNT ON;
	DELETE FROM Staging.OperaPlanning where BatchId = @batchId;
END